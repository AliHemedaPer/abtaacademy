<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
    	\DB::table('admins')->insert([
            'name'   =>  'Admin',
            'email' => 'admin@abtaacademy.com',
            'password'   =>  bcrypt('123456')
        ]);

        \DB::table('settings')->insert([
            'app_name'   =>  'ABTAACADEMY',
            'app_email' => '',
            'app_phone'   =>  '',
            'app_currency' => 'LE',
            'km_price' => 1,
            'ph_search_number'   =>  3,
            'ph_wait_min' => 15,
        ]);
    }
}
