let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.react('resources/assets/js/components/client/products.js', 'public/js/client/')
mix.sass('resources/assets/js/components/client/products.scss', 'public/backend/client/css');
mix.copyDirectory('resources/assets/backend/LTR', 'public/backend/LTR');
mix.copyDirectory('resources/assets/backend/images', 'public/backend/images');
mix.copyDirectory('resources/assets/backend/js/demo_pages', 'public/backend/js/demo_pages');
mix.copyDirectory('resources/assets/backend/js/main', 'public/backend/js/main');
mix.copyDirectory('resources/assets/backend/js/plugins', 'public/backend/js/plugins');
mix.copyDirectory('resources/assets/images', 'public/images');

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/backend/js/app.js', 'public/backend/js/app.js')
   .js('resources/assets/backend/js/custom.js', 'public/backend/js/custom.js')
   .sass('resources/assets/sass/app.scss', 'public/css');


