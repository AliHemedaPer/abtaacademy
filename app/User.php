<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class User extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * Corresponding Customer Details
     * @return mixed
     */
    public function customer()
    {
        return $this->hasOne('Customer', 'id', 'user_id');
    }


    /**
     * Corresponding Customer Details
     * @return mixed
     */
    public function professional()
    {
        return $this->hasOne('MedicalProfessional', 'id', 'user_id');
    }

}
