<?php
namespace App\Services;

use App\Language;

class Languageid
{

    /**
     * Get a validator for a contact.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    static function getId()
    {
        $locale = app()->getLocale();
        $lang = Language::where('code', '=', $locale)->first();
        return $lang->id;
    }
}