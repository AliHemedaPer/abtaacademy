<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rols'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'rols' => 'array',
    ];

    public function scopeNotSuperAdmin($builder)
    {
        $builder->where('id', '!=', 1);
    }

    public function hasRole($routeRols)
    {
        $userRols = $this->rols;
        if ((int) $this->id === 1) {
            return true;
        }
        if (!$userRols) {
            return false;
        }
        foreach ($routeRols as $roleName => $cliArray) {
            if (in_array($roleName, array_keys($userRols))) {
                foreach ($cliArray as $key => $value) {
                    if (!in_array($value, $this->rols[$roleName])) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public function canCreate($role)
    {
        return $this->hasRole([$role => ['create']]);
    }

    public function canRead($role)
    {
        return $this->hasRole([$role => ['read']]);
    }

    public function canUpdate($role)
    {
        return $this->hasRole([$role => ['update']]);
    }

    public function canDelete($role)
    {
        return $this->hasRole([$role => ['delete']]);
    }
}
