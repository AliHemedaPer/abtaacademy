<?php

namespace App\Models\Common;

use App;
use App\Models\Common\Images;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = "image_gallery";

	protected $guarded = [];
	
    public function getTitleAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'title_'.$local };
    }

   
    public function getDescriptionAttribute()
    {
        $local = App::getLocale();
        return $this->{ 'desc_'.$local };
    }

    public function images()
    {
        return $this->hasMany(Images::class, 'parent_id', 'id');
    }

    public function oneImage()
    {
        return $this->images()->limit(1)->get()[0]->image;
    }
}
