<?php

namespace App\Models\Common;

use App;
use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{    
    protected $guarded = [];

    public function Category()
    {
        return $this->belongsTo(Category::class, 'category');
    }


    public function getNameAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'name_'.$local };
    }

    public function getTitleAttribute()
    {
        $local = App::getLocale();
        return $this->{ 'title_'.$local };
    }

    public function getShortDescAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'short_desc_'.$local };
    }

    public function getDescripionAttribute()
    {
        $local = App::getLocale();
        return $this->{ 'descripion_'.$local };
    }
}
