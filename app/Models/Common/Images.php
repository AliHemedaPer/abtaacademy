<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = "images";

    protected $guarded = [];
}
