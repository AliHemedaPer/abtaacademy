<?php

namespace App\Models\Common;

use App;
use App\Models\Common\Post;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $guarded = [];
	
    public function posts()
    {
        return $this->hasMany(Post::class, 'category');
    }

    public function trainers()
    {
        return $this->hasMany(Trainer::class, 'category');
    }

    public function getNameAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'name_'.$local };
    }

    public function getShortDescAttribute()
    {
        $local = App::getLocale();
        return $this->{ 'short_desc_'.$local };
    }

    public function getContentAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'content_'.$local };
    }
    
    public function parentObj(){
		return $this->belongsTo(Category::class, 'parent');
	}

	public function children(){
		return $this->hasMany(Category::class, 'parent')->orderBy('order');
	}

    public function page(){
        return $this->hasMany(Post::class, 'category')->where('post_type', 'page');
    }
}
