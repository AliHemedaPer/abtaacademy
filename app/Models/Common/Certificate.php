<?php

namespace App\Models\Common;

use App;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{    
     protected $guarded = [];

    public function getTitleAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'title_'.$local };
    }

    public function getContentAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'content_'.$local };
    }
}
