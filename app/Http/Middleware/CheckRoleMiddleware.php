<?php
namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Auth\AuthenticationException;

/**
 * Class CheckRoleMiddleware
 * @package App\Http\Middleware
 */
class CheckRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pst = $request->get('pst') ?? 0;

        $user = auth('admin')->user();

        if ($user) {
            $rols = $this->getRequiredRoleForRoute($request->route(), $pst);
            if (!$rols || $user->hasRole($rols)) {
                return $next($request);
            }
        }
        throw new AuthenticationException('Unauthenticated.', ['admin']);
        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'You are not authorized to access this resource.'
            ]
        ], 401);
    }

    /**
     * @param $route
     * @return null
     */
    private function getRequiredRoleForRoute($route, $pst)
    {
        $actions = $route->getAction();
        
        if($pst)
            return isset($actions[$pst]) ? $actions[$pst] : null;

        return isset($actions['rols']) ? $actions['rols'] : null;
    }
}
