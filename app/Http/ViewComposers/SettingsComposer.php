<?php 

namespace App\Http\ViewComposers;

use App;
use DB;
use App\Models\Common\Category;
use Illuminate\Contracts\View\View;

class SettingsComposer
{
    public function compose(View $view) {
    	$locale      = App::getLocale();
    	$settings    = DB::table('settings')->get();
    	$about       = DB::table('posts')->where('menu_page', 'about')->first();
        $whyus       = DB::table('posts')->where('menu_page', 'why_us')->first();

    	$categories  = Category::where('home_page', 1)
                               ->where('parent', 0)
                               ->orderBy('order')
                               ->get();

    	$appSettings = DB::table('settings')->get();
    	$view->with('share_locale', $locale);
        $view->with('appSettings', $appSettings[0]);
        $view->with('menuCategories', $categories);
        $view->with('menuAbout', $about);
        $view->with('menuWhy', $whyus);
    }
}