<?php 

namespace App\Http\ViewComposers;

use DB;
use Illuminate\Contracts\View\View;

class DatesComposer
{
    public function compose(View $view) {
    	$monthes = ['0' => '- Month -', '01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Des'];

        $years[0] = '- Year -';
        for ($i=1930; $i <= date('Y') - 10; $i++) { 
            $years[$i] = $i;
        }

        $days[0] = '- Day -';
        for ($i=1; $i < 31; $i++) {
            if($i < 10) 
                $days[$i] = '0'.$i;
            else
                $days[$i] = $i;
        }

        $view->with('share_dates', ['days' => $days, 'monthes' => $monthes, 'years' => $years]);
    }
}