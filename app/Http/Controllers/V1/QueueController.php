<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Queue;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Auth\Factory as Auth;

class QueueController extends Controller
{
    protected $type;

    public function __construct(Auth $auth)
    {
        // $this->middleware('auth:' . $guard);
        //$this->type = Route::current()->parameter('type');
    }

    public function index()
    {
        $this->middleware('auth:'. $guard = $this->guard());
        $ret = [];

        switch ($guard) {
            case 'admin':
                $prescriptionsJops = Queue::where('type', '=', 'prescription')->whereNull('readed_at')->get();
                $prescriptionsJops->each->touch();
                $ret['prescriptionsJops'] = $prescriptionsJops;
                break;
            case 'client':

                break;
            case 'pharmacy':

                break;
        }

        return response()->json([
            'status' => 1,
            'data' => $ret
        ]);
    }

    protected function guard()
    {
        if (auth('admin')->check()) {
            return 'admin';
        } elseif (auth('client')->check()) {
            return 'client';
        } elseif (auth('pharmacy')->check()) {
            return 'pharmacy';
        }
        return false;
    }
}
