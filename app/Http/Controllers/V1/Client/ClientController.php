<?php
namespace App\Http\Controllers\V1\Client;

use App\User;
use App\Models\Client\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;
use Illuminate\Support\Facades\Redirect;

header("Access-Control-Allow-Origin: *");

class ClientController extends Controller
{
    protected function guard()
    {
        return Auth::guard('client');
    }

    public function registerAction(Request $request)
    {
        try {
            $name = $request->name ?? '';
            $email = $request->email ?? '';
            $phone = $request->phone ?? '';
            $password = $request->password ?? '';
            $confirm_password = $request->confirm_password ?? '';
            $client_type = $request->client_type ?? 'client';
            
            if (Client::where('email', '=', $email)->count() != 0) {
                return Response()->make(['status' => 'failure' , 'msg' => 'Email Exists!']);
            }

            Client::insert([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'password' => bcrypt($password),
                'client_type' => $client_type
            ]);

            if ($this->guard()->attempt(['email' => $email , 'password' => $password])) {
                $request->session()->put('user_id', $email);

                return Response()->json(['status' => 'success1' , 'msg' => '']);
            }else {
                 return Response()->json(['status' => 'success2' , 'msg' => 'Acccount Successfully Created!']);
            }
            
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'failure' , 'msg' => $message['msg']], $message['code']);
        }
    }


    /**
     * User Login
     *
     * @return mixed
     */
    public function UserLogin(Request $request)
    {
        try {
            $email = $request->email ?? '';
            $password = $request->pass ?? '';
            
            if ($this->guard()->attempt(['email' => $email , 'password' => $password])) {
                $request->session()->put('user_id', $email);
                if($request->course)
                    $result['course'] = $request->course;

                $result['status'] = 'success';
            } else {
                 $result = ['status' => 'failure'];
            }
        } catch (Exception $e) {
            $message = $this->catchException($e);
            $result = ['status' => 'failure'];
        }

        return Response()->json($result);
    }

    public function resetPasswordView(Request $request){
        $bkg = rand(1,4);
        $title = __('front.resetpassword');

        $reset_code = $request->reset_code ?? '';

        return view('front.login-forget')->with(compact('bkg', 'title', 'reset_code'));
    }
    /**
     * Reset Password
     *
     * @return mixed
     */
    public function resetPassword(Request $request)
    {
        $status = 'failure';
        $msg    = 'Failed, Please Try Later!';

        $email = $request->email ?? '';
        if ($request->reset_code && $request->pass) {
            $security_code = $request->reset_code;
            $password = $request->pass;

            $client = Client::where('security_code', '=', $security_code)->first();
            if (!is_null($client)) {
                $client->password = Hash::make($password);
                $client->status = 1;
                $client->save();
                $client = Client::where('security_code', '=', $security_code)->update(['security_code' => '']);
                $status = 'success';
                $msg    = 'Password Successfully Changed!';
            }else{
                $msg    = 'Failed, Wrong Reset Code!';
            }
        }else {
            if (Client::where('email', '=', $email)->count() == 1) {
                $digits = 8;
                $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $updatedValues = ['security_code' => $randomValue];
                $client = Client::where('email', '=', $email)->update($updatedValues);
                $data = ['code' => $randomValue];
                Mail::send('emails.reset_password', ['data' => $data], function ($message) use ($email) {
                    $message->to($email)->subject('ABTA ACADEMY Reset Password');
                });
                $msg    = 'Reset Password Link Sent to your Email!';
            }else{
                $msg    = 'Failed, Wrong Email!';
            }
        }

        return Response()->json( ['status' => $status, 'msg' =>  $msg ] );
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/');
    } 
}
