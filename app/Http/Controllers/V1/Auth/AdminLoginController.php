<?php

namespace App\Http\Controllers\V1\Auth;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function resetPassword(Request $request)
    {
        $result = [];
        if ($request->method() == 'POST') {
            $email = $request->email ?? '';
            if ($email && $request->security_code && $request->new_password) {
                $security_code = $request->security_code;
                $password = $request->new_password;
                //$confirm_password = $request->confirm_password;
                $admin = Admin::where('email', '=', $email)->where('security_code', '=', $security_code)->first();
                if (!is_null($admin)) {
                    $admin->password = Hash::make($password);
                    $admin->security_code = '';
                    $admin->save();
                    //$result = array(array('result' => array('status' => 'success')));
                    $result = ['status' => 1 , 'alert' => 'success','msg' => 'Password Changed!'];
                    //return Response()->json($result);
                }else{
                    $result = ['status' => 1 , 'alert' => 'danger','msg' => 'Account not Exists!'];
                }
            }else {
                if (Admin::where('email', '=', $email)->count() == 1) {
                    $digits = 8;
                    $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                    $updatedValues = ['security_code' => $randomValue];
                    $client = Admin::where('email', '=', $email)->update($updatedValues);
                    $data = ['email' => $email, 'app_name' => 'Saidalista', 'code' => $randomValue, 'guard' => '/admin/reset'];
                    Mail::send('emails.reset_password', ['data' => $data], function ($message) use ($email) {
                        $message->to($email)->subject('Reset Password');
                    });
                    //                  $result = array(array('result' => array('status' => 'reset_success')));
                    $result = ['status' => 1 , 'alert' => 'success','msg' => 'Check your Email for reset Link!'];
                    //return Response()->json($result);
                }else{
                    $result = ['status' => 1 , 'alert' => 'danger','msg' => 'Account not Exists!'];
                }
            }
        }
        return view('admin.reset-password')->with(compact('result'));
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/admin/login');
    }    
}
