<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Trainer;
use App\Models\Common\Category;
use Validator;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = Trainer::all();        
        return view('admin.trainers.index', compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trainer = NULL; 
        $categories = Category::all();
        return view('admin.trainers.form', compact('trainer', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255'
       ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = '/public/images/trainers/';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move( base_path() . $path, $file_name);
            $data['image'] = $path . $file_name;
        }
        Trainer::create($data);
        
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/trainers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer)
    {
        $categories = Category::all();
       return view('admin.trainers.form', compact('trainer', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Trainer $trainer)
    {
         $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255'
       ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 

        $data = $request->all();
        $path = '/public/images/trainers/';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move( base_path() . $path, $file_name);
            if (file_exists($trainer->image)) {
                unlink($trainer->image);
            }
            $data['image'] = $path . $file_name;
        }
        $trainer->update($data);
    
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/trainers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Trainer $trainer) {   
        $trainer->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/trainers');
    }
}
