<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Common\Post;
use App\Models\Common\Category;
use App\Models\Common\Trainer;
use App\Models\Common\Certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $post_type = $request->get('pst');
        if(!$post_type)
            die('no post type');

        $page_title = 
        $posts = Post::where('post_type', $post_type)->orderBy('id', 'DESC')->get();
        return view('admin.post.index', compact('posts', 'post_type'));
    }

    public function create(Request $request)
    {
        $post_type = $request->get('pst') ?? '';
        if(!$post_type)
            die('no post type');

        $certificates = Certificate::all();

        $categories = Category::all();
        $trainers = Trainer::all();
        return view('admin.post.form', compact('categories', 'post_type', 'trainers', 'certificates'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255',
            'video'    => 'max:50000'
       ]);

        $validator->validate();
        $post_type = $request->post_type;
        $data = $request->all();

        if($post_type == 'page'){
            $data['slug'] = str_slug($request->name_en, '-');
        }

        $file_name = '';
        if ($request->hasFile('image')) {
            $path = base_path() . '/public/images/posts/';
            $file_name = time();
            $ext = $request->image->getClientOriginalExtension();
            // If Invalid Extension
            if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $fname = $request->image->getClientOriginalName();
            $file_name = time();
            $file_name .= $file_name . $fname;
            $request->image->move($path, $file_name);
            $data['image'] = $file_name;
        }

        $video_name = '';
        if ($request->hasFile('video')) {
            $path = base_path() . '/public/videos/posts/';
            $video_name = time();
            //$ext = $request->video->getClientOriginalExtension();

            $fname = $request->video->getClientOriginalName();
            $video_name = time();
            $video_name .= $video_name . $fname;
            $request->video->move($path, $video_name);
            $data['video'] = $video_name;
        }

        $brochure_name = '';
        if ($request->hasFile('brochure')) {
            $path = base_path() . '/public/brochures/posts/';
            $brochure_name = time();
            //$ext = $request->brochure->getClientOriginalExtension();

            $fname = $request->brochure->getClientOriginalName();
            $ext   = $request->brochure->getClientOriginalExtension();
            if (!in_array($ext, ['pdf', 'doc', 'docx', 'xls', 'xlxs'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload PDF, DOC, DOCX files');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }
            $brochure_name = time();
            $brochure_name .= $brochure_name . $fname;
            $request->brochure->move($path, $brochure_name);
            $data['brochure'] = $brochure_name;
        }

        $data['offers']  = $request->has('offers')?1:0;
        $data['popular']  = $request->has('popular')?1:0;

        if($data['extra_info'])
            $data['extra_info'] = json_encode($data['extra_info']);

        if( $data['certificates'] && count($data['certificates']) > 0)
            $data['certificates'] = json_encode($data['certificates']);

        $post = Post::create($data);

        $request->session()->flash('success', 'Created Successfully');
        return redirect()->route('posts.index', ['pst' => $post_type]);
        //return Redirect::action('V1\Admin\PostController@index');
    }


    public function edit(Request $request, Post $post)
    {
        $post_type = $request->get('pst');
        if(!$post_type)
            die('no post type');

        $extra_info   = json_decode($post->extra_info, true);
        $post->certificates = json_decode($post->certificates, true);

        $certificates = Certificate::all();

        $categories = Category::all();
        $trainers = Trainer::all();
        return view('admin.post.form', compact('post', 'categories', 'post_type', 'extra_info', 'trainers', 'certificates'));
    }

    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255',
            'video'    => 'max:50000'
       ]);

        $validator->validate();
        $post_type = $request->post_type;

        $remove_brochure = $updates['remove_brochure'] ?? 0;
        $remove_video    = $updates['remove_video'] ?? 0;

        $updates = $request->except(['remove_brochure', 'remove_video']);

        if($post_type == 'page'){
            $updates['slug'] = str_slug($request->title_en, '-');
        }

        $file_name = '';
        if ($request->hasFile('image')) {
            $path = base_path() . '/public/images/posts/';
            $file_name = time();
            $ext = $request->image->getClientOriginalExtension();
            // If Invalid Extension
            if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $fname = $request->image->getClientOriginalName();
            $file_name = time();
            $file_name .= $file_name . $fname;
            $request->image->move($path, $file_name);
            $updates['image'] = $file_name;
        }

        if(isset($remove_video) && $remove_video == 'on'){
            $updates['video'] = '';

            if($post->video && \File::exists(base_path().'/public/videos/posts/' . $post->video)) {
                unlink(base_path().'/public/videos/posts/' . $post->video);
            }
        }
        $video_name = '';
        if ($request->hasFile('video')) {
            $path = base_path() . '/public/videos/posts/';
            $video_name = time();
            //$ext = $request->video->getClientOriginalExtension();

            $fname = $request->video->getClientOriginalName();
            $video_name = time();
            $video_name .= $video_name . $fname;
            $request->video->move($path, $video_name);
            $updates['video'] = $video_name;
        }
        
        if(isset($remove_brochure) && $remove_brochure == 'on'){
            $updates['brochure'] = '';

            if($post->brochure && \File::exists(base_path().'/public/brochures/posts/' . $post->brochure)) {
                unlink(base_path().'/public/brochures/posts/' . $post->brochure);
            }
        }
        $brochure_name = '';
        if ($request->hasFile('brochure')) {
            $path = base_path() . '/public/brochures/posts/';
            $brochure_name = time();
            //$ext = $request->brochure->getClientOriginalExtension();

            $fname = $request->brochure->getClientOriginalName();
            $ext   = $request->brochure->getClientOriginalExtension();
            if (!in_array($ext, ['pdf', 'doc', 'docx', 'xls', 'xlxs'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload PDF, DOC, DOCX files');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $brochure_name = time();
            $brochure_name .= $brochure_name . $fname;
            $request->brochure->move($path, $brochure_name);
            $updates['brochure'] = $brochure_name;

            if($post->brochure && \File::exists(base_path().'/public/brochures/posts/' . $post->brochure)) {
                unlink(base_path().'/public/brochures/posts/' . $post->brochure);
            }
        }

        $updates['offers']  = $request->has('offers')?1:0;
        $updates['popular']  = $request->has('popular')?1:0;
        
        if( isset($updates['extra_info']) )
            $updates['extra_info'] = json_encode($updates['extra_info']);

        if( isset($updates['certificates']) && count($updates['certificates']) > 0)
            $updates['certificates'] = json_encode($updates['certificates']);

        $post->update($updates);

        $request->session()->flash('success', 'تم بنجاح');
        return redirect()->route('posts.index', ['pst' => $post_type]);
    }

    /*public function toggleStatus(Post $post)
    {
        $newSts = $post->status ? 0 : 1;
        $post->update(['status' => $newSts ]);

        return response()->json(['status' => 1]);
    }*/

    public function destroy(Post $post)
    {
        $post->delete();
        return response()->json(['status' => 1]);
    }    
}
