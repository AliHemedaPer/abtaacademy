<?php

namespace App\Http\Controllers\V1\Admin;

use Image;

use App\Models\Common\Gallery;
use App\Models\Common\Images;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class GalleryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $gallery = Gallery::all();
        return view('admin.gallery.index', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $messages = [
            'title_en.required' => 'العنوان EN مطلوب!',
            'title_ar.required' => 'العنوان AR مطلوب!'
        ];

        $validator = Validator::make($request->all(), [
                    'title_en' => 'required|string|max:255',
                    'title_ar' => 'required|string|max:255'
        ], $messages);

        $validator->validate();

        $p_images    = $request->images ?? [];

        $data        = $request->except([
                                            'images'
                                        ]);
        

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = '/public/images/gallery/';
            $public_path = public_path('images/gallery/');

            $fname = $image->getClientOriginalName();
            $ext   = $image->getClientOriginalExtension();
            if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $file_name = time();
            $file_name .= $file_name . $fname;
            $full_file_name = $public_path.$file_name;

            $img = Image::make($image->path());
            $img->resize(255, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($full_file_name);

            $data['image'] = $path.$file_name;
            //$request->image->move($path, $file_name);
        }

        $gallery = Gallery::create($data);

        if($gallery->id){

            //Handle Images
            if(count($p_images)){
                $path ='/public/images/gallery/';

                foreach ($p_images as $image) {
                    $file_name = time();
                    $ext = $image->getClientOriginalExtension();
                    // If Invalid Extension
                    if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                        continue;
                    }

                    $fname = $image->getClientOriginalName();
                    $file_name = time();
                    $file_name .= $file_name . $fname;
                    $full_file_name = $path.$file_name;

                    $image->move( base_path() . $path, $file_name);

                    Images::create([
                        'parent_id' => $gallery->id,
                        'image' => $full_file_name,
                        'typ' => 'gallery'
                    ]);
                }
            }
        }
        
        $request->session()->flash('success', 'تم بنجاح!');
        return Redirect::action('Admin\GalleryController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery_image, Request $request) {
        $gallery = $gallery_image;
        return view('admin.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Gallery $gallery_image, Request $request) {
        $messages = [
            'title_en.required' => 'العنوان EN مطلوب!',
            'title_ar.required' => 'العنوان AR مطلوب!'
        ];

        $validator = Validator::make($request->all(), [
                    'title_en' => 'required|string|max:255',
                    'title_ar' => 'required|string|max:255'
        ], $messages);

        $validator->validate();
        
        $p_images    = $request->images ?? [];

        $data        = $request->except([
                                            'images'
                                        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = '/public/images/gallery/';
            $public_path = public_path('images/gallery/');

            $fname = $image->getClientOriginalName();
            $ext   = $image->getClientOriginalExtension();
            if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $file_name = time();
            $file_name .= $file_name . $fname;
            $full_file_name = $public_path.$file_name;

            $img = Image::make($image->path());
            $img->resize(255, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($full_file_name);

            if($gallery_image->image && \File::exists(base_path() . $gallery_image->image)) {
                unlink(base_path() . $gallery_image->image);
            }
            $data['image'] = $path.$file_name;
        }
    
        $gallery_image->update($data);

        //Handle Package Images
        if(count($p_images)){
            $path = '/public/images/gallery/';

            foreach ($p_images as $image) {
                $file_name = time();
                $ext = $image->getClientOriginalExtension();
                // If Invalid Extension
                if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                    continue;
                }

                $fname = $image->getClientOriginalName();
                $file_name = time();
                $file_name .= $file_name . $fname;
                $full_file_name = $path.$file_name;
                
                $image->move(base_path() . $path, $file_name);

                Images::create([
                    'parent_id' => $gallery_image->id,
                    'image' => $full_file_name,
                    'typ' => 'gallery'
                ]);
            }
        }

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect()->route('gallery_images.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery_image) {
        $gallery_image->delete();
        return response()->json(['status' => 1]);
    }

    public function removeImage(Images $image)
    {
        $image->delete();
        return response()->json(['status' => 1]);
    }
}
