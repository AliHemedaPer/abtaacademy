<?php

namespace App\Http\Controllers\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Certificate;
use Validator;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certificates = Certificate::all();        
        return view('admin.certificates.index', compact('certificates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $certificate = NULL; 
        return view('admin.certificates.form', compact('certificate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255'
       ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = '/public/images/certificates/';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move( base_path() . $path, $file_name);
            $data['image'] = $path . $file_name;
        }
        Certificate::create($data);
        
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/certificates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
       return view('admin.certificates.form', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Certificate $certificate)
    {
         $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255'
       ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 

        $data = $request->all();
        $path = '/public/images/certificates/';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move( base_path() . $path, $file_name);
            if (file_exists($banner->image)) {
                unlink($banner->image);
            }
            $data['image'] = $path . $file_name;
        }
        $certificate->update($data);
    
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/certificates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Certificate $certificate) {   
        $certificate->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/certificates');
    }
}
