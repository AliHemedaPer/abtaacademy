<?php

namespace App\Http\Controllers\V1\Admin;

use DB;
use App\Admin;
use App\Models\Client\Client;
use App\Models\Client\ClientMessages;
use App\Models\Common\Post;
use App\Models\Common\Certificate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Models\Common\Images;

class AdminController extends Controller
{
    public function home()
    {
        $admins = Admin::notSuperAdmin()->count();
        $courses = Post::where('post_type', 'course')->count();
        $services = Post::where('post_type', 'service')->count();
        $clients = Client::count();

        return view('admin.home', compact('admins', 'courses', 'services', 'clients'));
    }

    public function index()
    {
        $admins = Admin::notSuperAdmin()->get();
        return view('admin.admin.index', compact('admins'));
    }

    public function create()
    {
        $randomPassword = str_random('10');
        return view('admin.admin.create', compact('randomPassword'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:191|unique:admins',
            'email' => 'required|string|email|max:191|unique:admins',
            'password' => 'required|string|min:6',
       ]);
        $validator->validate();

        $rols = $this->getRoles($request->input('rols'));

        $admin = Admin::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' =>  Hash::make($request->input('password')),
            'rols' => $rols,
        ]);
        $request->session()->flash('success', 'Created Successfully');
        return Redirect::action('V1\Admin\AdminController@index');
    }

    public function edit(Admin $admin)
    {
        return view('admin.admin.edit', compact('admin'));
    }

    public function update(Admin $admin, Request $request)
    {
        $admin->rols = $this->getRoles($request->input('rols'));
        $admin->save();
        $request->session()->flash('success', 'تم بنجاح');
        return redirect()->route('admins.index');
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();
        return response()->json(['status' => 1]);
    }

    private function getRoles($rols)
    {
        if ($rols) {
            foreach ($rols as $key => $value) {
                $rols[$key] = array_keys($value);
            }
        }
        return $rols;
    }

    public function appSettings(Request $request)
    {
        if ($request->method() == 'POST') {
            $validatedData = $request->validate([
                'app_name' => 'required|string',
                'app_email' => 'required|string',
                'app_form_email' => 'required',
                'app_phone' => 'required',
            ]);

            $updates = [
                'app_name'   => $request->app_name,
                'app_email'   => $request->app_email,
                'app_form_email' => $request->app_form_email,
                'app_phone' => $request->app_phone,
                'app_address_en' => $request->app_address_en ?? '--',
                'app_address_ar' => $request->app_address_ar ?? '--',
                'fb'   => $request->fb ?? '',
                'tw'   => $request->tw ?? '',
                'in'   => $request->in ?? '',
                'gl'   => $request->gl ?? '',
                'ins'   => $request->ins ?? '',
                'yt'   => $request->yt ?? '',
                'chat'   => $request->chat ?? '',
                'map'   => $request->map ?? '',
                'dollar'   => $request->dollar ?? 0,
                'fawaterak'   => $request->fawaterak ?? '',
                'bank_en'   => $request->bank_en ?? 0,
                'bank_ar'   => $request->bank_ar ?? 0,
                'post_en'   => $request->post_en ?? 0,
                'post_ar'   => $request->post_ar ?? 0
            ];

            $file_name = '';
            if ($request->hasFile('image_en')) {
                $path = '/public/images/settings/';
                $file_name = time();
                $ext = $request->image_en->getClientOriginalExtension();
                // If Invalid Extension
                if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png', 'gif'])) {
                    $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                    $request->session()->flash('flash_type', 'alert-danger');
                    return back();
                }

                $fname = $request->image_en->getClientOriginalName();
                $file_name = time();
                $file_name .= $file_name . $fname;
                $request->image_en->move(base_path() . $path, $file_name);
                $updates['image_en'] = $path . $file_name;
            }

            $file_name = '';
            if ($request->hasFile('image_ar')) {
                $path = '/public/images/settings/';
                $file_name = time();
                $ext = $request->image_ar->getClientOriginalExtension();
                // If Invalid Extension
                if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png', 'gif'])) {
                    $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                    $request->session()->flash('flash_type', 'alert-danger');
                    return back();
                }

                $fname = $request->image_ar->getClientOriginalName();
                $file_name = time();
                $file_name .= $file_name . $fname;
                $request->image_ar->move(base_path() . $path, $file_name);
                $updates['image_ar'] = $path . $file_name;
            }

            if ($validatedData) {
                DB::table('settings')->where('id', 1)->update($updates);
                return redirect('admin/settings');
            }
        }
        $settings = DB::table('settings')->first();
        $request->session()->flash('success', 'تم بنجاح!');
        return view('admin.settings', compact('settings'));
    }

    public function getCounts()
    {
        $requestsIds = Prescription::where('status', '=', 'pending')->count();
        $refundsIds  = OrderToPharmacy::where('refund_status', '=', 1)->count();
        return Response()->json(['requests' => $requestsIds, 'refunds' => $refundsIds]);
    }

    public function changeEmail(Request $request)
    {
        $id = auth('admin')->id();
        $admin = Admin::find($id);

        if ($request->method() == 'POST') {
            
            $validator = Validator::make($request->all(), [
                'email' => 'required|unique:admins'
           ]);

            $validator->validate();

            $admin->update(['email' => $request->email]);
            $request->session()->flash('success', 'تم بنجاح!');
            return Redirect('admin/home');
        }
        
        return view('admin.chemail');
    }

    public function changePassword(Request $request)
    {
        $id = auth('admin')->id();
        $admin = Admin::find($id);

        if ($request->method() == 'POST') {
            $messages = [
              'same' => 'New Password and Confirmation password miss match!',
            ];
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|max:191',
                'npassword' => 'required|string|max:191',
                'cpassword' => 'required|string|max:191|same:npassword'
           ], $messages);

            $validator->validate();

            if(!Hash::check($request->password, $admin->password)){
                $validator->errors()->add('field', 'Current password incorrect!');
                $errors = $validator->errors()->all();
                return Redirect('admin/chpassword')->withErrors($errors);
            }

            $admin->update(['password' => Hash::make($request->npassword)]);
            $request->session()->flash('success', 'تم بنجاح!');
            return Redirect('admin/home');
        }
        
        return view('admin.chpassword');
    }

    public function clients(){
      $clients = Client::all();
      
      return view('admin.clients')->with(compact('clients'));
    }

    public function messages(Request $request){
      $msgQuerey = ClientMessages::Select(['client_messages.*', 'clients.name', 'clients.email', 'clients.phone']);

      if($request->vd){
        $vd = ($request->vd == 'y' ? 1 : 0);
        $msgQuerey->where('viewed', $vd);
      }
        
      $msgQuerey->leftJoin('clients', 'clients.id', '=', 'client_messages.client_id')->orderBy('id', 'DESC');
      
      $messages = $msgQuerey->get();

      return view('admin.clients-msgs')->with(compact('messages'));
    }

    public function sendmail(Request $request)
    {
        $cancel = 'messages';

        $message = '';
        $isarray = false;

        if(isset($request->id)){
            $msg = ClientMessages::where('id', $request->id);
            $msg->update(['viewed' => 1]);

            $client = ClientMessages::where('client_messages.id', $request->id)->leftJoin('clients', 'client_messages.client_id', '=', 'clients.id')->first();
            $message = json_decode($client->message, true);
            if(is_array($message)){
                $isarray = true;
                
                if(isset($message['certificates']) && count($message['certificates']))
                  $message['certificates'] = Certificate::whereIn('id', $message['certificates'])->get();

            }else{
                $message = $client->message;
            }
        }
        else if(isset($request->cid)){
            $client = Client::where('clients.id', $request->cid)->first();
            $cancel = 'clients';
        }
        
        return view('admin.clients-form', compact('client', 'cancel', 'message', 'isarray'));
    }

    public function sendaction(Request $request)
    {
        $subject = $request->subject;
        $htmlMessage = $request->message;

        $mails = [];
        $client_id = $request->client_id ?? '';

        if($client_id){
            $client  = Client::where('id', $client_id)->first();
            $mails   = [$client->email];
        }else{
            $clients = Client::all();
            foreach ($clients as $client) {
                $mails[] = $client->email;
            }
        }

        $count = count($mails);
        if($count > 0 && $subject && $htmlMessage){
            for ($i=0; $i < $count; $i++) { 

                $toMail = $mails[$i];

                Mail::send('emails.newsletter', ['data' => $htmlMessage], function ($m) use ($toMail, $subject) {
                    $m->to($toMail)->subject($subject);
                });
            }
        }else{
            \Session::flash('error', 'فشل الإرسال, الإيميل او العنوان او الرسالة غير صحيحة!');
            if($client_id)
                return redirect('admin/clients/mail?id='.$client_id);
            else
                return redirect('admin/clients/mail');
        }
        \Session::flash('success', 'تم الإرسال بنجاح!');
        return redirect('admin/clients');
    }
    
    public function msgdestroy(ClientMessages $message) {
        $message->delete();
        return response()->json(['status' => 1]);
    }

    public function setStatus(Post $post, Request $request) {

        $status = 0;

        if(isset($request->sts)){
            $post->update(['status' => $request->sts]);
            $status = 1;
        }
        return response()->json(['status' => $status]);
    }

    public function client_images(){
      $images = Images::where('typ', 'client')->get();
      return view('admin.client_images.index')->with(compact('images'));
    }

    public function partner_images(){
      $images = Images::where('typ', 'partner')->get();
      return view('admin.partner_images.index')->with(compact('images'));
    }

    public function add_image(Request $request){
        $p_images    = $request->images ?? [];
        $typ    = $request->img_typ;

        //Handle Package Images
        if(count($p_images)){
            $path ='/public/images/'.$typ.'/';

            foreach ($p_images as $image) {
                $file_name = time();
                $ext = $image->getClientOriginalExtension();
                // If Invalid Extension
                if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                    continue;
                }

                $fname = $image->getClientOriginalName();
                $file_name = time();
                $file_name .= $file_name . $fname;
                $full_file_name = $path.$file_name;

                $image->move( base_path() . $path, $file_name);

                Images::create([
                    'image' => $full_file_name,
                    'typ' => $typ
                ]);
            }
        }

        $request->session()->flash('success', 'تم بنجاح!');

        $redirects = ['client' => 'client_images', 'partner' => 'partner_images'];
        return redirect('admin/'.$redirects[$typ]);
    }

    public function delete_image(Images $image)
    {
        $img = $image->image;

        if($img){
            if (file_exists($img)) {
                unlink($img);
            }
            if($image->delete())
                $status = 'success';
            else
                $status = 'failed1';
        }else{
            $status = 'failed2';
        }

        return response()->json([ 'status' => $status ]); 
    }
}
