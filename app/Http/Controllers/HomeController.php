<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Common\MedicineCategory;
use Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){
            $is_mobile = true;
        }else{
            $is_mobile = false;
        }
        $homeCategories = MedicineCategory::where('home_page',1)->limit(6)->get();
        
        return view('client.home', compact('is_mobile','homeCategories'));
    }
    
    public function setLocale($locale)
    {
        $locales = Config::get('app.locales');
        if(in_array($locale, $locales)) {            
            \Session::put('locale', $locale);
        }
        return redirect()->back();
    }
}
