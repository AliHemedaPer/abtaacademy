<?php

namespace App\Http\Controllers\Auth;

use Hash;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $error = ['Invalid'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->redirectTo = env('REDIRECT_AFTER_LOGIN_URL', '/');
    }

    protected function credentials(Request $request)
    {
        $userName = $this->username();
        
        return $request->only($userName, 'password');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages($this->error);
    }

    private function checkUser()
    {
        $user = DB::table('users')->where($this->username(), '=', request($this->username()))->first();
        return !!$user && Hash::check(request('password'), $user->password);
    }

    protected function authenticated(Request $request, $user)
    {
        if ( $user->isAdmin() ) {
            return redirect('admin/home');
        }else if ( $user->isClient() ) {
            return redirect('client/home');
        }

        return redirect('/home');
    }
}
