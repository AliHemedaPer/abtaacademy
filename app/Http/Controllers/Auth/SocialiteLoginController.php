<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialiteLoginController extends Controller
{
    protected $providers = [
        'github',
        'google',
        'facebook',
        'linkedin',
    ];

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider(Request $request, $provider)
    {
        if (!$provider || !in_array($provider, $this->providers)) {
            return redirect()->intended(env('REDIRECT_AFTER_LOGIN_URL', '/'));
        }
        $request->session()->put('provider', $provider);
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Socialite Providers.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        if ($request->session()->has('provider')) {
            $provider = $request->session()->get('provider');
            $request->session()->forget('provider');

            if (!$request->has('error')) {
                $guest = Socialite::driver($provider)->user();
                $user = User::where([
                    'provider_id' => $guest->getId(),
                    'provider_type' => $provider
                ]);
                if (!$user->exists()) {
                    $user = new User;
                    $user->name =  $guest->getName();
                    $user->email = $guest->getEmail();
                    $user->provider_id = $guest->getId();
                    $user->provider_type = $provider;
                    $user->save();
                } else {
                    $user = $user->first();
                }
                Auth::login($user, true);
            }
        }
        return redirect()->intended(env('REDIRECT_AFTER_LOGIN_URL', '/'));
    }
}
