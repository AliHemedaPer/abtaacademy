<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Common\Post;
use App\Models\Common\Banner;
use App\Models\Common\Counters;
use App\Models\Common\Category;
use App\Models\Common\Trainer;
use App\Models\Common\Certificate;
use App\Models\Client\Client;
use App\Models\Client\ClientMessages;
use App\Models\Common\Images;
use App\Models\Common\Gallery;
use App\Models\Common\Favorite;

use App;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Redirect;

class FrontController extends Controller
{

    function fetchcpu(Request $request)
    {
     if($request->get('query'))
     {

      $topics = collect([]);
      $key = $request->get('query');
      $topicesColl  = Post::
                           where(function($query) use ($key){
                              $query->where('title_en', 'LIKE', "{$key}%");
                              $query->orWhere('title_ar', 'LIKE', "{$key}%");
                           })->
                           whereIn('post_type', ['course','page','service'])->
                           whereStatus(1)->
                           latest()->
                           get();
      $topics = $topicesColl->sortBy('post_type')->groupBy('post_type');

      $trainers  = Trainer::where(function($query) use ($key){
                              $query->where('name_en', 'LIKE', "{$key}%");
                              $query->orWhere('name_ar', 'LIKE', "{$key}%");
                           })->
                           latest()->
                           get();

      $categories  = Category::where(function($query) use ($key){
                                  $query->where('name_en', 'LIKE', "{$key}%");
                                  $query->orWhere('name_ar', 'LIKE', "{$key}%");
                               })->
                               latest()->
                               get();

      $output = '';
      foreach($topics as $grp => $topic){
        $output .= '<li class="search-group">&raquo; '. __('front.'.$grp.'s') .'</li>';
        for($i=0;$i <= count($topic) ;$i++){
            if(isset($topic[$i])){

              $url = ($grp == 'page' ? 'pg/'.$topic[$i]->slug : $grp.'/'.$topic[$i]->id);

              $output .= '<li><a href="'.url('/'.$url).'">'.$topic[$i]->title.'</a></li>';
          }
        }
      }

      if(count($categories)){
          $output .= '<li class="search-group">&raquo; '. __('front.categories') .'</li>';
          foreach($categories as $category){
              $url = 'courses/'.$category->id;
              $output .= '<li><a href="'.url('/'.$url).'">'.$category->name.'</a></li>';
          } 
      }

      if(count($trainers)){
          $output .= '<li class="search-group">&raquo; '. __('front.trainers') .'</li>';
          foreach($trainers as $trainer){
              $url = 'trainer/'.$trainer->id;
              $output .= '<li><a href="'.url('/'.$url).'">'.$trainer->name.'</a></li>';
          } 
      }
       
      //dd($topics);
      // $query = $request->get('query');
      // $data = DB::table('cpu')
      //   ->where('name', 'LIKE', "%{$query}%")
      //   ->get(); 
      if($output)
        $ul = '<ul class="dropdown-menu">'.$output.'</ul>';
      else
        $ul = '<ul class="dropdown-menu" style="text-align:center">'.__('front.nomatch').'</ul>';

      echo $ul;
     }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
        $ishome     = 1;
        $banners    = Banner::get();
        $counters   = Counters::get();
        
        $ourValues  = Post::where('post_type', 'our_values')->get();
        $services   = Post::where('post_type', 'service')->latest()->limit(4)->get();
        $courses    = Post::where('post_type', 'course')
                          ->where('offers', 0)
                          ->whereStatus(1)
                          ->where('popular', 0)
                          ->latest()
                          ->limit(6)
                          ->get();

        $offers     = Post::where('post_type', 'course')
                          ->where('offers', 1)
                          ->whereStatus(1)
                          ->latest()
                          ->limit(6)
                          ->get();

        $populars    = Post::where('post_type', 'course')
                          ->where('popular', 1)
                          ->whereStatus(1)
                          ->latest()
                          ->limit(6)
                          ->get();

        $posts      = Post::where('post_type', 'blog')->latest()->limit(4)->get();

        $clients     = Images::where('typ', 'client')->get();

        return view('front.home')->with(compact('ourValues', 'banners', 'services', 'posts', 'counters', 'courses', 'offers', 'populars', 'clients'));
    }

    public function search(Request $request)
    {
        $bkg = rand(1,4);
        
        $key = $request->key ?? '';
        $title = __('front.searchfor').' '.$key;

        $topics = collect([]);

        if($key){
            $topicesColl  = Post::
                         where(function($query) use ($key){
                            $query->orWhere('title_en', 'LIKE', "%{$key}%");
                            $query->orWhere('title_ar', 'LIKE', "%{$key}%");
                            $query->orWhere('content_en', 'LIKE', "%{$key}%");
                            $query->orWhere('content_ar', 'LIKE', "%{$key}%");
                         })->
                         whereIn('post_type', ['course','page','service'])->
                         whereStatus(1)->
                         latest()->
                         get();

            $topics = $topicesColl->sortBy('post_type')->groupBy('post_type');
        }
        //dd($topics);

        return view('front.search')->with(compact('topics', 'bkg', 'key', 'title'));
    }

    public function page($slug)
    {
        $bkg = rand(1,4);


        $data  = Post::where('slug', $slug)->limit(1)->get();

        $bkg_typ = 0;
        if($data[0]->image){
          $bkg_typ = 1;
          $bkg = $data[0]->image;
        }

        $title = $data[0]->title;

        $partners     = Images::where('typ', 'partner')->get();

        return view('front.page')->with(compact('data', 'title', 'bkg', 'bkg_typ', 'partners', 'slug'));
        
    }

    public function contact()
    {
        $bkg = rand(1,4);
        $title = __('front.contact');

        return view('front.contact')->with(compact('bkg', 'title'));
    }

    public function services()
    {
        $bkg = rand(1,4);
        $services = Post::where('post_type', 'service')->get();
        $title = __('front.services');

        return view('front.services')->with(compact('services', 'title', 'bkg'));
    }

    public function service(Post $service)
    {
        $bkg = rand(1,4);
        $title = $service->title;

        $courses = Post::where('post_type', 'course')->
                        whereStatus(1)->
                        latest()->
                        limit(5)->
                         get();

        $posts = Post::where('post_type', 'blog')->
                       orderBy('updated_at', 'DESC')->
                       limit(5)->
                       get();

        return view('front.service')->with(compact('service', 'title', 'bkg', 'courses', 'posts'));
    }

    public function courses(Category $category)
    {
        $cat_ids  = [ 0 => $category->id /*, 1 => $category->parentObj->id ?? 0*/ ];
        $children = $category->children->pluck('id')->toArray();
        $cat_ids = array_merge($cat_ids, $children);
        
        $bkg = rand(1,4);
        $courses = Post::whereIn('category', $cat_ids)->whereStatus(1)->where('post_type', 'course')->paginate(20);
        $trainers = Trainer::whereIn('category', $cat_ids)->get();
        
        $title = $category->name;

        return view('front.courses')->with(compact('category', 'courses', 'title', 'bkg', 'trainers'));
    }

    public function course(Post $course)
    {
        $bkg = rand(1,4);
        $title = $course->title;

        if(!$course->status)
          return redirect()->route('home');

        $toFav = 0;
        $toLib = 0;

        if(auth('client')->check()){
          $user =  auth('client')->user();

          $toFav = Favorite::where('client_id', $user->id)
                        ->where('type', 'favorite')
                        ->where('post_id', $course->id)
                        ->count();

          $toLib = Favorite::where('client_id', $user->id)
                        ->where('type', 'library')
                        ->where('post_id', $course->id)
                        ->count();
        }
        

        $category  = $course->category;
        $cat_ids  = [ 0 => $category ];

        $parent = Category::find($category);
        
        $local = App::getLocale();
        $allCertiicates = Certificate::select('id', 'title_'.$local)->pluck('title_'.$local, 'id')->toArray();

        $extra = json_decode($course->extra_info, true);
        $certificates = json_decode($course->certificates, true);

        if ($parent) {
           $children = $parent->children->pluck('id')->toArray();
            $cat_ids = array_merge($cat_ids, $children);
        }
        
        $courses = Post::whereIn('category', $cat_ids)->
                         where('id', '!=', $course->id)->
                         where('post_type', 'course')->
                         whereStatus(1)->
                         limit(4)->
                         get();

        // $posts = Post::where('post_type', 'blog')->
        //                latest()->
        //                limit(5)->
        //                get();

        return view('front.course')->with(compact('course', 'title', 'bkg', 'courses', 'extra', 'certificates', 'allCertiicates', 'toFav', 'toLib'));
    }

    public function courseRequest(Post $course)
    {
        $bkg = rand(1,4);
        $title = $course->title;

        if(!auth('client')->check()){
            return Redirect::to('/login/'.$course->id);
            //return redirect()->route('client.login');
        }

        $appSettings = DB::table('settings')->select(['dollar'])->where('id', 1)->first();
        $dollar = $appSettings->dollar ?? 1;

        $extra = json_decode($course->extra_info, true);
        $cert_ids = json_decode($course->certificates, true);
        $certificates = []; 
        if(count($cert_ids))
          $certificates = Certificate::whereIn('id', $cert_ids)->get();

        return view('front.request_form')->with(compact('course', 'extra', 'title', 'bkg', 'certificates', 'dollar'));
    }

    public function certificate(Certificate $certificate)
    {
        return view('front.certificate')->with(compact('certificate'));
    }

    public function trainers()
    {
        $trainers = Trainer::paginate(20);
        
        return view('front.trainers')->with(compact('trainers'));
    }

    public function trainer(Trainer $trainer)
    {
        $bkg = rand(1,4);

        $category  = $trainer->Category;
        
        $courses = Post::where('trainer', $trainer->id)->
                         where('post_type', 'course')->
                         whereStatus(1)->
                         limit(5)->
                         get();

        return view('front.trainer')->with(compact('category', 'trainer', 'courses'));
    }

    public function blog()
    {
        $bkg = rand(1,4);
        $posts = Post::where('post_type', 'blog')->orderBy('updated_at', 'DESC')->paginate(9);
        $title = __('front.blog');

        return view('front.blog')->with(compact('posts', 'title', 'bkg'));
    }

    public function post(Post $post)
    {
        $bkg = rand(1,4);
        $title = $post->title;

        $courses = Post::where('post_type', 'course')->
                         whereStatus(1)->
                         latest()->
                         limit(5)->
                         get();

        $posts = Post::where('id', '!=', $post->id)->
                       where('post_type', 'blog')->
                       orderBy('updated_at', 'DESC')->
                       limit(5)->
                       get();

        return view('front.blogpost')->with(compact('title', 'post', 'bkg', 'courses', 'posts'));
    }

    public function servicemail(Request $request){

        $typ = $request->typ;

        $appSettings = DB::table('settings')->select(['app_form_email'])->where('id', 1)->first();
        $toMail = $appSettings->app_form_email;

        if($typ == 'service')
        {
            $title  = $request->title;
            $data = ['title' => $title, 'email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'message' => $request->message];

            /*$mail = Mail::send('emails.service', ['data' => $data], function ($m) use ($toMail, $title) {
                $m->to($toMail)->subject($title.' - Request!');
            });*/
             $dataClient = [
                            'client_id' => auth('client')->user()->id,
                            'title'     => $request->title,
                            'message'   => $request->message
                           ];
            try{
                $client = ClientMessages::insert($dataClient);
            }catch(Exception $e){}

        }
        else if($typ == 'contact')
        {
            $subject = $request->title;
            $data = ['email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'subject' => $subject, 'message' => $request->message];
            $mail = Mail::send('emails.contact', ['data' => $data], function ($m) use ($toMail, $subject) {
                $m->to($toMail)->subject($subject);
            });
        }

        //if($mail) {
            $msgStatus = 1;
        //}
        return response()->json([
            'status' => $msgStatus
        ]);
    }

    public function downloads()
    {
        $downloads = Post::whereStatus(1)
                         ->where('post_type', 'course')
                         ->whereNotNull('brochure')
                         ->paginate(10);

        return view('front.downloads')->with(compact('downloads'));
    }

    public function videos()
    {
        $bkg = rand(1,4);
        $title = __('front.videos');

        $videos = Post::whereStatus(1)
                      ->where('post_type', 'course')
                      ->whereNotNull('video')
                      ->orwhereNotNull('video_url')
                      ->paginate(9);

        return view('front.videos')->with(compact('videos', 'title', 'bkg'));
    }

    public function gallery()
    {
        $bkg = rand(1,4);
        $title = __('front.gallery');

        $gallery = Gallery::all();

        return view('front.gallery')->with(compact('gallery', 'title', 'bkg'));
    }

    public function galleryImages(Gallery $gallery)
    {
        $bkg = rand(1,4);
        $title = $gallery->title;

        $images = Images::where('parent_id', $gallery->id)->get();

        return view('front.gallery_images')->with(compact('images', 'title', 'bkg'));
    }

    public function requestaction (Post $course, Request $request)
    {   
        $data = $request->all();
        $client_id = auth('client')->user()->id;
        
        if(!$course){
            $request->session()->flash('error', 'Course Not Found!');
            return redirect('course/'.$course->id.'/request');
        }
       
       $appSettings = DB::table('settings')->select(['app_form_email'])->where('id', 1)->first();
        $toMail = $appSettings->app_form_email;

        //Price Calc
        $appSettings = DB::table('settings')->where('id', 1)->first();

        $extra = json_decode($course->extra_info, true);
        $total =  isset($extra['fees']) ? (isset($extra['finalfees']) && $extra['finalfees'] < $extra['fees'] ) ? number_format($extra['finalfees'],2) : number_format($extra['fees'],2) : 0;


        $cert_ids = $request->certificates ?? [];
        $cert_price = 0;
        if(count($cert_ids)){
          $dollar = $appSettings->dollar ?? 1;
          $certificates = Certificate::whereIn('id', $cert_ids)->sum('price');
          $cert_price = $certificates * $dollar; 
        }
        
        $data['total'] = number_format(($total + $cert_price), 2);
        $data['title'] = $course->title_ar;
        
        if($data['payment'] == 'Fawaterak'){
            $postData = [
                'vendorKey' => $appSettings->fawaterak,
                'cartItems' => [ array(
                                        'name'     => $data['title'], 
                                        'price'    => $data['total'], 
                                        'quantity' => 1
                                    ) 
                               ],
                'cartTotal' => $data['total'], 
                'shipping' => 0,
                'customer' => [ 'first_name' => $data['quote_name_en'], 
                                'last_name'  => '-', 
                                'email'      => $data['quote_email'], 
                                'phone'      => $data['quote_phone'], 
                                'address'    => 'address'
                              ],
                'redirectUrl' => url('course/'.$course->id.'/request'),
                'currency' => 'EGP'
            ];
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://app.fawaterk.com/api/invoice",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($postData)
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $resArr = json_decode($response, true);

            //$payment = 1;
            if(isset($resArr['invoiceKey'])){

                $data['invoiceKey'] = $resArr['invoiceKey'];
                $dataClient = [
                    'client_id'  => $client_id,
                    'title' => $course->title_ar,
                    'message' => json_encode($data)
                  ];

                $client = ClientMessages::insert($dataClient);
                $request->session()->flash('success', '!');
                return redirect('course/'.$course->id.'/request');
                //return redirect($resArr['url']);
            }else{
               $request->session()->flash('error', 'Payment Error! '.$response);
               return redirect('course/'.$course->id.'/request');
            }
        }
        /*$mail = Mail::send('emails.service', ['data' => $data], function ($m) use ($toMail, $title) {
            $m->to($toMail)->subject($title.' - Request!');
        });*/
        $dataClient = [
                    'client_id'  => $client_id,
                    'title' => $course->title_ar,
                    'message' => json_encode($data)
                  ];
        try{
            $client = ClientMessages::insert($dataClient);
        }catch(Exception $e){}

       $request->session()->flash('success', '!');
       return redirect('course/'.$course->id.'/request');
    }

    public function profile(Request $request){

      if(!auth('client')->check()){
          return redirect()->route('client.login');
      }

      $bkg = rand(1,4);
      $title = __('front.profile');

      $user =  auth('client')->user();

      return view('front.profile')->with(compact('user', 'bkg', 'title'));
    }

    public function mylibrary(Request $request)
    {
        $bkg = rand(1,4);

        if(!auth('client')->check()){
          return redirect()->route('client.login');
        }
        $user =  auth('client')->user();

        $pids = Favorite::where('client_id', $user->id)
                        ->where('type', 'library')
                        ->pluck('post_id')
                        ->toArray();

        $courses = Post::whereStatus(1)
                       ->whereIn('id', $pids)
                       ->where('post_type', 'course')
                       ->paginate(20);
        
        $title = __('front.mylib');
        $type  = 'library';

        return view('front.mycourses')->with(compact('courses', 'title', 'bkg', 'type'));
    }

    public function myfavorites(Request $request)
    {
        $bkg = rand(1,4);

        if(!auth('client')->check()){
          return redirect()->route('client.login');
        }
        $user =  auth('client')->user();

        $pids = Favorite::where('client_id', $user->id)
                        ->where('type', 'favorite')
                        ->pluck('post_id')
                        ->toArray();

        $courses = Post::whereStatus(1)
                       ->whereIn('id', $pids)
                       ->where('post_type', 'course')
                       ->paginate(20);
        
        $title = __('front.myfav');
        $type  = 'favorite';

        return view('front.mycourses')->with(compact('courses', 'title', 'bkg', 'type'));
    }

    public function addtomy(Request $request){

      if(!auth('client')->check() || !$request->id){
         return Response()->make(['status' => 'failure']);
      }

      $data = [
                'client_id' => auth('client')->user()->id,
                'post_id'   => $request->id,
                'type' => $request->typ ?? 'favorite'
              ];

      $fav = Favorite::insert($data);
      if($fav)
        return Response()->make(['status' => 'success']);

      return Response()->make(['status' => 'failure']);
    }

    public function removefrommy(Request $request){

      if(!auth('client')->check() || !$request->id || !$request->typ){
         return Response()->make(['status' => 'failure']);
      }

      $fav = Favorite::where('client_id', auth('client')->user()->id)
                     ->where('post_id', $request->id)
                     ->where('type', $request->typ)
                     ->delete();
      if($fav){
        return Response()->make(['status' => 'success']);
      }
      return Response()->make(['status' => 'failure']);
    }
}
