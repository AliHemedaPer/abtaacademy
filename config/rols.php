<?php

return [

    'admin' => [
        'pharmacy' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'medicin' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'admin' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'units' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'material' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'percentage' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'category' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'uses' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'prescription' => [
            'create',
            'read',
            'update',
            'delete'
        ],'settings' => [
            'update'
        ],'wallet' => [
            'read'
        ],'refunds' => [
            'read'
        ]

    ]


];
