<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Guest Available Routes */
Route::get('/', 'FrontController@index')->name('home');
Route::get('/home', 'FrontController@index')->name('home');

Route::get('/pg/{slug}', 'FrontController@page')->name('page');
Route::get('/services', 'FrontController@services')->name('services');
Route::get('/service/{service}', 'FrontController@service')->name('service');
Route::get('/courses/{category}', 'FrontController@courses')->name('courses');
Route::get('/course/{course}', 'FrontController@course')->name('course');
Route::get('/course/{course}/request', 'FrontController@courseRequest')->name('courseRequest');
Route::get('/contact', 'FrontController@contact')->name('contect');
Route::get('/blog', 'FrontController@blog')->name('blog');
Route::get('/post/{post}', 'FrontController@post')->name('post');
Route::get('/search', 'FrontController@search')->name('search');
Route::get('/trainers', 'FrontController@trainers')->name('trainers');
Route::get('/trainer/{trainer}', 'FrontController@trainer')->name('trainer');
Route::get('/certificate/{certificate}', 'FrontController@certificate')->name('certificate');
Route::get('/downloads', 'FrontController@downloads')->name('downloads');
Route::get('/videos', 'FrontController@videos')->name('videos');
Route::get('/gallery', 'FrontController@gallery')->name('gallery');
Route::get('/gallery/{gallery}', 'FrontController@galleryImages')->name('galleryImages');

Route::get('/profile', 'FrontController@profile')->name('profile');
Route::get('/mylibrary', 'FrontController@mylibrary')->name('mylibrary');
Route::get('/myfavorites', 'FrontController@myfavorites')->name('myfavorites');
Route::post('/addtomy','FrontController@addtomy')->name('addtomy');
Route::delete('/removefrommy','FrontController@removefrommy')->name('removefrommy');

Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
});
Route::post('/servicemail', 'FrontController@servicemail')->name('servicemail');
Route::post('/contactmail', 'FrontController@contactmail')->name('contactmail');

Route::post('/autocomplete/fetchcpu','FrontController@fetchcpu')->name('autocomplete.fetchcpu');

/** Admin Auth Routes */
Route::get('admin/login', 'V1\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'V1\Auth\AdminLoginController@login')->name('admin.login.submit');

Route::get('admin/reset', 'V1\Auth\AdminLoginController@resetPassword')->name('admin.reset');
Route::post('admin/reset', 'V1\Auth\AdminLoginController@resetPassword')->name('admin.reset.submit');
Route::post('admin/logout', 'V1\Auth\AdminLoginController@logout')->name('admin.logout');

/** Client Auth Routes */
Route::get('register', function () {
	$bkg = rand(1,4);
	$title = __('front.register');
    return view('front.register')->with(compact('bkg', 'title'));
});
Route::post('register', 'V1\\Client\\ClientController@registerAction');

Route::get('login/{course?}', function ($course = 0) {
	$bkg = rand(1,4);
	$title = __('front.login');
	
    return view('front.login')->with(compact('bkg', 'title', 'course'));
})->name('client.login');

Route::get('resetpassword', 'V1\\Client\\ClientController@resetPasswordView')->name('client.resetpassword');
Route::post('login', 'V1\\Client\\ClientController@UserLogin');
Route::get('logout', 'V1\\Client\\ClientController@logout')->name('client.logout');
Route::post('resetpassword', 'V1\\Client\\ClientController@resetPassword');

Route::post('/requestaction/{course}', 'FrontController@requestaction')->name('requestaction');