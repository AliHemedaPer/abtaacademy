<?php
Route::get('/home', 'Admin\\AdminController@home')->name('admin.home');
Route::get('/', 'Admin\\AdminController@home')->name('admin.home');

Route::match(['get', 'post'], '/chpassword', 'Admin\\AdminController@changePassword');
Route::match(['get', 'post'], '/chemail', 'Admin\\AdminController@changeEmail');

Route::group(['middleware' => ['rols'], 'rols' => ['settings' => ['update']]], function () {
    Route::match(['get', 'post'], '/settings', 'Admin\AdminController@appSettings');
});


/** Posts Route  */
Route::group(['middleware' => ['rols'], 
              'service'    => ['service' => ['read']],
              'page'       => ['page' => ['read']],
              'blog'       => ['blog' => ['read']],
              'our_values' => ['our_values' => ['read']],
              'course'     => ['course' => ['read']]
             ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['index']]);
});
Route::post('setStatus/{post}', 'Admin\AdminController@setStatus');

Route::group(['middleware' => ['rols'], 
              'service'    => ['service' => ['create']],
              'page'       => ['page' => ['create']],
              'blog'       => ['blog' => ['create']],
              'our_values' => ['our_values' => ['create']],
              'course'     => ['course' => ['create']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['create']]);
});

Route::group(['middleware' => ['rols'], 
              'service'    => ['service' => ['create']],
              'page'       => ['page' => ['create']],
              'blog'       => ['blog' => ['create']],
              'our_values' => ['our_values' => ['create']],
              'course'     => ['course' => ['create']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['store']]);
});

Route::group(['middleware' => ['rols'], 
              'service'    => ['service' => ['update']],
              'page'       => ['page' => ['update']],
              'blog'       => ['blog' => ['update']],
              'our_values' => ['our_values' => ['update']],
              'course'     => ['course' => ['update']]
             ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['edit']]);
});

Route::group(['middleware' => ['rols'], 
              'service'    => ['service' => ['update']],
              'page'       => ['page' => ['update']],
              'blog'       => ['blog' => ['update']],
              'our_values' => ['our_values' => ['update']],
              'course'     => ['course' => ['update']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['update']]);
});

Route::group(['middleware' => ['rols'], 
              'service'    => ['service' => ['delete']],
              'page'       => ['page' => ['delete']],
              'blog'       => ['blog' => ['delete']],
              'our_values' => ['our_values' => ['delete']],
              'course'     => ['course' => ['delete']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['destroy']]);
});

// Route::group(['middleware' => ['rols']], function () {
//     Route::patch('/posts/toggle_status/{post}', 'Admin\PostController@toggleStatus');
// });


/** Categories Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['category' => ['read']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['index']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['category' => ['create']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['create']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['category' => ['create']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['store']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['category' => ['update']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['category' => ['delete']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['category' => ['update']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['update']]);
});


/** Admins Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['read']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['index']]);
});

Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['create']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['create']]);
});

Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['create']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['store']]);
});

Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['update']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['edit']]);
});

Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['delete']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['destroy']]);
});

Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['update']]], function () {
    Route::patch('/admins/toggle_status/{admin}', 'Admin\AdminController@toggleStatus');
});

Route::group(['middleware' => ['rols'], 'rols' => ['admin' => ['update']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['update']]);
});

/** Banners Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['banner' => ['read']]], function () {
    Route::resource('/banners', 'Admin\BannerController', ['only' => ['index']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['banner' => ['create']]], function () {
    Route::resource('/banners', 'Admin\BannerController', ['only' => ['create']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['banner' => ['create']]], function () {
    Route::resource('/banners', 'Admin\BannerController', ['only' => ['store']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['banner' => ['update']]], function () {
    Route::resource('/banners', 'Admin\BannerController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['banner' => ['delete']]], function () {
    Route::resource('/banners', 'Admin\BannerController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['banner' => ['update']]], function () {
    Route::resource('/banners', 'Admin\BannerController', ['only' => ['update']]);
});

/** Trainers Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['read']]], function () {
    Route::resource('/trainers', 'Admin\TrainerController', ['only' => ['index']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['create']]], function () {
    Route::resource('/trainers', 'Admin\TrainerController', ['only' => ['create']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['create']]], function () {
    Route::resource('/trainers', 'Admin\TrainerController', ['only' => ['store']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['update']]], function () {
    Route::resource('/trainers', 'Admin\TrainerController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['delete']]], function () {
    Route::resource('/trainers', 'Admin\TrainerController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['update']]], function () {
    Route::resource('/trainers', 'Admin\TrainerController', ['only' => ['update']]);
});

/** Crtificates Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['read']]], function () {
    Route::resource('/certificates', 'Admin\CertificateController', ['only' => ['index']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['create']]], function () {
    Route::resource('/certificates', 'Admin\CertificateController', ['only' => ['create']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['create']]], function () {
    Route::resource('/certificates', 'Admin\CertificateController', ['only' => ['store']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['update']]], function () {
    Route::resource('/certificates', 'Admin\CertificateController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['delete']]], function () {
    Route::resource('/certificates', 'Admin\CertificateController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['trainer' => ['update']]], function () {
    Route::resource('/certificates', 'Admin\CertificateController', ['only' => ['update']]);
});

/** Counters Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['read']]], function () {
    Route::resource('/counters', 'Admin\CountersController', ['only' => ['index']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['create']]], function () {
    Route::resource('/counters', 'Admin\CountersController', ['only' => ['create']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['create']]], function () {
    Route::resource('/counters', 'Admin\CountersController', ['only' => ['store']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['update']]], function () {
    Route::resource('/counters', 'Admin\CountersController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['delete']]], function () {
    Route::resource('/counters', 'Admin\CountersController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['update']]], function () {
    Route::resource('/counters', 'Admin\CountersController', ['only' => ['update']]);
});

/** Gallery Route  */
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['read']]], function () {
    Route::resource('/gallery_images', 'Admin\GalleryController', ['only' => ['index']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['create']]], function () {
    Route::resource('/gallery_images', 'Admin\GalleryController', ['only' => ['create']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['create']]], function () {
    Route::resource('/gallery_images', 'Admin\GalleryController', ['only' => ['store']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['update']]], function () {
    Route::resource('/gallery_images', 'Admin\GalleryController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['delete']]], function () {
    Route::resource('/gallery_images', 'Admin\GalleryController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['rols'], 'rols' => ['counter' => ['update']]], function () {
    Route::resource('/gallery_images', 'Admin\GalleryController', ['only' => ['update']]);
});

Route::get('getcounts', 'Admin\\AdminController@getCounts');
Route::get('clients', 'Admin\\AdminController@clients');
Route::get('messages', 'Admin\\AdminController@messages');
Route::delete('messages/{message}', 'Admin\\AdminController@msgdestroy');
Route::get('/messages/mail', 'Admin\AdminController@sendmail')->name('sendmail');
Route::post('/messages/mail', 'Admin\AdminController@sendaction')->name('sendaction');

Route::get('/client_images', 'Admin\AdminController@client_images')->name('client_images');
Route::get('/partner_images', 'Admin\AdminController@partner_images')->name('partner_images');
Route::post('/add_image', 'Admin\AdminController@add_image')->name('add_image');
Route::delete('/image/{image}', 'Admin\AdminController@delete_image')->name('add_image');