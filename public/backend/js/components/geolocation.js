var Maps = function() {
    // In the following example, markers appear when the user clicks on the map.
    // The markers are stored in an array.
    // The user can then click an option to hide, show or delete the markers.
    var map,
        markers = [],
        addrField,
        mapField,
        latField,
        longField;

    function initMap() {
        addrField = document.getElementById('address')
        mapField = document.getElementById('map')
        latField = document.getElementById('lat')
        longField = document.getElementById('long')

        geocoder = new google.maps.Geocoder();
        var lat = latField && latField.value ? parseFloat(latField.value) : 30.037085310164322
        var lng = longField && longField.value ? parseFloat(longField.value) : 31.239552484750448
        var haightAshbury = {
            lat: lat,
            lng: lng
        };

        map = new google.maps.Map(mapField, {
            zoom: 8, // Set the zoom level manually
            center: haightAshbury,
            mapTypeId: 'terrain'
        });
        addMarker(haightAshbury);
        //getAddress(haightAshbury);
        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
            if (markers.length >= 1) {
                deleteMarkers();
            }

            addMarker(event.latLng);
            //getAddress(event.latLng);

        }, false);
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markers.push(marker);
        latField.value = typeof location.lat === "number" ? location.lat : typeof location.lat === "function" ? location.lat() : "";
        longField.value = typeof location.lng === "number" ? location.lng : typeof location.lng === "function" ? location.lng() : "";
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    function getAddress(latLng) {
        geocoder.geocode({
            'latLng': latLng
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    addrField.value = results[1].formatted_address;
                }
            } else {
                //alert('Geocoder failed due to: ' + status);
            }
        }, function() {
            //alert('Geocoder failed due to: ' + status);
        });
    }

    return {
        init: function() {
            initMap()
        }
    }
}();
