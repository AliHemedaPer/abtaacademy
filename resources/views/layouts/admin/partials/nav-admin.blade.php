<div class="card card-sidebar-mobile">
    <ul class="nav nav-sidebar" data-nav-type="accordion">

        <!-- Main -->
        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">@lang('admin.main')</div> <i class="icon-menu" title="Main"></i></li>
        <li class="nav-item">
            <a href="{{ url('admin/home') }}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'home') !== false)) active @endif">
                <i class="icon-home4"></i>
                <span>@lang('admin.dashboard')</span>
            </a>
        </li>

        @if(auth('admin')->user()->canRead('category'))
        <li class="nav-item">
            <a href="{{url('admin/categories')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'categories') !== false)) active @endif"><i class="icon-color-sampler"></i> <span>@lang('admin.categories')</span></a>
        </li>
        @endif

        @if(auth('admin')->user()->canRead('service'))
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=service')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'service') !== false)) active @endif"><i class="icon-magic-wand2"></i> <span>@lang('admin.services')</span></a>
        </li>
        @endif

        @if(auth('admin')->user()->canRead('course'))
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=course')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'course') !== false)) active @endif"><i class="icon-book"></i> <span>@lang('admin.courses')</span></a>
        </li>
        @endif

        @if(auth('admin')->user()->canRead('page'))
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=page')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'page') !== false)) active @endif"><i class="icon-files-empty2"></i> <span>@lang('admin.pages')</span></a>
        </li>
        @endif

        @if(auth('admin')->user()->canRead('our_values'))
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=our_values')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'our_values') !== false)) active @endif"><i class="icon-stars"></i> <span>@lang('admin.our_values')</span></a>
        </li>
        @endif

        @if(auth('admin')->user()->canRead('blog'))
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=blog')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'blog') !== false)) active @endif"><i class="icon-blog"></i> <span>@lang('admin.blogs')</span></a>
        </li>
        @endif
        
        <li class="nav-item nav-item-submenu nav-item-expanded">
            <a href="#" class="nav-link @if( (strpos($_SERVER['REQUEST_URI'], '/banners') !== false) || (strpos($_SERVER['REQUEST_URI'], '_images') !== false)) active @endif"><i class="icon-images2"></i> <span>الصور</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="File uploaders" style="display: none;">
                @if(auth('admin')->user()->canRead('banner'))
                    <li class="nav-item">
                        <a href="{{url('admin/banners')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/banners') !== false)) active @endif"><i class="icon-images2"></i> <span> @lang('admin.banners')</span></a>
                    </li>
                @endif
                <li class="nav-item">
                    <a href="{{url('admin/gallery_images')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/gallery_images') !== false)) active @endif"><i class="icon-images2"></i> <span>معرض الصور</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/client_images')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/client_images') !== false)) active @endif"><i class="icon-images2"></i> <span> صور العملاء</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/partner_images')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/partner_images') !== false)) active @endif"><i class="icon-images2"></i> <span>صور الشركاء</span></a>
                </li>
            </ul>
        </li>

        

        @if(auth('admin')->user()->canRead('trainer'))
            <li class="nav-item">
                <a href="{{url('admin/trainers')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/trainers') !== false)) active @endif"><i class="icon-man"></i> <span> @lang('admin.trainers')</span></a>
            </li>
        @endif

        @if(auth('admin')->user()->canRead('certificate'))
            <li class="nav-item">
                <a href="{{url('admin/certificates')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/certificates') !== false)) active @endif"><i class="icon-certificate"></i> <span> @lang('admin.certificates')</span></a>
            </li>
        @endif

        @if(auth('admin')->user()->canRead('counter'))
            <li class="nav-item">
                <a href="{{url('admin/counters')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/counters') !== false)) active @endif"><i class="icon-list-numbered"></i> <span> @lang('admin.counters')</span></a>
            </li>
        @endif

        @if(auth('admin')->user()->canRead('admin'))
            <li class="nav-item">
                <a href="{{url('admin/admins')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/admins') !== false)) active @endif"><i class="icon-user-tie"></i> <span> @lang('admin.admins')</span></a>
            </li>
        @endif

        @if(auth('admin')->user()->canRead('clients'))
            <li class="nav-item">
                <a href="{{url('admin/clients')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/clients') !== false)) active @endif"><i class="icon-users4"></i> <span> العملاء</span></a>
            </li>
        @endif

        @if(auth('admin')->user()->canRead('messages'))
            <li class="nav-item">
                <a href="{{url('admin/messages')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/messages') !== false)) active @endif"><i class="icon-envelop"></i> <span> الرسائل والطلبات</span></a>
            </li>
        @endif

        <!-- /main -->

    </ul>
</div>
