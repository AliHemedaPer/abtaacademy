 @if ($category->home_page)
    <li class="{{ (( $share_locale == 'en' && $level > 1) || ( $share_locale == 'ar' )) ? 'drop-left' : '' }}">

        {{--
            @if( (count($category->children) > 0 || count($category->page) > 0) )
                <a href="#">{{ $category->name }}</a>
            @else
                <a href="{{ url('courses/'.$category->id) }}">{{ $category->name }}</a>
            @endif
        --}}

        <a href="{{ $category->clickable ? url('courses/'.$category->id) : '#' }}">
            {{ $category->name }}
            @if($level == 1)
            <br/>
            <span class="hint">{{ $category->short_desc }}</span>
            @endif 
        </a>
        

        @if ($category->children->count() > 0 || $category->page->count() > 0)
            <ul>
            @if ($category->page->count() > 0)
                @foreach($category->page as $page)
                    <a href="{{ url('pg/'.$page->slug) }}">{{ $page->title }}</a>
                @endforeach
            @endif 

            @if ($category->children->count() > 0)
                @foreach($category->children as $category)
                    @include('layouts.menu-categories', ['category' => $category, 'level' => $level+1])
                @endforeach
            @endif 

            @if($category->id == 21)
                <a href="{{ url('/services') }}">@lang('front.services')</a>
                <a href="{{ url('/trainers') }}">@lang('front.trainers')</a>
                <a href="{{ url('/videos') }}">@lang('front.videos')</a>
                <a href="{{ url('/gallery') }}">@lang('front.gallery')</a>
                <a href="{{ url('/downloads') }}">@lang('front.downloads')</a>
            @endif 
            </ul>
        @endif

       {{--
        @if ($category->page->count() > 0)
        	<ul>
            @foreach($category->page as $page)
                <a href="{{ url('pg/'.$page->slug) }}">{{ $page->title }}</a>
            @endforeach
            </ul>
        @endif    
        --}}    
    </li>
@endif