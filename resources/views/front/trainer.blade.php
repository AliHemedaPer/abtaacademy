@extends('front.front')

@section('title')
{{ $trainer->name }}
@endsection

@section('styles')
<style type="text/css">
  .fa{
    color: #569fda;
  }
</style>
@endsection

@section('content')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7">
      <div class="container">
        <div class="row">

          <div class="col-md-8 col-md-8-sp">
            <div class="row">
              <div class="col-md-12 animate-box">
                <div class="classes class-single">
                  <div>
                    <div class="course-thumb" style="border-radius: 50%; width: 150px; height: 150px">
                      <a href="#" style="background-image: url({{ url($trainer->image) }});">
                      </a>
                    </div>
                    <div class="course-info">
                      <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">{{ $trainer->name }}</h3>
                      <strong>{{ $trainer->title }}</strong>
                      <br/>
                      {{ $trainer->short_desc }}
                      <div style="padding-top: 10px; font-size: 14px">
                        @lang('front.category'): <a href="{{ url('/courses/'.$trainer->Category->id) }}">{{ $trainer->Category->name }}</a>
                      </div>
                    </div>
                    <div style="clear: both;"></div>
                  </div>

                  <div class=" desc2" style="padding-top: 20px">
                    <h4 style="font-size: 18px; color: #15254c">@lang('front.cv')</h4>
                    {!! $trainer->description !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-md-4-sp video-holder cust-side-bar">
         
            @if($trainer->Category->content)
              @if($share_locale == 'ar')
                <h3>لماذا تلتحق بالأكاديمية الأمريكية؟</h3>
              @else
                <h3>Why joining the ABTA Academy?</h3>
              @endif
              {{-- <h3>@lang('front.whyjoin') {{ $trainer->Category->name }}?</h3> --}}
              <div style="margin-bottom: 30px">
                {{-- {!! $trainer->Category->content !!} --}}
                @if($share_locale == 'ar')
                  {!! $menuWhy->short_desc_ar !!}
                @else
                  {!! $menuWhy->short_desc_en !!}
                @endif
              </div>
            @endif
            
          </div>
          
        </div>

        @if($courses->count() > 0)
              <h3>@lang('front.courses')</h3>
              @foreach($courses as $course)
                <div class="col-md-4 f-blog animate-box hoverShadow" style="border: 1px solid #efefef;{{ $share_locale == 'ar' ? 'padding-right: 100px;padding-left: 10px;float: right;' : 'padding-left: 100px;padding-right: 10px' }}">
                  <a href="{{ url('/course/'.$course->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$course->image) }});width: 70px;height: 52px;">
                  </a>
                  <div class="desc">
                    <h2 style="font-size: 12px;"><a href="{{ url('/course/'.$course->id) }}">{{ $course->title }}</a></h2>
                    <p class="admin" style="margin-bottom: 5px;"><span>
                      {{ Carbon::parse($course->updated_at)->format('d M Y') }}
                    </span></p>
                  </div>
                </div>
              @endforeach
              <br/><br/>
            @endif

      </div>  
    </div>
@stop