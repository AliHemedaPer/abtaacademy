@extends('front.front')

@section('title')
{{ $title }}
@endsection


@section('content')

@include('front.inner-banner')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7">
      <div class="container">
        <div class="row">

          <div class="col-md-8 col-md-8-sp">
            <div class="row">
              <div class="col-md-12 animate-box">
                <div class="classes class-single">
                  <div>
                    <div class="course-info">
                      <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">{{ $user->name }}</h3>
                      <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">{{ $user->email }}</h3>
                      <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">{{ $user->phone }}</h3>
                    </div>
                    <div style="clear: both;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-md-4-sp video-holder cust-side-bar">
            <a href="{{ url('/mylibrary') }}" class="btn btn-primary btn-effect" style="border-radius: 0;border-bottom: 4px solid #1b5696;width: 100%">@lang('front.mylib')</a>
            <a href="{{ url('/myfavorites') }}" class="btn btn-primary btn-effect" style="border-radius: 0;border-bottom: 4px solid #1b5696;width: 100%">@lang('front.myfav')</a>
            <br/>
            <br/>
          </div>
        </div>
      </div>  
    </div>
@stop