@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-blog colorlib-light" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }}) "  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row" style="padding-top: 50px;">
          @foreach($services as $service)
          <div class="col-md-3 text-center animate-box">
            <div class="services hoverShadow">
              <span class="icon">
                {{ $service->title }}
              </span>
              <div class="desc">
                
                <p>{{ $service->short_desc }}</p>

                <p><a href="{{ url('service/'.$service->id) }}" class="btn btn-danger">@lang('front.more')</a></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>

@stop