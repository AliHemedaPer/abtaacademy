@extends('front.front')

@section('title')
{{ $category->name }}
@endsection

@section('content')

<div class="colorlib-blog colorlib-light-grey" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7 "  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row" style="padding-top: 20px;">
          <div class="col-md-8 col-md-8-sp">

            <h3 style="font-size: 22px;color: #15254c; margin-bottom: 5px;">@lang('front.cources_in') {{ $category->name }}</h3>

            @if(isset($category->short_desc))
              <h4 class="title-border">{{ $category->short_desc }}</h4>
            @endif

            @if($courses->count() > 0)
              @foreach($courses as $course)

              <div class="f-blog animate-box hoverShadow" style="border: 1px solid #eaeaea;">
                <a href="{{ url('/course/'.$course->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$course->image) }});width: 95px; height: 60px;">
                </a>
                <div class="desc">
                  <h2 style="font-size: 18px;"><a href="{{ url('/course/'.$course->id) }}">{{ $course->title }}</a></h2>
                  <p class="admin" style="margin-bottom: 5px;">
                    <span>
                      {{ Carbon::parse($course->updated_at)->format('d M Y') }}
                    </span>
                    @if(isset($course->extra['location'][$share_locale]))
                     - 
                    <span>
                      {{ $course->extra['location'][$share_locale] }}
                    </span>
                    @endif
                    @if(isset($course->extra['duration'][$share_locale]))
                     - 
                    <span>
                      {{ $course->extra['duration'][$share_locale] }}
                    </span>
                    @endif

                     - <a href="{{ url('/courses/'.$course->postCategory->id) }}">{{ $course->postCategory->name }}</a>
                  </p>
                  <p style="margin-bottom: 0px;">{!! $course->short_desc !!}</p>
                </div>
              </div>
              @endforeach
              @else
                <hr/>
                <div style="text-align: center;">@lang('front.nocourses')</div>
              @endif

              @if($courses->count() > 0)
                {{ $courses->links() }}
              @endif
            </div>

            <div class="col-md-4 col-md-4-sp video-holder cust-side-bar">
              @if($trainers->count())
                <h3>@lang('front.trainers')</h3>
                @foreach($trainers as $trainer)
                <div style="margin-bottom: 30px">
                  <div class="trainer-thumb">
                    <a href="{{ url('/trainer/'.$trainer->id) }}" style="background-image: url({{ url($trainer->image) }});">
                    </a>
                  </div>
                  <div class="trainer-info">
                    <strong>{{ $trainer->name }}</strong><br/>
                    {{ $trainer->title }}
                  </div>
                  <div style="clear: both;"></div>
                  <div style="padding-top: 15px">
                    {{ $trainer->short_desc }}... <a href="{{ url('/trainer/'.$trainer->id) }}" style="font-size: 12px">@lang('front.trainer_page')</a>
                  </div>
                </div>
                @endforeach
              @endif
              <a href="{{ url('trainers') }}" class="btn btn-primary btn-effect" style="border-radius: 0;border-bottom: 4px solid #1b5696">@lang('front.alltrainers')</a>
              <br/><br/>

              <h3>@lang('front.whyjoin') {{ $category->name }}</h3>
              @if($category->content)
                <div style="margin-bottom: 30px">
                  {!! $category->content !!}
                </div>
              @endif
            </div>
        </div>
        
      </div>
    </div>

@stop