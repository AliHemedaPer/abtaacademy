<div id="colorlib-subscribe" class="subs-img" style="background-image: url({{ asset('public/images/img_bg_2.jpg') }});padding: 0;border-top: 2px solid rgb(133, 166, 194);" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container" style="width: 100%">
        <div class="row">
          <div class="col-md-6 colorlib-heading animate-box home-form">
            <h2>@lang('front.contact')</h2>
            <p >@lang('front.have_aquestion')</p>

            <div class="row animate-box">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                  <form class="form-inline qbstp-header-subscribe">
                    <input type="hidden" class="form-control" id="quote_service" value="Web Question From">
                    <div class="col-full">
                      <div class="form-group">
                        <input type="text" class="form-control" id="quote_name" placeholder="{{ __('front.name') }}">
                      </div>
                    </div>
                    <div class="col-full">
                      <div class="form-group">
                        <input type="text" class="form-control" id="quote_phone" placeholder="{{ __('front.phone') }}">
                      </div>
                    </div>
                    <div class="col-full">
                      <div class="form-group">
                        <input type="text" class="form-control" id="quote_email" placeholder="{{ __('front.email') }}">
                      </div>
                    </div>
                  </form>
                  </div>
                  <div class="col-md-12">
                  <form class="form-inline qbstp-header-subscribe">
                    <div class="col-three-forth">
                      <div class="form-group">
                        <input type="text" class="form-control" id="quote_message" placeholder="{{ __('front.question') }}">
                      </div>
                    </div>
                    <div class="col-one-third">
                      <div class="form-group">
                        <button type="button"  onclick="servicemail('contact')" class="btn btn-primary">@lang('front.send')</button>
                      </div>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 animate-box home-map">
           {!!  $appSettings->map ?? '' !!}
          </div>
        </div>
        
      </div>
    </div>