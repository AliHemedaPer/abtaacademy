@extends("front.front")

@section('title')
@lang('front.home')
@endsection

@section("content")
	<aside id="colorlib-hero">
      <div class="flexslider">
        <ul class="slides">
          @foreach($banners as $banner)
          <li style="background-image: url({{ url($banner->image) }})">
            <div class="overlay"></div>
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-8 col-sm-12 slider-text">
                  <div class="slider-text-inner">
                    @if($banner->title)
                    <h1>{{ $banner->title }}</h1>
                    @endif
                    @if($banner->short_desc)
                    <h2>{{ $banner->short_desc }}</h2>
                    @endif
                    <!-- <p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p> -->
                  </div>
                </div>
              </div>
            </div>
          </li>
          @endforeach
          </ul>
        </div>
    </aside>
    
    {{-- <div id="colorlib-intro">
      <div class="container">
        <div class="row">
           <div class="col-md-4 intro-wrap">
            <div class="intro-flex">
              @foreach($ourValues as $value)
                <div class="one-third {{ $loop->index % 2 == 0 ? 'color-3' : 'color-2' }} animate-box hoverShadow">
                  <span class="icon"><i class="flaticon-professor" style="color: #cecece;"></i></span>
                  <div class="desc">
                    <h3>{{ $value->title }}</h3>
                    <p style="{{ $loop->index % 2 == 0 ? 'color: #d8d8d8' : '#080707' }}">
                      {!! $value->short_desc !!}
                    </p>
                  </div>
                </div>
              @endforeach
              <!-- <div class="one-third color-3 animate-box">
                <span class="icon"><i class="flaticon-professor" style="color: #cecece;"></i></span>
                <div class="desc">
                  <h3>@lang('front.vesion')</h3>
                  <p style="color: #d8d8d8">
                    @lang('front.vesion_txt')
                  </p>
                </div>
              </div>
              <div class="one-third color-2 animate-box">
                <span class="icon"><i class="flaticon-market"></i></span>
                <div class="desc">
                  <h3>@lang('front.goal')</h3>
                  <p style="color: #080707">@lang('front.goal_txt')</p>
                </div>
              </div>
              <div class="one-third color-3 animate-box">
                <span class="icon"><i class="flaticon-open-book" style="color: #cecece;"></i></span>
                <div class="desc">
                  <h3>@lang('front.message')</h3>
                  <p style="color: #d8d8d8">@lang('front.mission_txt')</p>
                </div>
              </div> -->
            </div>
          </div>
          <div class="col-md-8">
            <div class="about-desc animate-box" style="padding: 2em 2em 2em 2em;">
              <h2>
                @if($share_locale == 'ar')
                  {{ $menuWhy->title_ar }}
                @else
                  {{ $menuWhy->title_en }}
                @endif
              </h2>
              <p>@if($share_locale == 'ar')
                  {!! $menuWhy->short_desc_ar !!}
                @else
                  {!! $menuWhy->short_desc_en !!}
                @endif</p>
          </div>
        </div>
      </div>
    </div> --}}
    
    <div class="colorlib-classes colorlib-light-grey" style="background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #cbd9e5;" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2>@lang('front.offers')</h2>
            <!-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p> -->
          </div>
        </div>
        <div class="row">
          @foreach($offers as $offer)
          <div class="col-md-4 animate-box " style="height: 480px;margin-bottom: 16px;{{ $share_locale == 'ar' ? 'float: right;' : '' }}">
            <div class="classes hoverShadow">
              <a href="{{ url('course/'.$offer->id) }}">
              <div class="classes-img" style="background-image: url({{ asset('public/images/posts/'.$offer->image) }})">
                <!-- <span class="price text-center"><small>$450</small></span> -->
              </div>
            </a>
              <div class="desc">
                <h3 style="font-size: 16px;margin-bottom: 8px;"><a href="{{ url('course/'.$offer->id) }}">{{ $offer->title }}</a></h3>
                
                <div style="font-size: 14px;padding-bottom: 10px;color: #15254c;direction: ltr;
    text-align: left;">
                  <i class="fa fa-money"></i> {!! isset($offer->extra['fees']) ? (isset($offer->extra['finalfees']) && $offer->extra['finalfees'] < $offer->extra['fees'] ) ? '<span style="text-decoration: line-through;color: #a7a6a6;">'.$offer->extra['fees'].'</span> '.$offer->extra['finalfees'] : $offer->extra['fees'] : '/' !!} LE &nbsp;&nbsp;|&nbsp;&nbsp; <i class="fa fa-calendar"></i> {{ isset($offer->extra['date']) ? $offer->extra['date'] : '-' }}
                </div>

                <p>{{ $offer->short_desc }}</p>
                <p style="text-align: center;"><a href="{{ url('course/'.$offer->id) }}" class="btn btn-danger">@lang('front.viewmore')</a></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>  
    </div>

    <div class="colorlib-classes colorlib-light-grey" style="background:#15254c;border-top: 1px solid #cbd9e5;" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2 style="color: #fff">@lang('front.popular')</h2>
            <!-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p> -->
          </div>
        </div>
        <div class="row">
          @foreach($populars as $popular)
          <div class="col-md-4 animate-box " style="height: 480px;margin-bottom: 16px;{{ $share_locale == 'ar' ? 'float: right;' : '' }}">
            <div class="classes hoverShadow">
              <a href="{{ url('course/'.$popular->id) }}">
              <div class="classes-img" style="background-image: url({{ asset('public/images/posts/'.$popular->image) }})">
                <!-- <span class="price text-center"><small>$450</small></span> -->
              </div>
            </a>
              <div class="desc">
                <h3 style="font-size: 16px;margin-bottom: 8px;"><a href="{{ url('course/'.$popular->id) }}">{{ $popular->title }}</a></h3>

                <div style="font-size: 14px;padding-bottom: 10px;color: #15254c;direction: ltr;
    text-align: left;">
                  <i class="fa fa-money"></i> {!! isset($popular->extra['fees']) ? (isset($popular->extra['finalfees']) && $popular->extra['finalfees'] < $popular->extra['fees'] ) ? '<span style="text-decoration: line-through;color: #a7a6a6;">'.$popular->extra['fees'].'</span> '.$popular->extra['finalfees'] : $popular->extra['fees'] : '/' !!} LE &nbsp;&nbsp;|&nbsp;&nbsp; <i class="fa fa-calendar"></i> {{ isset($popular->extra['date']) ? $popular->extra['date'] : '-' }}
                </div>
                
                <p>{{ $popular->short_desc }}</p>
                <p style="text-align: center;"><a href="{{ url('course/'.$popular->id) }}" class="btn btn-danger">@lang('front.viewmore')</a></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>  
    </div>
    
    <div id="colorlib-services" style="background: #f2f3f7;border-top: 1px solid #cbd9e5;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2 style="margin: 0 0 0 0;">@lang('front.services')</h2>
            <!-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p> -->
          </div>
        </div>
        <div class="row">
          @foreach($services as $service)
          <div class="col-md-3 text-center animate-box ">
            <div class="services hoverShadow">
              <span class="icon">
                {{ $service->title }}
              </span>
              <div class="desc">
                <p>{{ $service->short_desc }}</p>
                <p>
                  <a href="{{ url('service/'.$service->id) }}" class="btn btn-danger">@lang('front.more')</a>
                 </p>
              </div>
            </div>
          </div>
          @endforeach
        </div>

        @if($services->count() > 4)
        <div style="text-align: center;">
          <a href="{{ url('/services') }}" class="btn btn-primary btn-lg btn-learn">@lang('front.viewmore')</a>
        </div>
        @endif
      </div>
    </div>

    

    <div id="colorlib-counter" class="colorlib-counters" style="background-image: url({{ asset('public/images/bg1.jpg') }})" data-stellar-background-ratio="0.5">
        <!-- <div class="overlay"></div> -->
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              @foreach($counters as $counter)
              <div class="col-md-3 col-sm-6 animate-box">
                <div class="counter-entry">
                  <span class="icon"><i class="flaticon-market"></i></span>
                  <div class="desc">
                    <span class="colorlib-counter js-counter" data-from="0" data-to="{{ $counter->count }}" data-speed="2000" data-refresh-interval="50"></span>
                    <span class="colorlib-counter-label">{{ $counter->title }}</span>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    {{-- <div class="colorlib-classes colorlib-light-grey" style="background-image: url({{ asset('public/images/bg2.jpg') }})" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2>@lang('front.latest_courses')</h2>
            <!-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p> -->
          </div>
        </div>
        <div class="row">
          @foreach($courses as $course)
          <div class="col-md-4 animate-box " style="height: 480px;margin-bottom: 16px;">
            <div class="classes hoverShadow">
              <a href="{{ url('course/'.$course->id) }}">
              <div class="classes-img" style="background-image: url({{ asset('public/images/posts/'.$course->image) }})">
                <!-- <span class="price text-center"><small>$450</small></span> -->
              </div>
            </a>
              <div class="desc">
                <h3 style="font-size: 16px;"><a href="{{ url('course/'.$course->id) }}">{{ $course->title }}</a></h3>
                <p>{{ $course->short_desc }}</p>
                <!-- <p><a href="{{ url('course/'.$course->id) }}" class="btn-learn">@lang('front.more') <i class="icon-arrow-right3"></i></a></p> -->
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>  
    </div>
    --}}
    <!-- <div id="colorlib-testimony" class="testimony-img" style="background-image: url({{ asset('public/images/img_bg_2.jpg') }})" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
            <h2>What Are The Students Says</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center">
            <div class="row animate-box">
              <div class="owl-carousel1">
                <div class="item">
                  <div class="testimony-slide">
                    <div class="testimony-wrap">
                      <blockquote>
                        <span>Sophia Foster</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                      </blockquote>
                      <div class="figure-img" style="background-image: url({{ asset('public/images/person1.jpg') }})"></div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="testimony-slide">
                    <div class="testimony-wrap">
                      <blockquote>
                        <span>John Collins</span>
                        <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                      </blockquote>
                      <div class="figure-img" style="background-image: url({{ asset('public/images/person2.jpg') }})"></div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="testimony-slide">
                    <div class="testimony-wrap">
                      <blockquote>
                        <span>Adam Ross</span>
                        <p>Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                      </blockquote>
                      <div class="figure-img" style="background-image: url({{ asset('public/images/person3.jpg') }})"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- <div class="colorlib-trainers">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
            <h2>Our Experienced Professor</h2>
            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-3 animate-box">
            <div class="trainers-entry">
              <div class="trainer-img" style="background-image: url({{ asset('public/images/person1.jpg') }})"></div>
              <div class="desc">
                <h3>Olivia Young</h3>
                <span>Teacher</span>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-3 animate-box">
            <div class="trainers-entry">
              <div class="trainer-img" style="background-image: url({{ asset('public/images/person2.jpg') }})"></div>
              <div class="desc">
                <h3>Daniel Anderson</h3>
                <span>Professor</span>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-3 animate-box">
            <div class="trainers-entry">
              <div class="trainer-img" style="background-image: url({{ asset('public/images/person3.jpg') }})"></div>
              <div class="desc">
                <h3>David Brook</h3>
                <span>Teacher</span>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-3 animate-box">
            <div class="trainers-entry">
              <div class="trainer-img" style="background-image: url({{ asset('public/images/person4.jpg') }})"></div>
              <div class="desc">
                <h3>Brigeth Smith</h3>
                <span>Teacher</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <div class="colorlib-blog colorlib-light-grey" style="background-image: url({{ asset('public/images/bg2.jpg') }})"  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2>@lang('front.latest_blog')</h2>
            
          </div>
        </div>
        <div class="row">
        @if($posts->count() > 0)
          @foreach($posts as $post)
            @if($loop->index == 0)
            <div class="floating col-md-6 animate-box">
              <article class="article-entry hoverShadow">
                <a href="{{ url('/post/'.$post->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$post->image) }})">
                  <p class="meta"><span class="day">{{ Carbon::parse($post->updated_at)->format('d') }}</span><span class="month">{{ Carbon::parse($post->updated_at)->format('M') }}</span></p>
                </a>
                <div class="desc">
                  <h2><a href="{{ url('/post/'.$post->id) }}">{{ $post->title }}</a></h2>
                  
                  <p>{{ $post->short_desc }}</p>
                </div>
              </article>
            </div>
            <!-- open small boxes --><div class="floating col-md-6"> 
            @else
              <div class="f-blog animate-box hoverShadow" style="border: 1px solid #dcdcdc;">
                <a href="{{ url('/post/'.$post->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$post->image) }})">
                </a>
                <div class="desc">
                  <h2 style="font-size: 16px;"><a href="{{ url('/post/'.$post->id) }}">{{ $post->title }}</a></h2>
                  <p class="admin" style="margin-bottom: 5px;"><span>
                    {{ Carbon::parse($post->updated_at)->format('d M Y') }}
                  </span></p>
                  <p style="margin-bottom: 0px;">{{ $post->short_desc }}</p>
                </div>
              </div>
            @endif
           @endforeach
           <!-- close small boxes --></div>
           @endif
        </div>
      </div>
    </div>
    
    @if(count($clients))
    <div class="colorlib-classes colorlib-light-grey" style="border-top: 1px solid #cbd9e5;" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2>@lang('front.clients')</h2>
            <!-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p> -->
          </div>
        </div>
        <section class="center slider" style="direction: ltr;">
          @foreach($clients as $client)
          <div>
            <img src="{{ asset($client->image) }}">
          </div>
          @endforeach
        </section>        
      </div>  
    </div>
    @endif
    

    @include('front.contact-form')

    
@endsection
