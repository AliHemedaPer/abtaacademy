@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

<div class="colorlib-blog colorlib-light-grey" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7 "  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row" style="padding-top: 20px;">
          <div class="col-md-12 col-md-12-sp">

            <h3 style="font-size: 22px;color: #15254c; margin-bottom: 20px;">{{ $title }}</h3>

            @if($courses->count() > 0)
              @foreach($courses as $course)

              <div class="f-blog animate-box hoverShadow" style="border: 1px solid #eaeaea;position: relative;" id="item_{{ $course->id }}">

                <a href="javascript://" onclick="removefrommy('{{ $course->id }}', '{{ $type }}')" style="position: absolute; top: 10px; {{ $share_locale == 'en' ? 'right' : 'left'  }}:10px"><i class="icon-trash" style="color: red"></i></a>

                <a href="{{ url('/course/'.$course->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$course->image) }});width: 95px; height: 60px;">
                </a>
                <div class="desc">
                  <h2 style="font-size: 18px;"><a href="{{ url('/course/'.$course->id) }}">{{ $course->title }}</a></h2>
                  <p class="admin" style="margin-bottom: 5px;">
                    <span>
                      {{ Carbon::parse($course->updated_at)->format('d M Y') }}
                    </span>
                    @if(isset($course->extra['location'][$share_locale]))
                     - 
                    <span>
                      {{ $course->extra['location'][$share_locale] }}
                    </span>
                    @endif
                    @if(isset($course->extra['duration'][$share_locale]))
                     - 
                    <span>
                      {{ $course->extra['duration'][$share_locale] }}
                    </span>
                    @endif

                     - <a href="{{ url('/courses/'.$course->postCategory->id) }}">{{ $course->postCategory->name }}</a>
                  </p>
                </div>
              </div>
              @endforeach
              @else
                <hr/>
                <div style="text-align: center;">@lang('front.nocourses')</div>
              @endif

              @if($courses->count() > 0)
                {{ $courses->links() }}
              @endif
            </div>
        </div>
        
      </div>
    </div>

@stop

@section("scripts")
<script type="text/javascript">
  function removefrommy(id, typ)
  {

    var txt;
    var r = confirm("Are you sure?");
    if (r == true) {
      $.ajax({
        type: "DELETE",
        url: "{{url('/removefrommy')}}",
        data: {typ: typ, id:id},
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        complete:function(data){
        },
        success: function (data) {
            $('#item_'+id).remove();
        }
      });
    }  
  }
</script>
@endsection
