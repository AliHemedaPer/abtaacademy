@extends("front.front")

@section('title')
{{ $title }}
@endsection

@section("content")

@include('front.inner-banner')

<div id="colorlib-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 animate-box">
        <h2>@lang('front.register')</h2>
        <form action="#" id="user_reg">
          <div class="row form-group">
            <div class="col-md-12">
              <label for="fname">{{ __('front.name') }} / {{ __('front.cp_name') }} 
                <span style="color: #ce393d">* <span id="user_name_error" class='required'>{{ __('front.required') }}</span></span>
              </label>
              <input type="text" id="user_name" name="name" class="form-control">

            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="email">{{ __('front.email') }} <span style="color: #ce393d">* <span id="email_error" class='required'>{{ __('front.required') }}</span></span></label>
              <input type="text" id="email" name="email" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="pass">{{ __('front.pass') }} <span style="color: #ce393d">* <span id="pass_error" class='required'>{{ __('front.required') }}</span></span></label>
              <input type="password" id="pass" name="password" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="confpass">{{ __('front.confpass') }} <span style="color: #ce393d">* <span id="confpass_error" class='required'>{{ __('front.required') }} {{ __('front.passmatch') }}</span></span></label>
              <input type="password" id="confpass" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="fname">{{ __('front.phone') }} <span style="color: #ce393d">* <span id="phone_error" class='required'>{{ __('front.required') }}</span></span></label>
              <input type="text" id="phone" name="phone" class="form-control">
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-12">
              <label for="fname">{{ __('front.client_type') }} <span style="color: #ce393d"></label>
                <br/>
              <input type="radio" class="client_type" name="client_type" value="client" checked=""> {{ __('front.client') }}
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" class="client_type" name="client_type" value="company"> {{ __('front.company') }}
            </div>
          </div>
          
          <div class="form-group">
            <input type="button" onclick="user_register()" value="@lang('front.register')" class="btn btn-primary">
            <span id="regis_wait" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
            <span class="success_msg" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
            <span class="failure_msg" style="display: none;padding-left: 12px;color: #ce393d;"></span>
          </div>
        </form>   
      </div>
    </div>
  </div>
</div>

@endsection

@section("scripts")
<script type="text/javascript">
  function user_register()
    {
        var pass=$('#pass').val();
        var confpass=$('#confpass').val();
        var email=$('#email').val();
        var user_name=$('#user_name').val();
        var phone = $('#phone').val();

        var error = false;

        if(!user_name)
        {
           $("#user_name_error").fadeIn();
           error = true;
        }else{
          $("#user_name_error").fadeOut();
        }

        if(!email)
        {
           $("#email_error").fadeIn();
           error = true;
        }else{
          $("#email_error").fadeOut();
        }

        if(pass.length<=0)
        {
           $("#pass_error").fadeIn();
           error = true;
        }else{
          $("#pass_error").fadeOut();
        }

        if(confpass.length<=0 || confpass != pass)
        {
           $("#confpass_error").fadeIn();
           error = true;
        }else{
          $("#confpass_error").fadeOut();
        }

        if(!phone)
        {
           $("#phone_error").fadeIn();
           error = true;
        }else{
          $("#phone_error").fadeOut();
        }

        if(error){
          return;
        }

        $('#regis_wait').fadeIn(100);
        $.ajax({
          type: "POST",
          url: "{{url('register' )}}",
          data: $( "#user_reg" ).serialize(),
          datatype: 'json',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function (data) {
              let obj = data.status;
              if(obj == 'success1'){
                location.href="{{url('/')}}";
              }else if(obj == 'success2'){
                $(".success_msg").css({"display":"block"});
                $(".success_msg").html("{{ __('front.success') }}");
                $(".success_msg").delay(10000).fadeOut("slow");
                $('#regis_wait').fadeOut(100);
              }else{
                $(".failure_msg").css({"display":"block"});
                $(".failure_msg").html(data.msg);
                $(".failure_msg").delay(10000).fadeOut("slow");
                $('#regis_wait').fadeOut(100);
              }              
          }
        });
    }
</script>
@endsection