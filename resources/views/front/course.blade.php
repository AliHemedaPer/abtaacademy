@extends('front.front')

@section('title')
{{ $course->title }}
@endsection

@section('styles')
<style type="text/css">
  .fa{
    color: #15254c;
  }
</style>
@endsection

@section('content')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-8-sp">
            <div class="row">
              <div class="col-md-12 animate-box">
                <div class="classes class-single">
                  <div>
                    <div class="course-thumb">
                      <a href="#" style="background-image: url({{ asset('public/images/posts/'.$course->image) }});">
                      </a>
                    </div>
                    <div class="course-info" style="padding-top: 11%">
                      <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">{{ $course->title }}</h3>
                      {{ $course->short_desc }}
                      <div style="padding-top: 10px; font-size: 14px">
                        @lang('front.category'): <a href="{{ url('/courses/'.$course->postCategory->id) }}">{{ $course->postCategory->name }}</a>
                      </div>
                      @if (auth('client')->check()) 
                      <div style="padding-top: 10px; font-size: 14px">
                        @if($toFav > 0)
                        <i class="icon-heart" style="color: red"></i> @lang('front.addtofav')
                        @else
                        <a href="javascript://" onclick="addtomy('favorite')"><i class="icon-heart" style="color: red"></i> @lang('front.addtofav')</a>
                        @endif
                         | 
                        @if($toLib > 0)
                         <i class="icon-book" style="color: green"></i> @lang('front.addtolib')
                         @else
                          <a href="javascript://" onclick="addtomy('library')"><i class="icon-book" style="color: green"></i> @lang('front.addtolib')</a>
                          @endif
                      </div>
                      @endif
                    </div>
                    <div style="clear: both;"></div>
                  </div>

                  <div class=" desc2" style="padding-top: 20px">
                    <h4 class="title-border" style="font-size: 20px;">@lang('front.details')</h4>
                    {!! $course->content !!}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <br/>
              <a class="btn btn-primary" href="{{ url('/course/'.$course->id.'/request') }}"  target="_blank" class="btn btn-outline">@lang('front.joincourse')</a>
            </div>
          </div>

          <div class="col-md-4 col-md-4-sp video-holder " style="padding-bottom: 20px; padding-left: 0; padding-right: 0">
            <div class="col-md-12 cust-side-bar">
              <a href="{{ url('/course/'.$course->id.'/request') }}" target="_blank" class="btn btn-danger btn-effect" style="width: 100%;border-radius: 0;border-bottom: 4px solid #ab2330">@lang('front.joincourse')</a>
              <ul class="list-information list-unstyled">
                  <li class="list-information__item"><span class="list-information__title"><i class="fa fa-calendar"></i>&nbsp;&nbsp;@lang('front.date')</span> <span class="list-information__description">
                      {{ isset($extra['date']) ? $extra['date'] : '-' }}</span> </li>
                  <li class="list-information__item"><span class="list-information__title"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;@lang('front.duration')</span> <span class="list-information__description">{{ isset($extra['duration'][$share_locale]) ? $extra['duration'][$share_locale] : '-' }}</span> </li>
                  <li class="list-information__item"><span class="list-information__title"><i class="fa fa-map-o"></i>&nbsp;&nbsp;@lang('front.location') </span><span class="list-information__description">{{ isset($extra['location'][$share_locale]) ? $extra['location'][$share_locale] : '-' }}</span> </li>
                  <li class="list-information__item"><span class="list-information__title"><i class="fa fa-book"></i>&nbsp;&nbsp;@lang('front.level')</span> <span class="list-information__description">{{ isset($extra['level']) ? $extra['level'] : '-' }}</span> </li>
                  <li class="list-information__item"><span class="list-information__title"><i class="fa fa-users"></i>&nbsp;&nbsp;@lang('front.specialization')</span> <span class="list-information__description">{{ isset($extra['specialization'][$share_locale]) ? $extra['specialization'][$share_locale] : '-' }}</span> </li>
                  <li class="list-information__item"><span class="list-information__title"><i class="icon stroke icon-Bag"></i>@lang('front.fees')</span> <span class="list-information__description"><span class="list-information__number"></span>{!! isset($extra['fees']) ? (isset($extra['finalfees']) && $extra['finalfees'] < $extra['fees'] ) ? '<span style="text-decoration: line-through;color: #d2d2d2;">'.$extra['fees'].'</span> '.$extra['finalfees'] : $extra['fees'] : '/' !!} LE</span> </li>
              </ul>
              @if($course->brochure)
              <a href="{{ url('public/brochures/posts/'.$course->brochure) }}" target="_blank" class="btn btn-primary btn-effect" style="border-radius: 0;border-bottom: 4px solid #1b5696">@lang('front.download')</a>
              <br/><br/>
              @endif
              @if($course->postTrainer)
              <h3 style="margin-bottom: 5px;">@lang('front.trainer')</h3>
              <div>
                <div class="trainer-thumb">
                  <a href="{{ url('/trainer/'.$course->postTrainer->id) }}" style="background-image: url({{ url($course->postTrainer->image) }});">
                  </a>
                </div>
                <div class="trainer-info">
                  <strong>{{ $course->postTrainer->name }}</strong>
                  <br/>
                  {{ $course->postTrainer->title }}
                </div>
                <div style="clear: both;"></div>
                <div style="padding-top: 15px">
                  {{ $course->postTrainer->short_desc }}... <a href="{{ url('/trainer/'.$course->postTrainer->id) }}" style="font-size: 12px">@lang('front.more')</a>
                </div>
              </div>
              <br/>
            @endif
            <!-- <div class="classes-img" style="height: 320px;background-image: url({{ asset('public/images/posts/'.$course->image) }})">
            </div> -->

            @if($course->video)
              <embed src="{{ asset('public/videos/posts/'.$course->video) }}" allowfullscreen="true" width="100%" height="320" autoplay='false'>
                <br/><br/>
            @endif

            @if($course->video_url)
              <embed src="{!! str_replace('watch?v=', 'embed/', $course->video_url) !!}" allowfullscreen="true" width="100%" height="320">
              <br/><br/>
            @endif
            </div>
            
            @if($course->postCategory->content)
            <div class="col-md-12 cust-side-bar" style="margin-top: 15px">
              
                <h3 style="margin-bottom: 5px;">@lang('front.whyjoin') {{ $course->postCategory->name }}?</h3>
                <div style="margin-bottom: 30px">
                  {!! $course->postCategory->content !!}
                </div>
              
            </div>
            @endif

            @if(count($certificates) > 0)
            <div class="col-md-12 cust-side-bar" style="margin-top: 15px">
                <h3 style="margin-bottom: 5px;">@lang('front.certificates')</h3>
                @foreach($certificates as $certificate)
                  <div>
                    <ul style="padding: 0 28px;">
                      <li><a href="{{ url('certificate/'.$certificate) }}" style="font-size: 14px;" target="_blank">{{ $allCertiicates[$certificate] }}</a></li>
                    </ul>
                  </div>
                @endforeach
            </div>
            @endif
          </div>
        </div>

        @if($courses->count() > 0)
              <br/><br/>
              <h3 class="title-border">@lang('front.related_courses')</h3>
              <div style="clear: both;"></div>
              @foreach($courses as $crs)
                <div class="col-md-3 animate-box " style="height: 480px;margin-bottom: 16px;float: {{ $share_locale == 'en' ? 'left' : 'right'  }}">
                  <div class="classes hoverShadow">
                    <a href="{{ url('course/'.$crs->id) }}">
                    <div class="classes-img" style="background-image: url({{ asset('public/images/posts/'.$crs->image) }})">
                      <!-- <span class="price text-center"><small>$450</small></span> -->
                    </div>
                  </a>
                    <div class="desc">
                      <h3 style="font-size: 16px;"><a href="{{ url('course/'.$crs->id) }}">{{ $crs->title }}</a></h3>
                      <p>{{ $crs->short_desc }}</p>
                      <!-- <p><a href="{{ url('course/'.$crs->id) }}" class="btn-learn">@lang('front.more') <i class="icon-arrow-right3"></i></a></p> -->
                    </div>
                  </div>
                </div>
              @endforeach
              <br/><br/>
            @endif

      </div>  
    </div>
@stop


@section("scripts")
<script type="text/javascript">
  function addtomy(typ)
  {
    var id = '{{ $course->id }}';

    $.ajax({
        type: "POST",
        url: "{{url('/addtomy')}}",
        data: {typ: typ, id:id},
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        complete:function(data){
        },
        success: function (data) {
            var status=data.status;
            if(status=='success')
            {
              alert('Done!');
            }
        }
      });
    }
</script>
@endsection