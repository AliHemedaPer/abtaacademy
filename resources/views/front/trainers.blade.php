@extends('front.front')

@section('title')
@lang('front.alltrainers')
@endsection

@section('content')

<div class="colorlib-blog colorlib-light-grey" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7 "  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row" >
          <div class="col-md-12 col-md-12-sp">

            <h3 style="font-size: 22px;color: #15254c; margin-bottom: 20px;">@lang('front.alltrainers')</h3>

            @if($trainers->count() > 0)
              @foreach($trainers as $trainer)
              <div class="f-blog animate-box hoverShadow" style="border: 1px solid #eaeaea;">
                <a href="{{ url('/trainer/'.$trainer->id) }}" class="blog-img" style="background-image: url({{ url($trainer->image) }});width: 90px; height: 90px;">
                </a>
                <div class="desc">
                  <h2 style="font-size: 18px;"><a href="{{ url('/trainer/'.$trainer->id) }}">{{ $trainer->name }}</a></h2>
                  <p class="admin" style="margin-bottom: 5px;">
                    <span>
                      {{ $trainer->title }}
                    </span>

                     - <a href="{{ url('/courses/'.$trainer->Category->id) }}">{{ $trainer->Category->name }}</a>
                  </p>
                </div>
                <div style="padding-top: 5px">
                    {{ $trainer->short_desc }}... <a href="{{ url('/trainer/'.$trainer->id) }}" style="font-size: 12px">@lang('front.trainer_page')</a>
                  </div>
              </div>
              @endforeach
              @else
                <hr/>
                <div style="text-align: center;">No Cources!</div>
              @endif

              @if($trainers->count() > 0)
                {{ $trainers->links() }}
              @endif
            </div>
        </div>
        
      </div>
    </div>

@stop