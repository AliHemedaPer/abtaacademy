@extends("front.front")

@section('title')
{{ $title }}
@endsection

@section("content")

@include('front.inner-banner')

<div id="colorlib-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 animate-box">
        <h2>@lang('front.resetpassword')</h2>
        <form action="#" id="login_form">
          
            @if($reset_code)
            <input type="hidden" name="reset_code" value="{{ $reset_code }}">
            <div class="row form-group">
              <div class="col-md-12">
                <label for="pass">{{ __('front.newpass') }} <span style="color: #ce393d">* <span id="pass_error" class='required'>{{ __('front.required') }}</span></span></label>
                <input type="password" id="pass" name="pass" class="form-control">
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <label for="confpass">{{ __('front.confpass') }} <span style="color: #ce393d">* <span id="confpass_error" class='required'>{{ __('front.required') }} {{ __('front.passmatch') }}</span></span></label>
                <input type="password" id="confpass" class="form-control">
              </div>
            </div>
            @else
              <div class="row form-group">
                <div class="col-md-12">
                  <label for="email">{{ __('front.email') }} <span style="color: #ce393d">* <span id="email_error" class='required'>{{ __('front.required') }}</span></span></label>
                  <input type="text" id="email" name="email" class="form-control">
                </div>
              </div>
            @endif
          
          <div class="form-group">
            <input type="button" onclick="loginforget()" value="@lang('front.resetpassword')" class="btn btn-primary">
            
            <a href="{{url('login')}}"><span style="padding-left: 12px;">{{ __('front.login')}}</span></a>
           
            <span id="login_wait" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
            <span class="success_msg" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
            <span class="failure_msg" style="display: none;padding-left: 12px;color: #ce393d;"></span>
          </div>
        </form>   
      </div>
    </div>
  </div>
</div>


@endsection

@section("scripts")
<script type="text/javascript">
  function loginforget()
  {

    @if($reset_code)
      
      var pass=$('#pass').val();
      var confpass=$('#confpass').val();

      if(pass.length<=0)
      {
         $("#pass_error").fadeIn();
         error = true;
      }else{
        $("#pass_error").fadeOut();
      }

      if(confpass.length<=0 || confpass != pass)
      {
         $("#confpass_error").fadeIn();
         error = true;
      }else{
        $("#confpass_error").fadeOut();
      }

    @else

      var email=$("#email").val();

      var error = false;

      if(email=="")
      {
         $("#email_error").fadeIn();
         error = true;
      }else{
        $("#email_error").fadeOut();
      }

      
    @endif

    if(error){
      return;
    }

    $('#login_wait').fadeIn(100);
    $.ajax({
        type: "POST",
        url: "{{url('resetpassword')}}",
        data: $( "#login_form" ).serialize(),
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        complete:function(data){

        },
        success: function (data) {
            $('#login_wait').fadeOut(100);
            var status=data.status;

            if(status=='failure')
            {
              $(".failure_msg").html(data.msg);
              $(".failure_msg").css({"display":"block"});
              $(".failure_msg").delay(5000).fadeOut("slow");
            }

            if(status=='success')
            {
                $(".success_msg").html(data.msg);
                $(".success_msg").css({"display":"block"});
                $(".failure_msg").delay(5000).fadeOut("slow");
            }
        }
      });
    }
</script>
@endsection