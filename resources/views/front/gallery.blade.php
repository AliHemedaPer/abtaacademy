@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }}) "  data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      @if($gallery->count() > 0)
        @foreach($gallery as $image)
            <div class="col-md-4 animate-box " style="height: 480px;margin-bottom: 16px;">
              <div class="classes hoverShadow">
               <div class="desc">
                  <h3 style="font-size: 16px;margin: 0 0 0 0;text-align: center;"><a href="{{ url('gallery/'.$image->id) }}">{{ $image->title }}</a></h3>
                </div> 
                <a href="{{ url('gallery/'.$image->id) }}">
                  <div class="classes-img" style="background-image: url({{ asset($image->oneImage()) }})">
                    <!-- <span class="price text-center"><small>$450</small></span> -->
                  </div>
                </a>
              </div>
            </div>
        @endforeach
      @else
        <hr/>
        <div style="text-align: center;">@lang('front.noimages')</div>
      @endif

    </div>
  </div>  
</div>
@stop