@extends("front.front")

@section('title')
{{ $title }}
@endsection

@section("content")

@include('front.inner-banner')

<div id="colorlib-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 animate-box">
        <h2>@lang('front.login')</h2>
        <form action="#" id="login_form">
          <input type="hidden" name="course" value="{{ $course }}">
          <div class="row form-group">
            <div class="col-md-12">
              <label for="email">{{ __('front.email') }} <span style="color: #ce393d">* <span id="email_error" class='required'>{{ __('front.required') }}</span></span></label>
              <input type="text" id="email" name="email" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="pass">{{ __('front.pass') }} <span style="color: #ce393d">* <span id="pass_error" class='required'>{{ __('front.required') }}</span></span></label>
              <input type="password" id="pass" name="pass" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <input type="button" onclick="login()" value="@lang('front.login')" class="btn btn-primary">
            <a href="{{url('resetpassword')}}"><span style="padding-left: 12px;">{{ __('front.forget')}}</span></a> &nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp; <a href="{{url('register')}}"><span style="padding-left: 12px;">{{ __('front.register')}}</span></a>
            <span id="login_wait" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
            <span class="success_msg" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
            <span class="failure_msg" style="display: none;padding-left: 12px;color: #ce393d;"></span>
          </div>
        </form>   
      </div>
    </div>
  </div>
</div>


@endsection

@section("scripts")
<script type="text/javascript">
  function login()
  {
    var activate_form = "";
    var email=$("#email").val();
    var pass=$('#pass').val();

    var error = false;

    if(email=="")
    {
       $("#email_error").fadeIn();
       error = true;
    }else{
      $("#email_error").fadeOut();
    }

    if(pass.length<=0)
    {
       $("#pass_error").fadeIn();
       error = true;
    }else{
      $("#pass_error").fadeOut();
    }

    if(error){
      return;
    }


    $('#login_wait').fadeIn(100);
    $.ajax({
        type: "POST",
        url: "{{url('login')}}",
        data: $( "#login_form" ).serialize(),
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        complete:function(data){

        },
        success: function (data) {
            $('#login_wait').fadeOut(100);
            var status=data.status;

            if(status=='failure')
            {
              $(".failure_msg").html('Invalid username or password');
              $(".failure_msg").css({"display":"block"});
              $(".failure_msg").delay(5000).fadeOut("slow");
            }

            if(status=='success')
            {
              if(data.course)
                location.href="{{url('course/')}}/"+data.course+"/request";
              else
                location.href="{{url('/')}}";
            }
        }
      });
    }
</script>
@endsection