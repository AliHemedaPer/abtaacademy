@extends('front.front')

@section('title')
{{ $certificate->title }}
@endsection

@section('styles')
<style type="text/css">
  .fa{
    color: #569fda;
  }
</style>
@endsection

@section('content')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7">
      <div class="container">
        <div class="row">

          <div class="col-md-12 col-md-12-sp">
            <div class="row">
              <div class="col-md-12 animate-box">
                <div class="classes class-single">
                  <div style="text-align: center;">
                      <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">@lang('front.certificate'): {{ $certificate->title }}</h3>
                  </div>
                  <div style="text-align: center;margin: 15px 0;">
                    <img src="{{ url($certificate->image) }}">
                  </div>
                  <div class=" desc2" style="padding-top: 20px">
                    {!! $certificate->content !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>
@stop