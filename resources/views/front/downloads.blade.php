@extends('front.front')

@section('title')
@lang('front.downloads')
@endsection

@section('content')

<div class="colorlib-blog colorlib-light-grey" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7 "  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row" style="padding-top: 20px;">
          <div class="col-md-12 col-md-12-sp">

            <h3 style="font-size: 22px;color: #15254c; margin-bottom: 20px;">@lang('front.downloads')</h3>

            @if($downloads->count() > 0)
              @foreach($downloads as $course)
                @if($course->brochure)
                  <div class="f-blog animate-box hoverShadow" style="border: 1px solid #eaeaea;padding-left:15px; padding-right: 15px">
                    <h2 style="font-size: 18px;margin: 0 0 0px 0;"><a href="{{ url('public/brochures/posts/'.$course->brochure) }}" target="_blank">{{ $course->title }}</a></h2>
                  </div>
                @endif
              @endforeach
              @else
                <hr/>
                <div style="text-align: center;">@lang('front.nodownloads')</div>
              @endif

              @if($downloads->count() > 0)
                {{ $downloads->links() }}
              @endif
            </div>
        </div>
        
      </div>
    </div>

@stop