@extends("front.front")

@section('title')
{{ $title }}
@endsection

@section("content")

@include('front.inner-banner')

<div id="colorlib-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 animate-box">
        <div class="row">
          <div class="col-md-12">
            <div class="contact-info-wrap-flex">
              <div class="con-info">
                <p><span><i class="icon-globe"></i></span>
                    @if($share_locale == 'ar')
                      {{ $appSettings->app_address_ar }}
                    @else
                      {{ $appSettings->app_address_en }}
                    @endif
                </p>
              </div>
              <div class="con-info">
                <p><span><i class="icon-phone3"></i></span> <a href="tel://{{ $appSettings->app_phone }}">{{ $appSettings->app_phone }}</a></p>
              </div>
              <div class="con-info">
                <p><span><i class="icon-paperplane"></i></span> <a href="mailto:{{ $appSettings->app_email }}">{{ $appSettings->app_email }}</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-10 col-md-offset-1 animate-box">
        <h2>@lang('front.have_aquestion')</h2>
        <form action="#">
          <div class="row form-group">
            <div class="col-md-12">
              <label for="fname">{{ __('front.name') }} *</label>
              <input type="text" id="quote_name" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="fname">{{ __('front.phone') }} *</label>
              <input type="text" id="quote_phone" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="email">{{ __('front.email') }} *</label>
              <input type="text" id="quote_email" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="email">{{ __('front.subject') }} *</label>
              <input type="text" id="quote_service" class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label for="message">{{ __('front.message') }} *</label>
              <textarea name="message" id="quote_message" cols="30" rows="6" class="form-control"></textarea>
            </div>
          </div>
          <div class="form-group">
            <input type="button" onclick="servicemail('service')" value="@lang('front.send')" class="btn btn-primary">
          </div>
        </form>   
      </div>
    </div>
  </div>
</div>
@endsection
