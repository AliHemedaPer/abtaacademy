@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-classes">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-8-sp">
            <div class="row row-pb-lg">
              <div class="col-md-12 animate-box">
                <div class="classes class-single">
                  
                  <div class="desc2">
                    <!-- <h3>{{ $post->title }}</h3> -->
                    {!! $post->content !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-md-4-sp">
            <div class="classes-img" style="background-image: url({{ asset('public/images/posts/'.$post->image) }});height: 320px;">
                  </div>
                  <br/>
          @if($posts->count() > 0)
                <h3>@lang('front.latest_blog')</h3>
                @foreach($posts as $post)
                  <div class="f-blog animate-box hoverShadow" style="border: 1px solid #efefef;{{ $share_locale == 'ar' ? 'padding-right: 100px;padding-left: 10px' : 'padding-left: 100px;padding-right: 10px' }}">
                    <a href="{{ url('/post/'.$post->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$post->image) }});width: 80px;height: 68px;">
                    </a>
                    <div class="desc">
                      <h2 style="font-size: 12px;"><a href="{{ url('/post/'.$post->id) }}">{{ $post->title }}</a></h2>
                      <p class="admin" style="margin-bottom: 5px;"><span>
                        {{ Carbon::parse($post->updated_at)->format('d M Y') }}
                      </span></p>
                    </div>
                  </div>
                @endforeach
                <br/><br/>
              @endif

          @if($courses->count() > 0)
            <h3>@lang('front.latest_courses')</h3>
            @foreach($courses as $course)
              <div class="f-blog animate-box hoverShadow" style="border: 1px solid #efefef;{{ $share_locale == 'ar' ? 'padding-right: 100px;padding-left: 10px' : 'padding-left: 100px;padding-right: 10px' }}">
                <a href="{{ url('/course/'.$course->id) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$course->image) }});width: 80px;height: 68px;">
                </a>
                <div class="desc">
                  <h2 style="font-size: 12px;"><a href="{{ url('/course/'.$course->id) }}">{{ $course->title }}</a></h2>
                  <p class="admin" style="margin-bottom: 5px;"><span>
                    {{ Carbon::parse($course->updated_at)->format('d M Y') }}
                  </span></p>
                </div>
              </div>
            @endforeach
            
          @endif

              
          </div>
          
        </div>
      </div>  
    </div>

@include('front.contact-form')
@stop