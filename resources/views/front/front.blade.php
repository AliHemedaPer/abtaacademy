<!DOCTYPE HTML>
<html dir="{{ $share_locale == 'en' ? 'ltr' : 'rtl'  }}" lang="en" style="direction: {{ $share_locale == 'en' ? 'ltr' : 'rtl'  }}">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield("title")</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo:400,600,700">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  
  <!-- Animate.css -->
  <link rel="stylesheet" href="{{ asset('public/css/animate.css') }}">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="{{ asset('public/css/icomoon.css') }}">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="{{ asset('public/css/bootstrap.css') }}">

  <!-- Magnific Popup -->
  <link rel="stylesheet" href="{{ asset('public/css/magnific-popup.css') }}">

  <!-- Flexslider  -->
  <link rel="stylesheet" href="{{ asset('public/css/flexslider.css') }}">

  <!-- Owl Carousel -->
  <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/owl.theme.default.min.css') }}">
  
  <!-- Flaticons  -->
  <link rel="stylesheet" href="{{ asset('public/fonts/flaticon/font/flaticon.css') }}">

  <!-- Theme style  -->
  <link rel="stylesheet" href="{{ asset('public/css/style.css?v=1.1') }}">

  <link rel="stylesheet" type="text/css" media="all" href="{{ asset('public/css/stellarnav.css?v=1.1') }}">

  @if($share_locale == 'ar')
  <link rel="stylesheet" href="{{ asset('public/css/style-rtl.css?v=1.2') }}">
  @endif

  <link rel="stylesheet" href="{{ asset('public/slick/slick.css') }}">
  <link rel="stylesheet" href="{{ asset('public/slick/slick-theme.css') }}">

  <link rel="stylesheet" href="{{ asset('public/imgViewer/jBox.css') }}">
  @yield("styles")

  <!-- Modernizr JS -->
  <script src="{{ asset('public/js/modernizr-2.6.2.min.js') }}"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="{{ asset('public/js/respond.min.js') }}"></script>
  <![endif]-->

  @if($appSettings->chat)
    {!! $appSettings->chat !!}
  @endif

  <!-- Start of  Zendesk Widget script 
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=1742939e-e2e0-4c86-910a-0f142c246b96"> </script>
  End of  Zendesk Widget script -->

  </head>
  @if($share_locale == 'ar')
    <body style="text-align:right; font-family:'moon','Cairo', sans-serif;font-size:16px;">
  @else
    <body style="text-align: left;">
  @endif
    
  <!-- <div class="colorlib-loader"></div> -->


  <div class="icon-bar">
    @if($appSettings->fb)
    <a href="{{ $appSettings->fb }}" target="_blank" class="facebook"><i class="icon-facebook2"></i></a> 
    @endif
    @if($appSettings->tw)
    <a href="{{ $appSettings->tw }}" target="_blank" class="twitter"><i class="icon-twitter2"></i></a> 
    @endif
    @if($appSettings->in)
    <a href="{{ $appSettings->in }}" target="_blank" class="linkedin"><i class="icon-linkedin2"></i></a>
    @endif
    @if($appSettings->gl)
    <a href="{{ $appSettings->gl }}" target="_blank" class="google"><i class="icon-google"></i></a>
    @endif
    @if($appSettings->ins)
    <a href="{{ $appSettings->ins }}" target="_blank" class="instagram"><i class="icon-instagram"></i></a>
    @endif
    @if($appSettings->yt)
    <a href="{{ $appSettings->yt }}" target="_blank" class="youtube"><i class="icon-youtube"></i></a>
    @endif
  </div>

  <div id="page">
    <nav class="colorlib-nav" role="navigation">
      <div class="upper-menu">
        <div class="container">
          <div class="row">

            <div class="col-xs-3 top_phone spacing" style="text-align: left;width: auto;">
              <!-- <p style="color: #fff;">
                <a href="{{ url('contact') }}" style="color: #fff;">@lang('front.haveques')</a>
              </p> -->
              <p style="color: #fff;direction: ltr;">
                <i class="icon-phone2"></i> {{ $appSettings->app_phone }}
              </p>
              &nbsp;<span style="color: #fff">|</span>&nbsp;
              <p style="color: #fff;direction: ltr;">
               <i class="icon-envelope"></i> {{ $appSettings->app_email }}
              </p>
            </div>
            
            <div class="col-xs-4 text-right socials pull-right spacing" style="width: auto;padding: 6px 0 0 10px">
              <p>
                <ul class="colorlib-social-icons" style="direction: ltr;">                 
                  {{--
                    @if($appSettings->tw)
                  <li><a href="#"><i class="icon-twitter"></i></a></li>
                  @endif

                  @if($appSettings->fb)
                  <li><a href="#"><i class="icon-facebook"></i></a></li>
                  @endif

                  @if($appSettings->in)
                  <li><a href="#"><i class="icon-linkedin"></i></a></li>
                  @endif
                  --}}

                  <li class="btn-cta" style="padding-left: 10px;">
                    @if($share_locale == 'ar')
                      <a href="javascript://" onclick="changeLang('en')"><span style="font-size: 10px;border-radius: 0;">EN</span></a>
                    @else
                      <a href="javascript://" onclick="changeLang('ar')"><span style="font-size: 10px;border-radius: 0;">AR</span></a>
                    @endif
                  </li>


                </ul>
              </p>
            </div>

            <div class="col-xs-4 text-right  pull-right spacing" style="background: #15254c;width: auto;padding: 10px 20px;">
              <p>
                <ul class="colorlib-social-icons account" style="padding-right: 0">

                  @if (auth('client')->check())   
                    <li>
                        <a href="{{ url('/profile') }}" style="color: #ffadaf"><span style="font-size: 12px;">@lang('front.profile')</span></a>
                    </li>
                    <li style="color: #fff">
                        &nbsp;|&nbsp;
                    </li>
                    <li>
                        <a href="{{url('logout')}}"><span style="font-size: 12px;">@lang('front.logout')</span></a>
                    </li>
                    <li style="color: #fff">
                        &nbsp;|&nbsp;
                    </li>
                    <li>
                      <a href="{{ url('/contact') }}"><span style="font-size: 12px;">@lang('front.contact')</span></a>
                    </li>
                  @else
                    <li>
                        <a href="{{url('login')}}"><span style="font-size: 12px;">@lang('front.login')</span></a>
                    </li>
                    <li style="color: #fff">
                        &nbsp;|&nbsp;
                    </li>
                    <li>
                        <a href="{{url('register')}}"><span style="font-size: 12px;">@lang('front.register')</span></a>
                    </li>
                    <li style="color: #fff">
                        &nbsp;|&nbsp;
                    </li>
                    <li>
                      <a href="{{ url('/contact') }}"><span style="font-size: 12px;">@lang('front.contact')</span></a>
                    </li>
                  @endif
                </ul>
              </p>
            </div>

            <div class="col-xs-4 text-right socials socials2 pull-right spacing" style="width: auto;padding: 6px 15px;position: relative;width: 38%">
              <form method="get" action="{{ url('search') }}">
                <input type="text" name="key" id="key" value="" placeholder="{{ __('front.search') }}" style="height: 32px;padding: 2px 6px;width: 78%" autocomplete="off">
                <div id="searchList">
                </div>
                <button type="submit" class="btn btn-danger" style="margin-right: 0; margin-bottom: 0; padding: 2px 10px;margin-top: -5px;
    height: 32px;">@lang('front.search')</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="top-menu">
        <div class="container">
          <div class="row">
            <div class="" style="float: {{ $share_locale == 'en' ? 'left' : 'right'  }}">
              <div id="colorlib-logo"><a href="{{ url('/') }}"><img src="{{ $share_locale == 'en' ? asset($appSettings->image_en) : asset($appSettings->image_ar) }}?i={{ time() }}" alt="" style="max-width: 178px;
    max-height: 65px;"></a></div>
            </div>
            <div class="col-md-10">
              <div class="stellarnav">
                <ul class="ul1">
                  <!-- <li><a href="{{ url('/') }}" class="{{ $share_locale == 'en' ? 'pl-0' : 'pr-0' }}">@lang('front.home')</a></li> -->
                  
                  
                  {{-- <li class="{{ $share_locale == 'en' ? '' : 'drop-left' }}">
                    <a href="#">@lang('front.about')</a>
                      <ul>
                         @if(isset($menuAbout->id))
                        <li class="{{ $share_locale == 'en' ? '' : 'drop-left' }}">
                          <a href="{{ url('/pg/'.$menuAbout->slug) }}">
                            @if($share_locale == 'ar')
                              {{ $menuAbout->title_ar }}
                            @else
                              {{ $menuAbout->title_en }}
                            @endif
                          </a>
                        </li>
                        @endif
                        @if(isset($menuWhy->id))
                        <li class="{{ $share_locale == 'en' ? '' : 'drop-left' }}">
                          <a href="{{ url('/pg/'.$menuWhy->slug) }}">
                            @if($share_locale == 'ar')
                              {{ $menuWhy->title_ar }}
                            @else
                              {{ $menuWhy->title_en }}
                            @endif
                          </a>
                        </li>
                        @endif
                      </ul>
                    
                  </li>--}}

                  @foreach($menuCategories as $category)
                      @include('layouts.menu-categories', ['category' => $category, 'level' => 1])
                  @endforeach
                  <li>
                    <a href="{{ url('/blog') }}">
                      @lang('front.blog')
                      <br/>
                      <span class="hint">@lang('front.blog_hint')</span>
                    </a>
                  </li>
                </ul>
              </div>
              <!-- <ul>
                <li class="active"><a href="index.html">Home</a></li>
                <li class="has-dropdown">
                  <a href="courses.html">Courses</a>
                  <ul class="dropdown">
                    <li><a href="courses-single.html">Courses Single</a></li>
                    <li><a href="#">Mobile Apps</a></li>
                    <li><a href="#">Website</a></li>
                    <li><a href="#">Web Design</a></li>
                    <li><a href="#">WordPress</a></li>
                  </ul>
                </li>
                <li><a href="about.html">About</a></li>
                <li><a href="event.html">Events</a></li>
                <li><a href="news.html">News</a></li>
                <li><a href="contact.html">Contact</a></li>
                <li class="btn-cta"><a href="#"><span>Free Trial</span></a></li>
              </ul> -->
            </div>
          </div>
        </div>
      </div>
    </nav>
        @yield("content")

    <!-- footer -->
    <footer id="colorlib-footer">
      <div class="container">
        <div class="row">
          <div class="floating col-md-3 colorlib-widget">
            <h4>@lang('front.follow')</h4>
            <p></p>
            <p>
              <ul class="colorlib-social-icons">
                @if($appSettings->tw)
                <li><a href="{{$appSettings->tw}}" target="_blank"><i class="icon-twitter"></i></a></li>
                @endif

                @if($appSettings->fb)
                <li><a href="{{$appSettings->fb}}" target="_blank"><i class="icon-facebook"></i></a></li>
                @endif

                @if($appSettings->in)
                <li><a href="{{$appSettings->in}}" target="_blank"><i class="icon-linkedin"></i></a></li>
                @endif
                @if($appSettings->gl)
                <li><a href="{{$appSettings->gl}}" target="_blank"><i class="icon-google"></i></a></li>
                @endif

                @if($appSettings->ins)
                <li><a href="{{$appSettings->ins}}" target="_blank"><i class="icon-instagram"></i></a></li>
                @endif

                @if($appSettings->yt)
                <li><a href="{{$appSettings->yt}}" target="_blank"><i class="icon-youtube"></i></a></li>
                @endif
              </ul>
            </p>
            <p>
              <img src="{{ url('public/images/visa.png') }}" width="50">
              <img src="{{ url('public/images/master.png') }}" width="50">
              <!-- <img src="{{ url('public/images/fawaterak.png') }}" width="95"> -->
            </p>
          </div>
          <div class="floating col-md-3 colorlib-widget">
            <h4>@lang('front.links')</h4>
            <p>
              <ul class="colorlib-footer-links">
                <li><a href="{{ url('/') }}"><i class="icon-check"></i> @lang('front.home')</a></li>
                
                @if(isset($menuAbout->id))
                <!-- <li>
                  <a href="{{ url('/pg/'.$menuAbout->slug) }}">
                    <i class="icon-check"></i> 
                    @if($share_locale == 'ar')
                      {{ $menuAbout->title_ar }}
                    @else
                      {{ $menuAbout->title_en }}
                    @endif
                  </a>
                </li> -->
                @endif
                
                <li><a href="{{ url('/services') }}"><i class="icon-check"></i> @lang('front.services')</a></li>
                <li><a href="{{ url('/trainers') }}"><i class="icon-check"></i> @lang('front.trainers')</a></li>
                <li><a href="{{ url('/contact') }}"><i class="icon-check"></i> @lang('front.contact')</a></li>
              </ul>
            </p>
          </div>

          <div class="floating col-md-3 colorlib-widget">
            <h4>&nbsp;</h4>
            <p>
              <ul class="colorlib-footer-links">
                <li><a href="{{ url('/blog') }}"><i class="icon-check"></i> @lang('front.blog')</a></li>
                <li><a href="{{ url('/downloads') }}"><i class="icon-check"></i> @lang('front.downloads')</a></li>
                <li><a href="{{ url('/videos') }}"><i class="icon-check"></i> @lang('front.videos')</a></li>
                <li><a href="{{ url('/gallery') }}"><i class="icon-check"></i> @lang('front.gallery')</a></li>
              </ul>
            </p>
          </div>

          <div class="floating col-md-3 colorlib-widget">
            <h4>@lang('front.contact')</h4>
            <ul class="colorlib-footer-links">
              <li>
                @if($share_locale == 'ar')
                  {{ $appSettings->app_address_ar }}
                @else
                  {{ $appSettings->app_address_en }}
                @endif
              </li>
              <li><a href="tel://{{ $appSettings->app_phone }}"><i class="icon-phone2"></i> {{ $appSettings->app_phone }}</a></li>
              <li><a href="mailto:{{ $appSettings->app_email }}"><i class="icon-envelope"></i> {{ $appSettings->app_email }}</a></li>            </ul>
          </div>
        </div>
      </div>
      <div class="copy">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              <p>
                <small class="block">@lang('front.rights')</small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!-- <div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
  </div>
   -->
  <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
  <script type="text/javascript" src="{{ asset('public/imgViewer/jBox-min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/slick/slick.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/js/stellarnav.min.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {

      jQuery(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        autoplay: true,
        slidesToShow: 5,
        slidesToScroll: 3,
        autoplaySpeed: 2000,
      });

      jQuery('.stellarnav').stellarNav({
        theme: 'dark',
        breakpoint: 960,
        menuLabel: "{{ __('front.menu') }}",
        openingSpeed: 50, // how fast the dropdown should open in milliseconds
        closingDelay: 10,
        closeLabel: ' Close ', // label for the close button
        position: 'right',
        phoneBtn: '{{ $appSettings->app_phone }}',
        phoneLabel: 'Call Us',
        locationBtn: ''
      });

      var nav = $('.top-menu');

      $(window).scroll(function () {
          if ($(this).scrollTop() > 125) {
              nav.addClass("f-nav");
          } else {
              nav.removeClass("f-nav");
          }
      });


      $('#key').keyup(function(){ 
        var query = $(this).val();
        var imgLoader = "{{asset('public/images/loader.gif')}}";
        if(query != '')
        {
          $('#searchList').fadeIn(); 
          $('#searchList').html('<div class="search_loader" style="background: white;width: 195px;text-align: center;border: 1px solid #d6d6d6;"><img src="'+imgLoader+'" style="width:15px"></div>');
          
         var _token = $('meta[name="csrf-token"]').attr('content');
         $.ajax({
          url:"{{ route('autocomplete.fetchcpu') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
            //$('#searchList').fadeIn();  
            $('#searchList').html(data);
          }
         });
        }else{
          $('#searchList').fadeOut(); 
        }
    });

      $(document).on('click', 'body', function(){  
        //$('#key').val('');  
        $('#searchList').fadeOut();  
    });  


    });
  </script>

  <script src="{{ asset('public/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <!-- jQuery -->
  <!-- <script src="{{ asset('public/js/jquery.min.js') }}"></script> -->
  <!-- jQuery Easing -->
  <script src="{{ asset('public/js/jquery.easing.1.3.js') }}"></script>
  <!-- Bootstrap -->
  <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <!-- Waypoints -->
  <script src="{{ asset('public/js/jquery.waypoints.min.js') }}"></script>
  <!-- Stellar Parallax -->
  <script src="{{ asset('public/js/jquery.stellar.min.js') }}"></script>
  <!-- Flexslider -->
  <script src="{{ asset('public/js/jquery.flexslider-min.js') }}"></script>
  <!-- Owl carousel -->
  <script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
  <!-- Magnific Popup -->
  <script src="{{ asset('public/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('public/js/magnific-popup-options.js') }}"></script>
  <!-- Counters -->
  <script src="{{ asset('public/js/jquery.countTo.js') }}"></script>
  <!-- Main -->
  <script src="{{ asset('public/js/main.js') }}"></script>
  <script type="text/javascript">

  function changeLang(lang){
  $.ajax({
    url: "{{url('/')}}/locale/"+lang,
    type: 'GET',
    success: function (data) {
       location.reload();
      }
    });
  }

function servicemail(typ){
  let title   = $('#quote_service').val();
  let name   = $('#quote_name').val();
  let email   = $('#quote_email').val();
  let phone   = $('#quote_phone').val();
  let message = $('#quote_message').val();

  if(title == '0' || title == '' || name == '' || email == '' || phone == '' || message == ''){
    alert('All fileds are required!');
    return;
  }
  $('#sending').fadeIn(100);

  $.ajax({
    url: "{{url('/')}}/servicemail",
    type: 'POST',
    data:{title:title, name:name, email:email, phone:phone, message:message, typ:typ },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
       $('#sending').fadeOut(100);

       $('.success_msg').fadeIn(100);
       setTimeout(function(){ 
        $('.success_msg').fadeOut(100);
       }, 3000);

      },
      error: function (jqXHR, exception) {
        $('.failure_msg').fadeIn(100);
       setTimeout(function(){ 
        $('.failure_msg').fadeOut(100);
       }, 3000);
      }
  });
}
</script>

@yield("scripts")
  </body>
</html>