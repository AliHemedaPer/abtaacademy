@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }}) "  data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      @if($images->count() > 0)
        @foreach($images as $image)
          <div class="col-md-3 animate-box " style="margin-bottom: 25px;">
              <div class="classes hoverShadow">
                  <div class="classes-img" style="background-image: url({{ asset($image->image) }});overflow: hidden;">
                    <img class="jbox-img" src="{{ asset($image->image) }}" alt="Image 1" width="100%" height="100%" style="opacity: 0" />
                  </div>
              </div>
            </div>

                
               

        @endforeach
        <div style="clear: both;"></div>
      @else
        <hr/>
        <div style="text-align: center;">@lang('front.noimages')</div>
      @endif


    <div class="jbox-container">
        <div class="img-alt-text"></div>
        <img src="" />
        <svg version="1.1" class="jbox-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 306 306" xml:space="preserve">
            <g>
                <g id="chevron-right">
                    <polygon points="211.7,306 247.4,270.3 130.1,153 247.4,35.7 211.7,0 58.7,153" />
                </g>
            </g>
        </svg>
        <svg version="1.1" class="jbox-next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 306 306" xml:space="preserve">
            <g>
                <g id="chevron-right">
                    <polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153" />
                </g>
            </g>
        </svg>
        <svg version="1.1" class="jbox-close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
            <path d="M512,51.75L460.25,0L256,204.25L51.75,0L0,51.75L204.25,256L0,460.25L51.75,512L256,307.75L460.25,512L512,460.25
  L307.75,256L512,51.75z" />
        </svg>

    </div>
    <script type="text/javascript" src="{{ asset('public/imgViewer/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/imgViewer/jBox-min.js') }}"></script>
    <script>
        var gallery = new jBox();
    </script>

    </div>
  </div>  
</div>
@stop