@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }}) "  data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      @if($videos->count() > 0)
        @foreach($videos as $video)

            @if($video->video)
            <div class="floating col-md-4 animate-box" style="height: 320px;margin-bottom: 35px;">
              <div class="classes hoverShadow">
                  <embed src="{{ asset('public/videos/posts/'.$video->video) }}" allowfullscreen="true" width="100%" height="320" autoplay='false'>
                </div>
           </div>
            @endif

            @if($video->video_url)
            <div class="floating col-md-4 animate-box" style="height: 320px;margin-bottom: 35px;">
              <div class="classes hoverShadow">
                <embed src="{!! str_replace('watch?v=', 'embed/', $video->video_url) !!}" allowfullscreen="true" width="100%" height="320">
                  </div>
            </div>
            @endif

        @endforeach
      @else
        <hr/>
        <div style="text-align: center;">@lang('front.novideos')</div>
      @endif

      @if($videos->count() > 0)
        {{ $videos->links() }}
      @endif
    </div>
  </div>  
</div>
@stop