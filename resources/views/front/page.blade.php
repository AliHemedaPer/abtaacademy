@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div id="colorlib-about" class="colorlib-light-grey">
  <div class="container">
    <div class="row row-pb-md">
      <div class="col-md-12 animate-box">
        <h4 class="title-border">{{ $title }}</h4>
        <div class="about-wrap">
          {!! $data[0]->content !!}
        </div>
      </div>
    </div>

    @if(count($partners) && $slug == 'why-us')
    <div class="colorlib-classes colorlib-light-grey" style="border-top: 1px solid #cbd9e5;" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center colorlib-heading animate-box">
            <h2>@lang('front.partners')</h2>
            <!-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p> -->
          </div>
        </div>
        <section class="center slider" style="direction: ltr;">
          @foreach($partners as $partner)
          <div>
            <img src="{{ asset($partner->image) }}">
          </div>
          @endforeach
        </section>        
      </div>  
    </div>
    @endif

  </div>
</div>

@include('front.contact-form')
@stop