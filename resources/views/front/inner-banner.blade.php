<aside id="colorlib-hero" style="height: 200px; min-height: 200px">
<div class="flexslider">
  <ul class="slides" style="height: 200px;">
    @if(isset($bkg_typ) && $bkg_typ && $bkg)
    <li style="background-image: url({{ asset('public/images/posts/'.$bkg) }})">
    @else
    <li style="background-image: url({{ asset('public/images/img_bg_'.$bkg.'.jpg') }})">
    @endif
      <div class="overlay"></div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text" style="    height: 200px;">
            <div class="slider-text-inner text-center" style="height: 230px;">
              <h1 style="font-size: 28px">{{ $title }}</h1>
            </div>
          </div>
        </div>
      </div>
    </li>
    </ul>
  </div>
</aside>