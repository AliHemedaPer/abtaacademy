@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }}) "  data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      @foreach($posts as $post)
      <div class="floating col-md-4 animate-box"  style="height: 480px;">
        <div class="classes hoverShadow">
          <a href="{{ url('post/'.$post->id) }}">
            <div class="classes-img" style="background-image: url({{ asset('public/images/posts/'.$post->image) }})">
            <span class="price text-center"><small>{{ Carbon::parse($post->updated_at)->format('d M Y') }}</small></span>
          </div>
        </a>
          <div class="desc">
            <h3 style="font-size: 15px;"><a href="{{ url('post/'.$post->id) }}">{{ $post->title }}</a></h3>
            <p>{{ $post->short_desc }}</p>
            <!-- <p><a href="{{ url('post/'.$post->id) }}" class="btn-learn">@lang('front.more') <i class="icon-arrow-right3"></i></a></p> -->
          </div>
        </div>
      </div>
      @endforeach
    </div>
    {{ $posts->links() }}
  </div>  
</div>
@stop