@extends('front.front')

@section('title')
{{ $title }}
@endsection

@section('content')

@include('front.inner-banner')

<div class="colorlib-blog colorlib-light-grey" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }}) "  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          @if(count($topics) > 0)
            @foreach($topics as $grp => $topic)
              <br/><br/>
              <h3>&raquo; {{ __('front.'.$grp.'s') }}:</h3>
              @for($i=0;$i <= count($topic) ;$i++)
                  @if(isset($topic[$i]))

                  @php 
                    $url = ($grp == 'page' ? 'pg/'.$topic[$i]->slug : $grp.'/'.$topic[$i]->id)
                  @endphp

                    <div class="f-blog animate-box hoverShadow" style="border: 1px solid #eaeaea;">
                        <a href="{{ url('/'.$url) }}" class="blog-img" style="background-image: url({{ asset('public/images/posts/'.$topic[$i]->image) }});width: 95px; height: 60px;">
                        </a>
                        <div class="desc">
                          <h2 style="font-size: 18px;"><a href="{{ url('/'.$url) }}">{{ $topic[$i]->title }}</a></h2>
                          <p class="admin" style="margin-bottom: 5px;"><span>
                            {{ Carbon::parse($topic[$i]->updated_at)->format('d M Y') }}
                          </span></p>
                          <!-- <p style="margin-bottom: 0px;">{!! $topic[$i]->short_desc !!}</p> -->
                        </div>
                      </div>
                    @endif
                @endfor
            @endforeach
          @else
            <div style="text-align: center;">No Data!</div>
          @endif
        </div>
       
      </div>
    </div>

@stop