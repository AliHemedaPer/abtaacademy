@extends('front.front')

@section('title')
{{ $course->title }}
@endsection

@section('styles')
<style type="text/css">
  .fa{
    color: #569fda;
  }
  .req{
    position: absolute;
    color: #ff0b0b;
    top: 6px;
    {{ $share_locale == 'en' ? 'right' : 'left'  }}: 0px;
    font-size: 13px;
    display:none
  }
</style>
@endsection

@section('content')


<div class="colorlib-classes" style="min-height: 500px;background-image: url({{ asset('public/images/bg2.jpg') }});border-top: 1px solid #a9b2c7">
      <div class="container">
        @if (Request::session()->has('error')) 
          <div class="message-box-c" style="color: #dc3545;font-weight: bold;">{{Request::session()->get('error')}}</div>
        @endif
        @if (Request::session()->has('success')) 
        <span class="success_msg" style="padding-left: 12px;color: #07b58e;font-weight: bold;">{{ __('front.message_sent')}}</span>
        <br/><br/>
        @endif
    <form id="booking_form" method="POST" action="{{ url('/requestaction/').'/'.$course->id }}">
      @csrf
      <div class="row animate-box">
        <div class="col-md-12" id="request">

            <h3 style="font-size: 22px;color: #15254c; margin-bottom: 10px;">{{ $course->title }} ({!! isset($extra['fees']) ? (isset($extra['finalfees']) && $extra['finalfees'] < $extra['fees'] ) ? '<span style="text-decoration: line-through;color: #858a94;">'.number_format($extra['fees'], 2).'</span> '.number_format($extra['finalfees'],2) : number_format($extra['fees'],2) : '-' !!} LE)</h3>

            <div class="well" style="background: #fff">
              <input type="hidden" class="form-control" id="quote_service" value="{{ $title }}">
              <!-- <h2 class="colorlib-heading-2">@lang('front.request_txt')</h2> -->

              @if (auth('client')->check())
              <div class="row">
                <div class="form-group col-md-6">
                  <div  style="position: relative;">
                    <label for="fname">{{ __('front.name') }} (English) *</label>
                    <input type="text" id="quote_name_en" name="quote_name_en" class="form-control" value="{{ auth('client')->user()->name }}">
                    <div class="req" id="quote_name_en">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="fname">{{ __('front.name') }} (عربي)  *</label>
                    <input type="text" id="quote_name_ar" name="quote_name_ar" class="form-control" value="{{ auth('client')->user()->name }}">
                    <div class="req" id="quote_name_ar_req">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="email">{{ __('front.email') }} *</label>
                    <input type="text" id="quote_email" name="quote_email" class="form-control" value="{{ auth('client')->user()->email }}">
                    <div class="req" id="quote_email_req">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="email">{{ __('front.country') }} *</label>
                    <select id="quote_country" name="quote_country" class="form-control" style="height: 34px; padding: 0px 20px;">
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Åland Islands">Åland Islands</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antarctica">Antarctica</option>
                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guernsey">Guernsey</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guinea-bissau">Guinea-bissau</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jersey">Jersey</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                        <option value="Korea, Republic of">Korea, Republic of</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macao">Macao</option>
                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montenegro">Montenegro</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Pitcairn">Pitcairn</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russian Federation">Russian Federation</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saint Helena">Saint Helena</option>
                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option value="Saint Lucia">Saint Lucia</option>
                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                        <option value="Samoa">Samoa</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                        <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Timor-leste">Timor-leste</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States">United States</option>
                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Viet Nam">Viet Nam</option>
                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                        <option value="Western Sahara">Western Sahara</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                    </select>
                    <div class="req" id="quote_country_req">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="fname">{{ __('front.watsApp') }} *</label>
                    <input type="text" id="quote_wats" name="quote_wats" class="form-control" value="{{ auth('client')->user()->phone }}">
                    <div class="req" id="quote_wats_req">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="fname">{{ __('front.phone') }} *</label>
                    <input type="text" id="quote_phone" name="quote_phone" class="form-control" value="{{ auth('client')->user()->phone }}">
                    <div class="req" id="quote_phone_req">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="fname">{{ __('front.job') }} *</label>
                    <input type="text" id="quote_job" name="quote_job" class="form-control" value="">
                    <div class="req" id="quote_job_req">@lang('front.req')</div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div style="position: relative;">
                    <label for="fname">{{ __('front.education') }} *</label>
                    <input type="text" id="quote_education" name="quote_education" class="form-control" value="">
                    <div class="req" id="quote_education_req">@lang('front.req')</div>
                  </div>
                </div>
                
                <!-- <div class="form-group col-md-6">
                  <div >
                    <label for="message">{{ __('front.message') }} *</label>
                    <textarea name="message" id="quote_message" cols="30" rows="6" class="form-control"></textarea>
                  </div>
                </div> -->
              </div>
              @else
                <a href="{{ url('/login') }}"  target="_blank" class="btn btn-outline">@lang('front.request')</a>
                <!-- <span class="failure_msg" style="color: #ce393d;">@lang('front.plslog')</span> -->
              @endif
            </div>

            <div class="well" style="background: #fff">
              <h2 class="colorlib-heading-2">@lang('front.certificates')</h2>
              <input type="hidden" id="dollar" value="{{ $dollar }}">
              @if(count($certificates) > 0)
                  @foreach($certificates as $certificate)
                    <div>
                      <ul style="padding: 0 28px;list-style: none;">
                        <li>
                            <input type="checkbox" name="certificates[]" value="{{ $certificate->id }}" class="tot_inputs" data-price="{{ $certificate->price }}" onclick="tot_calc()"/> <a href="{{ url('certificate/'.$certificate->id) }}" style="font-size: 14px;" target="_blank">{{ $certificate->title }} ( {{ $certificate->price }}$ )</a></li>
                      </ul>
                    </div>
                  @endforeach
              @endif
            </div>

            <div class="well" style="background: #fff">
                <h2 class="colorlib-heading-2">@lang('front.payment')</h2>
                <div>
                  <ul style="padding: 0 28px;list-style: none;">
                    <li style="padding-bottom: 10px;margin-bottom: 10px;border-bottom: 1px solid #d8d8d8;color: #565656;"><input type="radio" name="payment" value="Fawaterak"> @lang('front.fawaterak')</li>
                    <li style="padding-bottom: 10px;margin-bottom: 10px;border-bottom: 1px solid #d8d8d8;color: #565656;"><input type="radio" name="payment" value="Bank Transfer"> @lang('front.bank')
                    <p style="margin-bottom: 0;font-size: 14px;color: #8c8c8c;">
                    @if($share_locale == 'ar')
                        {{ $appSettings->bank_ar }}
                    @else
                        {{ $appSettings->bank_en }}
                    @endif
                    </p>
                    </li>
                    <li style="padding-bottom: 10px;margin-bottom: 10px;border-bottom: 1px solid #d8d8d8;color: #565656;"><input type="radio" name="payment" value="Post Mail"> @lang('front.post')
                    <p style="margin-bottom: 0;font-size: 14px;color: #8c8c8c;">
                    @if($share_locale == 'ar')
                        {{ $appSettings->post_ar }}
                    @else
                        {{ $appSettings->post_en }}
                    @endif
                    </p>
                    </li>
                    <li style="padding-bottom: 10px;margin-bottom: 10px;color: #565656;"><input type="radio" name="payment" value="Cash" checked=""> @lang('front.cash')</li>
                  </ul>
                </div>
            </div>
            <div class="well" style="background: #fff">
                <div class="row">
                    <div class="col-md-6" style="float: {{ $share_locale == 'en' ? 'left' : 'right'  }}">
                      <input type="hidden" id="total_value" value="{{ isset($extra['fees']) ? (isset($extra['finalfees']) && $extra['finalfees'] < $extra['fees'] ) ? number_format($extra['finalfees'],2) : number_format($extra['fees'],2) : '0' }}">
                      <h2 class="colorlib-heading-2">@lang('front.totle'): <span id="total" style="color: #1d4ab7">{{ isset($extra['fees']) ? (isset($extra['finalfees']) && $extra['finalfees'] < $extra['fees'] ) ? number_format($extra['finalfees'],2) : number_format($extra['fees'],2) : '0' }}</span> LE</h2>
                    </div>
                    <div class="col-md-6" style="text-align: {{ $share_locale == 'en' ? 'right' : 'left'  }};float: {{ $share_locale == 'en' ? 'right' : 'left'  }}">
                      
                      <span id="sending" style="display: none;padding-left: 12px;color: #07b58e;">{{ __('front.pleasewait')}}</span>
                      
                      <span class="failure_msg" style="display: none;padding-left: 12px;color: #ce393d;">{{ __('front.message_not_sent')}}</span>

                      <input type="button" onclick="bookingRequest()" value="@lang('front.request')" class="btn btn-primary">
                    </div>
                </div>
                
            </div>
        </div>
      </div>
    </form>
    </div>  
</div>
@stop

@section('scripts')
<script>
  function bookingRequest(){
    var vali_arr = ['quote_name_en', 'quote_name_ar', 'quote_email', 'quote_phone', 'quote_country', 'quote_wats', 'quote_job', 'quote_education'];

    var validate = true;
    for (var i = 0; i < vali_arr.length; i++) {
      if( !$('#'+vali_arr[i]).val() || 
          $('#'+vali_arr[i]).val() == '' || 
          $('#'+vali_arr[i]).val() == undefined || 
          $('#'+vali_arr[i]).val() == null )
      {
        $('#'+vali_arr[i]+'_req').fadeIn();
        validate = false;
      }else{
        $('#'+vali_arr[i]+'_req').fadeOut();
      }
    }
    
    if(validate){
      $('#sending').fadeIn();
      $('#failure_msg').fadeOut();

      var form=$("#booking_form").submit();

      // $.ajax({
      //     type:"POST",
      //     url:form.attr("action"),
      //     headers: {
      //         'X-CSRF-TOKEN': '{{ csrf_token() }}'
      //     },
      //     data: $("#booking_form").serializeArray(),//only input
      //     success: function(response){
      //       $('#loading').fadeOut();
      //       if(response.status == 'success'){
      //         window.location.href = "{{ url('/bookingsuccess') }}"
      //       }else{
      //         $('#error_message').text(response.message);
      //         $('#error_message').fadeIn();
      //       }
      //     }
      // });
    }else{
      $("html, body").animate({ scrollTop: 0 }, "slow");
    }
  }

  function tot_calc(){
      var total_opts = 0;
      var dollar = parseFloat("{{ $dollar }}");
      var totla  = parseFloat($("#total_value").val());

      $('.tot_inputs').each(function () {
          var opt_price = $(this).attr('data-price');
           if (this.checked) {
               total_opts += (parseFloat(opt_price) * dollar) ;
           }
      });
      
      //Calculate total
      total = totla + total_opts;
      $("#total").text( (total).toFixed(2) );
    }
</script>
@endsection