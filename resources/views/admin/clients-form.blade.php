@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    
@endpush

@section('header')

@endsection

@section('content')
    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> 
                @if(isset($client))
                العميل: [ {{ $client->email }} ] {{ $client->name }}
                @else
                إرسال رسالة إلي جميع العملاء
                @endif
            </h5>
        </div>

        <div class="card-body">

            @if(isset($client) && $message)
                <h5 class="card-title"><i class="icon-envelop mr-2"></i> 
                   العنوان: {{ $client->title }}
                </h5>
                @if($isarray)
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>الإسم (English): </strong></div>
                        <div class="col-md-9">{{ $message['quote_name_en'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>الإسم (عربي): </strong></div>
                        <div class="col-md-9">{{ $message['quote_name_ar'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>البريد الإليكتروني: </strong></div>
                        <div class="col-md-9">{{ $message['quote_email'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>الدولة : </strong></div>
                        <div class="col-md-9">{{ $message['quote_country'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>رقم الهاتف : </strong></div>
                        <div class="col-md-9">{{ $message['quote_phone'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>رقم الواتساب : </strong></div>
                        <div class="col-md-9">{{ $message['quote_wats'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>المؤهل الدراسي : </strong></div>
                        <div class="col-md-9">{{ $message['quote_job'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>عنوان الوظيفه : </strong></div>
                        <div class="col-md-9">{{ $message['quote_education'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>الشهادات:</strong></div>
                        <div class="col-md-9">
                            @if(isset($message['certificates']) && count($message['certificates']))
                                <ul>
                                @foreach($message['certificates'] as $certf)
                                    <li>{{ $certf->title_ar }} ({{ $certf->price }}$)</li>
                                @endforeach
                                </ul>
                            @else
                                ---
                            @endif
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>الدفع: </strong></div>
                        <div class="col-md-9">{{ $message['payment'] ?? '--' }}</div>
                    </div>
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>الإجمالي</strong></div>
                        <div class="col-md-9">{{ $message['total'] ?? '00' }} LE</div>
                    </div>
                    @if(isset($message['invoiceKey']))
                    <div class="row" style="padding-bottom: 5px; margin-bottom: 10px;   border-bottom: 1px solid #c5c5c5;">
                        <div class="col-md-2"><strong>رقم الفاتورة</strong></div>
                        <div class="col-md-9">{{ $message['invoiceKey'] ?? '--' }}</div>
                    </div>
                    @endif
                @else
                    {{ $client->message }}
                @endif
                <br/><br/>
                <h5 class="card-title"><i class="icon-reply mr-2"></i> 
                    الرد:
                </h5>
            @endif
@push('scripts')
     <!-- Theme JS files -->
     <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>    
     <script src="{{asset('public/backend/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>   
    <!-- /theme JS files -->
     <script type="text/javascript">    
         // Validation config
        var FormValidation = function() {

            // Summernote
            var _componentSummernote = function() {
                if (!$().summernote) {
                    console.warn('Warning - summernote.min.js is not loaded.');
                    return;
                }

                // Basic examples
                // ------------------------------

                // Default initialization
                $('.summernote').summernote({
                    height: 200
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }
                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };    



            return {
                init: function() {
                    _componentSummernote();
                    _componentUniform();
                }
            }
        }();
        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
           
        });
    </script>
@endpush
<form method="POST" action="{{url('admin/clients/mail')}}" class="form__init" enctype="multipart/form-data">
    @if(isset($category))
    {{ method_field('PATCH') }}
    @endif
    @csrf
    <input type="hidden" name="client_id" value="{{ isset($client) ? $client->id : '' }}">
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <div class="form-group col-md-12" >
                    <label>العنوان:</label>
                    <input type="text" class="form-control" name="subject"
                           value="" required>
                </div>
            </fieldset>
        </div>
        <div class="col-md-12">
            <fieldset>
                <div class="form-group">
                    <label>@lang('admin.message'):</label>
                    <textarea rows="4" id="content_en" class="summernote" name="message" 
                        ></textarea>
                </div>
            </fieldset>
        </div>
    </div>


    <div class="d-flex justify-content-end align-items-center">
        <button type="button" onclick="App.redirect('/admin/{{ $cancel }}');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
        <button type="submit" class="btn btn-primary">@lang('admin.send') <i class="icon-floppy-disk"></i></button>
    </div>
</form>
</div>
    </div>
    <!-- /2 columns form -->
@endsection

