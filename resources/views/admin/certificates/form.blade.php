 @extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin - 
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>    
     <script src="{{asset('public/backend/js/plugins/editors/summernote/summernote.min.js')}}"></script>
     <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script> 
    <!-- /theme JS files -->
   <script type="text/javascript">    
         // Validation config
        var FormValidation = function() {

            // Summernote
            var _componentSummernote = function() {
                if (!$().summernote) {
                    console.warn('Warning - summernote.min.js is not loaded.');
                    return;
                }

                // Basic examples
                // ------------------------------

                // Default initialization
                $('.summernote').summernote({
                    height: 200
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }
                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };    

            // Select2 select
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                var $select = $('.form-control-select2').select2({
                    
                });

                // Trigger value change when selection is made
                $select.on('change', function() {
                    $(this).trigger('blur');
                });
            };

            return {
                init: function() {
                    _componentSummernote();
                    _componentUniform();
                    _componentSelect2();
                    //_componentValidation();
                }
            }
        }();
        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
           
        });
    </script>
@endpush

@section('header')

@endsection

@section('content')
<form method="POST" action="{{url('admin/certificates'). (isset($certificate) ? ('/' . $certificate->id) : '')}}" class="form__init" enctype="multipart/form-data">

    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ isset($certificate) ? __('admin.edit') : __('admin.add') }} {{ __('admin.banner') }}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <button type="button" onclick="App.redirect('/admin/certificates');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>


                </div>
            </div>
        </div>

        <div class="card-body">
                @if(isset($certificate))
                    {{ method_field('PATCH') }}
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title')  (EN):</label>
                                <input type="text" class="form-control" name="title_en"
                                    value="{{isset($certificate) ? $certificate->title_en: old('title_en') }}" required>
                            </div>              
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title') (AR):</label>
                                <input type="text" class="form-control" name="title_ar"
                                    value="{{isset($certificate) ? $certificate->title_ar: old('title_ar') }}" required>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.image'):</label>
                                <input type="file" name="image" class="form-control-uniform-custom">
                            </div>
                            
                            <div class="card-img-actions m-1">
                                @if(isset($certificate) && $certificate->image)
                                <img class="card-img img-fluid" src="{{ asset($certificate->image) }}" alt="">
                                <div class="card-img-actions-overlay card-img">
                                    <a href="{{ asset($certificate->image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                        <i class="icon-plus3"></i>
                                    </a>
                                </div>
                                @else
                                <img class="card-img img-fluid" src="{{ asset('public/images/no-photo.png') }}" alt="" style="max-width: 100px">
                                @endif
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.price') ($):</label>
                                <input type="text" class="form-control" name="price"
                                    value="{{isset($certificate) ? $certificate->price: old('price') }}" required>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>الوصف (EN):</label>
                                <textarea rows="4" id="content_en" class="summernote" name="content_en" 
                                    >{{isset($certificate) ? $certificate->content_en : old('content_en')}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>الوصف (AR):</label>
                                <textarea rows="4" id="content_ar" class="summernote" name="content_ar" 
                                    >{{isset($certificate) ? $certificate->content_ar : old('content_ar')}}</textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="d-flex justify-content-end align-items-center">
                    
                        <button type="button" onclick="App.redirect('/admin/certificates');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
        </div>
    </div>
    <!-- /2 columns form -->
</form>
@endsection

@section('jquery')
<script type="text/javascript">

</script>
@endsection


