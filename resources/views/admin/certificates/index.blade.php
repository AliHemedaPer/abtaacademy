@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$certificates->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('header')
    
@endsection

@section('content')
    
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.certificates')</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(auth('admin')->user()->canCreate('certificate'))
                        <a href="{{url('admin/certificates/create')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add_new') @lang('admin.certificate')</a>
                    @endif
                    
                </div>
            </div>
        </div>
        
        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.title')</th>
                    <th>@lang('admin.price')</th>
                    @if(auth('admin')->user()->canUpdate('certificate') or auth('admin')->user()->canDelete('certificate'))
                    <th class="text-center"></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($certificates as $certificate)
                    <tr>
                        <td>{{ $certificate->title_ar }}</td>
                        <td>{{ $certificate->price }} $</td>
                        @if(auth('admin')->user()->canUpdate('certificate') or auth('admin')->user()->canDelete('certificate'))
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(auth('admin')->user()->canUpdate('certificate'))
                                        <a href="{{ url('/admin/certificates/'.$certificate->id.'/edit')}}"
                                            class="dropdown-item"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        @endif

                                        @if(auth('admin')->user()->canDelete('certificate'))
                                        <a href="#"
                                            onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\CertificateController@destroy', $certificate->id)}}', null, App.redirect('/admin/certificates?ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                        @endif
                                       
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
