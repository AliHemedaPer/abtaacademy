@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$banners->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('header')
    
@endsection

@section('content')
    
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.banners')</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(auth('admin')->user()->canCreate('banner'))
                        <a href="{{url('admin/banners/create')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add_new') @lang('admin.banner')</a>
                    @endif
                    
                </div>
            </div>
        </div>
        
        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.title')</th>
                    <th>@lang('admin.image')</th>
                    @if(auth('admin')->user()->canUpdate('banner') or auth('admin')->user()->canDelete('banner'))
                    <th class="text-center"></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($banners as $banner)
                    <tr>
                        <td>{{ $banner->title_en }} / {{ $banner->title_ar }}</td>
                        <td>img</td>
                        @if(auth('admin')->user()->canUpdate('banner') or auth('admin')->user()->canDelete('banner'))
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(auth('admin')->user()->canUpdate('banner'))
                                        <a href="{{ url('/admin/banners/'.$banner->id.'/edit')}}"
                                            class="dropdown-item"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        @endif

                                        @if(auth('admin')->user()->canDelete('banner'))
                                        <a href="#"
                                            onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\BannerController@destroy', $banner->id)}}', null, App.redirect('/admin/banners?ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                        @endif
                                       
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
