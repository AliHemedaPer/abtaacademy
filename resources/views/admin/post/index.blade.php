@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$posts->count()}}&v=1"></script>
    <!-- /theme JS files -->
    <script type="text/javascript">  
        function setStatus(sts, id){

            $('#sts-'+id).html('<i class="icon-spinner2 spinner"></i>');
            
            $.ajax({
                url: "{{url('/admin/setStatus')}}/"+id,
                type: 'POST',
                data: {sts : sts},
                 headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                success: function (data) {
                   if(sts == 0){
                        var newHtml = "<a href='javascript://' onclick='setStatus(1, "+id+")'><span class='badge badge-secondary'>{{ __('admin.inactive') }}</span></a>";
                        $('#sts-'+id).html(newHtml);
                   }else{
                        var newHtml = "<a href='javascript://' onclick='setStatus(0, "+id+")'><span class='badge badge-success'>{{ __('admin.active') }}</span></a>";
                        $('#sts-'+id).html(newHtml);
                   }
                  }
                });
          }
    </script>
@endpush

@section('header')
    
@endsection

@section('content')
    
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.'.$post_type.'s')</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(auth('admin')->user()->canCreate($post_type))
                        <a href="{{url('admin/posts/create?pst='.$post_type)}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add_new') @lang('admin.'.$post_type)</a>
                    
                    @endif
                </div>
            </div>
        </div>
        
        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.title')</th>
                    @if((isset($post_type) && $post_type == 'course'))
                    <th>@lang('admin.status')</th>
                    @endif
                    @if(auth('admin')->user()->canUpdate($post_type) or auth('admin')->user()->canDelete($post_type))
                    <th class="text-center"></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->title_ar }}</td>
                        @if((isset($post_type) && $post_type == 'course'))
                        <td id="sts-{{ $post->id }}">
                            <a href="javascript://" onclick="setStatus('{{$post->status ? 0 : 1}}', '{{ $post->id }}')">
                                <span class="badge badge-{{$post->status ? 'success' : 'secondary'}}">{{$post->status ? __('admin.active') : __('admin.inactive') }}
                                </span>
                            </a>
                        </td>
                        @endif
                        @if(auth('admin')->user()->canUpdate($post_type) or auth('admin')->user()->canDelete($post_type))
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(auth('admin')->user()->canUpdate($post_type))
                                        <a href="{{ url('/admin/posts/'.$post->id.'/edit?pst='.$post_type)}}"
                                            class="dropdown-item"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        <!-- <a href="#"
                                            class="dropdown-item"
                                            onclick="App.toggleStatus.call(this, '{{url('admin/posts/toggle_status').'/'. $post->id}}')"><i class="fa fa-undo"></i> Toggle Status</a> -->
                                        @endif
                                        @if(auth('admin')->user()->canDelete($post_type))

                                            @if( $post->menu_page == 'why_us' )
                                                خاص بالصفحة الرئيسية لا يمكن الحذف
                                            @else
                                                <a href="#"
                                                onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\PostController@destroy', $post->id)}}', null, App.redirect('/admin/posts?pst={{$post_type}}&ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                            @endif
                                        
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
