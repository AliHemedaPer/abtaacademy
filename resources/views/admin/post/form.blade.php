 @extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin - 
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/editors/summernote/summernote.min.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
     <script src="{{asset('public/backend/js/plugins/pickers/daterangepicker.js')}}"></script>

    <!-- /theme JS files -->
    <script type="text/javascript">
        var Lightbox = function() {
            var _componentFancybox = function() {
                if (!$().fancybox) {
                    console.warn('Warning - fancybox.min.js is not loaded.');
                    return;
                }
                // Image lightbox
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            };
            return {
                init: function() {
                    _componentFancybox();
                }
            }
        }();

         // Validation config
        var FormComponents = function() {

            // Summernote
            var _componentSummernote = function() {
                if (!$().summernote) {
                    console.warn('Warning - summernote.min.js is not loaded.');
                    return;
                }

                // Basic examples
                // ------------------------------

                // Default initialization
                $('.summernote').summernote({
                    height: 300
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }

                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };
            
            // Daterange picker
            var _componentDaterange = function() {
                if (!$().daterangepicker) {
                    console.warn('Warning - daterangepicker.js is not loaded.');
                    return;
                }

                // Single picker
                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    minDate: '{{ date('Y/m/d') }}',
                    locale: {
                        // formatSubmit: 'Y-M-D',
                        format: 'Y/M/D',
                    }
                });
            };
            // Select2 select
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                var $select = $('.form-control-select2').select2({
                    
                });

                // Trigger value change when selection is made
                $select.on('change', function() {
                    $(this).trigger('blur');
                });

                @if(isset($post_type) && in_array($post_type, ['course', 'page']))
                    var $selectCategory = $('.form-control-select2-category').select2({
                        
                    });

                    $selectCategory.on('change', function() {
                        $(this).trigger('blur');
                    });


                    $(".form-control-select2-category").data('select2').trigger('select', {
                        data: {"id": '{{ isset($post) ? $post->category : ''}}' }
                    });
                @endif

                @if(isset($post_type) && in_array($post_type, ['course']))
                    var $selectTrainer = $('.form-control-select2-trainer').select2({
                        
                    });

                    $selectTrainer.on('change', function() {
                        $(this).trigger('blur');
                    });


                    $(".form-control-select2-trainer").data('select2').trigger('select', {
                        data: {"id": '{{ isset($post) ? $post->trainer : ''}}' }
                    });
                @endif

                @if((isset($post_type) && $post_type == 'page'))
                // $(".form-control-select2-post").data('select2').trigger('select', {
                //     data: {"id": '{{ isset($post) ? $post->menu_page : ''}}' }
                // });
                @endif

            };
            var _componentValidation = function() {
                if (!$().validate) {
                    console.warn('Warning - validate.min.js is not loaded.');
                    return;
                }
                // Initialize
                var validator = $('.form__init').validate({
                    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                    errorClass: 'validation-invalid-label',
                    successClass: 'validation-valid-label',
                    validClass: 'validation-valid-label',
                    highlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    success: function(label) {
                        label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
                    },

                    // Different components require proper error label placement
                    errorPlacement: function(error, element) {
                        // Unstyled checkboxes, radios
                        if (element.parents().hasClass('form-check')) {
                            error.appendTo( element.parents('.form-check').parent() );
                        }

                        // Input with icons and Select2
                        else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                            error.appendTo( element.parent() );
                        }

                        // Input group, styled file input
                        else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                            error.appendTo( element.parent().parent() );
                        }

                        // Other elements
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        quantity: {
                            number: true,
                            min: 1,
                        },
                        selling_price: {
                            number: true,
                        }
                    },
                    messages: {
                        quantity: {
                            min: 'Please select at least {0} quantity'
                        }
                    }
                });
            };
            return {
                init: function() {
                    _componentUniform();
                    _componentDaterange();
                    _componentSelect2();
                    _componentSummernote();
                    //_componentValidation();
                }
            }
        }();

        var FormComponents2 = function() {
            // Select2 select
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                var $select = $('.form-control-select2').select2({
                    
                });

                // Trigger value change when selection is made
                $select.on('change', function() {
                    $(this).trigger('blur');
                });

                // Trigger value change when selection is made
                $selectMaterial.on('change', function() {
                    $(this).trigger('blur');
                });

                $selectUnit.on('change', function() {
                    $(this).trigger('blur');
                });
            };
            return {
                init: function() {
                    _componentSelect2();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormComponents.init();
            Lightbox.init();
        });

    function removeTr(id, typ){
        $('#'+typ+'_'+id).remove();
    }
    </script>
@endpush

@section('header')

@endsection

@section('content')
<form method="POST" action="{{url('admin/posts'). (isset($post) ? ('/' . $post->id) : '')}}" class="form__init" enctype="multipart/form-data">

    <!-- Basic modal -->
    
    <div id="modal_default" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('admin.certificates')</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    @foreach($certificates as $certificate)
                        <div style="padding-bottom: 5px">
                            <input type="checkbox" name="certificates[]" value="{{ $certificate->id }}" {{ (isset($post) && isset($post->certificates) && in_array($certificate->id, $post->certificates)) ? 'checked="checked"' : '' }}/> {{ $certificate->title_ar }}
                        </div>
                    @endforeach
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">تم</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /basic modal -->

    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ isset($post) ? __('admin.edit') : __('admin.add') }} {{ __('admin.'.$post_type) }}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(count($certificates) > 0)
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_default" style="width: 200px;margin-left: 50px"><i class="icon-certificate"></i> @lang('admin.certificates') </button>
                    @endif
                    <button type="button" onclick="App.redirect('/admin/posts?pst={{ $post_type }}');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>

                    
                </div>
            </div>
        </div>

        <div class="card-body">
                @if(isset($post))
                    {{ method_field('PATCH') }}
                @endif
                @csrf
                <input type="hidden" name="post_type" value="{{ (isset($post) ? $post->post_type : (isset($post_type) ? $post_type : '')) }}">
                @if((isset($post_type) && $post_type == 'page'))
                <!-- <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.menu_page'):</label>
                                <select name="menu_page" id="menu_page" class="form-control form-control-select2-post select-search" data-container-css-class="bg-teal-400" data-fouc>
                                    <option value="">---- بدون ----</option>
                                    <option value="about" >@lang('admin.about')</option>
                                    <option value="why_us" >@lang('admin.why_us')</option>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                </div> -->
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title')  (EN):</label>
                                <input type="text" class="form-control" name="title_en"
                                    value="{{isset($post) ? $post->title_en: old('title_en') }}" required>
                            </div>
                            @if( isset($post_type) && in_array($post_type, ['course', 'page']))
                            <div class="row">
                                <div class="col-md-12" style="padding-left: 0; padding-right: 0">
                                    <div class="form-group col-md-{{ in_array($post_type, ['course']) ? '6' : '12' }}" style="float: right;">
                                        <label>@lang('admin.category'):</label>
                                        <select data-placeholder="Select Category" name="category" id="category" class="form-control form-control-select2-category select-search" data-fouc>
                                            <option></option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{ (isset($post) && $post->category == $category->id) ?'selected=""':'' }}>
                                                    @include('admin.categories.parents', $category) 
                                                </option>
                                            @endforeach
                                        </select>
                                    </div> 

                                    @if(in_array($post_type, ['course']))
                                    <div class="form-group col-md-6" style="float: right;">
                                        <label>@lang('admin.trainer'):</label>
                                        <select data-placeholder="Select trainer" name="trainer" id="trainer" class="form-control form-control-select2-trainer select-search" data-fouc>
                                            <option></option>
                                            @foreach($trainers as $trainer)
                                                <option value="{{ $trainer->id }}" {{ (isset($post) && $post->trainer == $trainer->id) ?'selected=""':'' }}>
                                                    {{ $trainer->name_ar }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div> 
                                    @endif  
                                </div> 
                            </div> 
                             @endif  

                            @if($post_type == 'course')
                                <div class="form-group">
                                    <label>@lang('admin.date'):</label>
                                    <input type="text" class="form-control daterange-single" name="extra_info[date]"
                                        value="{{isset($post) ? $extra_info['date']: old('extra_info[date]') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.duration') (EN):</label>
                                    <input type="text" class="form-control" name="extra_info[duration][en]"
                                        value="{{ (isset($post) && isset($extra_info['duration']['en'])) ? $extra_info['duration']['en']: old('extra_info[duration][en]') ?? '' }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.location') (EN):</label>
                                    <input type="text" class="form-control" name="extra_info[location][en]"
                                        value="{{ (isset($post) && isset($extra_info['location']['en'])) ? $extra_info['location']['en']: old('extra_info[location][en]') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.specialization') (EN):</label>
                                    <input type="text" class="form-control" name="extra_info[specialization][en]"
                                        value="{{ (isset($post) && isset($extra_info['specialization']['en'])) ? $extra_info['specialization']['en']: old('extra_info[specialization][en]') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.level'):</label>
                                    <input type="text" class="form-control" name="extra_info[level]"
                                        value="{{isset($post) ? $extra_info['level']: old('extra_info[level]') }}" required>
                                </div>
                            @endif                         
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title') (AR):</label>
                                <input type="text" class="form-control" name="title_ar"
                                    value="{{isset($post) ? $post->title_ar: old('title_ar') }}" required>
                            </div>

                            @if((isset($post_type) && in_array($post_type, ['course', 'page'])))
                                <div class="form-group">
                                    <label>@lang('admin.video'):</label>
                                    <input type="text" class="form-control" name="video_url"
                                        value="{{isset($post) ? $post->video_url: old('video_url') }}">
                                </div>
                            @endif  

                            @if($post_type == 'course')
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>@lang('admin.fees') (LE):</label>
                                        <input type="text" class="form-control" name="extra_info[fees]"
                                            value="{{ (isset($post) && isset($extra_info['fees'])) ? $extra_info['fees']: old('extra_info[fees]') }}" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>السعر النهائي (LE):</label>
                                        <input type="text" class="form-control" name="extra_info[finalfees]"
                                            value="{{ (isset($post) && isset($extra_info['finalfees'])) ? $extra_info['finalfees']: old('extra_info[finalfees]') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.duration') (AR):</label>
                                    <input type="text" class="form-control" name="extra_info[duration][ar]"
                                        value="{{ (isset($post) && isset($extra_info['duration']['ar'])) ? $extra_info['duration']['ar']: old('extra_info[duration][ar]') ?? '' }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.location') (AR):</label>
                                    <input type="text" class="form-control" name="extra_info[location][ar]"
                                        value="{{ (isset($post) && isset($extra_info['location']['ar'])) ? $extra_info['location']['ar']: old('extra_info[location][ar]') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.specialization') (AR):</label>
                                    <input type="text" class="form-control" name="extra_info[specialization][ar]"
                                        value="{{ (isset($post) && isset($extra_info['specialization']['ar'])) ? $extra_info['specialization']['ar']: old('extra_info[specialization][ar]') }}" required>
                                </div>
                                
                            @endif  

                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.short_desc') (EN):</label>
                                <textarea rows="4" id="short_desc" class="{{ (isset($post_type) && $post_type == 'page') ? 'summernote' : 'form-control' }}" name="short_desc_en" 
                                    >{{isset($post) ? $post->short_desc_en : old('short_desc_en')}}</textarea>
                            </div>
                            @if($post_type == 'course')
                            <hr/>
                            <div class="form-group" >
                                <input type="checkbox" name="popular" {!! (isset($post) && $post->popular ) ? ' checked="checked"' : '' !!}>
                                <label><strong>دورات مشهورة</strong></label>
                            </div>
                            <hr/>
                            @endif  
                        </fieldset>
                    </div>
                    
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.short_desc') (AR):</label>
                                <textarea rows="4" id="short_desc" class="{{ (isset($post_type) && $post_type == 'page') ? 'summernote' : 'form-control' }}" name="short_desc_ar" 
                                    >{{isset($post) ? $post->short_desc_ar : old('short_desc_ar')}}</textarea>
                            </div>
                            @if($post_type == 'course')
                            <hr/>
                            <div class="form-group col-md-6" >
                                <input type="checkbox" name="offers" {!! (isset($post) && $post->offers ) ? ' checked="checked"' : '' !!}>
                                <label><strong>عروض</strong></label>
                            </div>
                            <hr/>
                            @endif  
                        </fieldset>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.content') (EN):</label>
                                <textarea rows="4" id="content_en" class="summernote" name="content_en" 
                                    >{{isset($post) ? $post->content_en : old('content_en')}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>@lang('admin.content') (AR):</label>
                                <textarea rows="4" id="content_ar" class="summernote" name="content_ar" 
                                    >{{isset($post) ? $post->content_ar : old('content_ar')}}</textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.image') (W:800px - H:530px):</label>
                                <input type="file" name="image" class="form-control-uniform-custom">
                            </div>
                            @if(isset($post) && $post->image)
                            <div class="card-img-actions m-1">
                                <img class="card-img img-fluid" src="{{ asset('public/images/posts/'.$post->image) }}" alt="">
                                <div class="card-img-actions-overlay card-img">
                                    <a href="{{ asset('public/images/posts/'.$post->image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                        <i class="icon-plus3"></i>
                                    </a>
                                </div>
                            </div>
                            @endif
                        </fieldset>

                        @if($post_type == 'course')
                        <fieldset>
                            <br/><br/>
                            <div class="form-group">
                                <label>@lang('admin.brochure'):</label>
                                <input type="file" name="brochure" class="form-control-uniform-custom">
                            </div>
                            @if(isset($post) && $post->brochure)
                            <div class="card-img-actions m-1">
                                <a href="{{ url('public/brochures/posts/'.$post->brochure) }}" target="_blank">@lang('front.download')</a>
                                <br/>
                                <input type="checkbox" name="remove_brochure"> @lang('admin.delete')
                            </div>
                            @endif
                        </fieldset>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.upload_video') - MAX 50MB:</label>
                                <input type="file" name="video" class="form-control-uniform-custom">
                            </div>
                            @if(isset($post) && $post->video)
                            <div class="card-img-actions m-1">
                                <embed src="{{ asset('public/videos/posts/'.$post->video) }}" allowfullscreen="true" width="425" height="320">
                            </div>
                            <input type="checkbox" name="remove_video"> @lang('admin.delete')
                            @endif
                        </fieldset>
                    </div>
                </div>
                <br/>
                <div class="d-flex justify-content-end align-items-center">
                    
                        <button type="button" onclick="App.redirect('/admin/posts?pst={{ $post_type }}');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
        </div>
    </div>
    <!-- /2 columns form -->
</form>
@endsection

@section('jquery')
<script type="text/javascript">

</script>
@endsection


