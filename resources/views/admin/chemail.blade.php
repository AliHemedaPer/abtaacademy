@extends('layouts.admin.master')

@section('title')
    {{ $appSettings->app_name }} - Admin -
@endsection

@push('scripts')
@endpush

@section('header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i> تغيير البريد الإليكتروني للحساب</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- 2 columns form -->
    <div class="card">

        <div class="card-body">
            <form method="POST" action="{{url('admin/chemail')}}" class="form__init"  enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>البريد الإليكتروني:</label>
                                <input type="email" class="form-control" name="email"
                                     required>
                            </div>

                           
                        </fieldset>
                    </div>
                </div>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="submit" class="btn btn-primary">حفظ <i class="icon-floppy-disk"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /2 columns form -->
@endsection
