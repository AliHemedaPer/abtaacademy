@if(isset($category->parent))
    @include('admin.categories.parents', ['category' => $category->parentObj])
@endif

@if(isset($category->name_ar))
	&larr; {{ $category->name_ar }}
@endif