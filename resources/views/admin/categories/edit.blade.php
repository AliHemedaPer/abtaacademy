@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')

@endpush

@section('header')

@endsection

@section('content')
    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.edit') @lang('admin.category')</h5>
        </div>

        <div class="card-body">
            @include('admin.categories.form')
        </div>
    </div>
    <!-- /2 columns form -->
@endsection
