 @extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin - 
@endsection

@push('scripts')
<!-- Theme JS files -->
     <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
     <script src="{{asset('public/backend/js/plugins/pickers/daterangepicker.js')}}"></script>

     <script src="{{asset('public/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>

     <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>    
     
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>   
    <!-- /theme JS files -->
     <script type="text/javascript">    
         // Validation config
        var FormValidation = function() {


            // Lightbox
            var _componentFancybox = function() {
                if (!$().fancybox) {
                    console.warn('Warning - fancybox.min.js is not loaded.');
                    return;
                }

                // Image lightbox
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            };


            var _componentFileUpload = function() {
                if (!$().fileinput) {
                    console.warn('Warning - fileinput.min.js is not loaded.');
                    return;
                }

                //
                // Define variables
                //

                // Modal template
                var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                    '  <div class="modal-content">\n' +
                    '    <div class="modal-header align-items-center">\n' +
                    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                    '    </div>\n' +
                    '    <div class="modal-body">\n' +
                    '      <div class="floating-buttons btn-group"></div>\n' +
                    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                    '    </div>\n' +
                    '  </div>\n' +
                    '</div>\n';

                // Buttons inside zoom modal
                var previewZoomButtonClasses = {
                    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                    fullscreen: 'btn btn-light btn-icon btn-sm',
                    borderless: 'btn btn-light btn-icon btn-sm',
                    close: 'btn btn-light btn-icon btn-sm'
                };

                // Icons inside zoom modal classes
                var previewZoomButtonIcons = {
                    prev: '<i class="icon-arrow-left32"></i>',
                    next: '<i class="icon-arrow-right32"></i>',
                    toggleheader: '<i class="icon-menu-open"></i>',
                    fullscreen: '<i class="icon-screen-full"></i>',
                    borderless: '<i class="icon-alignment-unalign"></i>',
                    close: '<i class="icon-cross2 font-size-base"></i>'
                };

                // File actions
                var fileActionSettings = {
                    zoomClass: '',
                    zoomIcon: '<i class="icon-zoomin3"></i>',
                    dragClass: 'p-2',
                    dragIcon: '<i class="icon-three-bars"></i>',
                    removeClass: '',
                    removeErrorClass: 'text-danger',
                    removeIcon: '<i class="icon-bin"></i>',
                    indicatorNew: '<i class="icon-file-plus text-success"></i>',
                    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                    indicatorError: '<i class="icon-cross2 text-danger"></i>',
                    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
                };

                $('.file-input').fileinput({
                    browseLabel: 'Browse',
                    browseIcon: '<i class="icon-file-plus mr-2"></i>',
                   // uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                   // removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate
                    },
                    initialCaption: "No file selected",
                    previewZoomButtonClasses: previewZoomButtonClasses,
                    previewZoomButtonIcons: previewZoomButtonIcons,
                    fileActionSettings: fileActionSettings
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }
                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };    


            return {
                init: function() {
                    _componentFileUpload();
                    _componentFancybox();
                    _componentUniform();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
           
        });

    function removeTr(id, typ){
        $('#'+typ+'_'+id).remove();
    }
    </script>
@endpush

@section('header')

@endsection

@section('content')
    <!-- 2 columns form -->
    <form method="POST" action="{{url('admin/add_image')}}" class="form__init" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="img_typ" value="client">
        <!-- <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> 
                
                @lang('admin.package') [ {{ __('admin.clients') }} ]</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <button type="button" onclick="App.redirect('/admin/packages');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>

                        <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> صور العملاء</h5>
            </div>

            <div class="card-body">
                <div class="col-lg-12">
                    <input type="file" name="images[]" class="file-input" multiple="multiple" data-fouc>
                </div>
                <br/>
                <div class="row">
                    @foreach ($images as $p_image) 
                    <div id="image_{{ $loop->index }}" class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-img-actions m-1">
                                <img class="card-img img-fluid" src="{{ asset($p_image->image) }}" alt="">
                                <div class="card-img-actions-overlay card-img">
                                    <a href="{{ asset($p_image->image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                        <i class="icon-eye"></i>
                                    </a>

                                    <a href="javascript://" onclick="App.dialog({}, () => App.makeRequest('delete', '/admin/image/{{ $p_image->id }}', null, removeTr('{{ $loop->index }}', 'image') ));" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                        <i class="icon-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </form>
    <!-- /2 columns form -->
@endsection
