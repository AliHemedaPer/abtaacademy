@extends('layouts.admin.master')

@section('title')
    {{ $appSettings->app_name }} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$admins->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('header')

@endsection

@section('content')
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.admins')</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(auth('admin')->user()->canCreate('admin'))
                        <a href="{{url('admin/admins/create')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add_new') @lang('admin.admin')</a>
                    @endif
                </div>
            </div>
        </div>

        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.name')</th>
                    <th>@lang('admin.email')</th>
                    @if(auth('admin')->user()->canUpdate('admin') or auth('admin')->user()->canDelete('admin'))
                    <th class="text-center"></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($admins as $admin)
                    <tr>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->email}}</td>
                        @if(auth('admin')->user()->canUpdate('admin') or auth('admin')->user()->canDelete('admin'))
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(auth('admin')->user()->canUpdate('admin'))
                                        <a href="#"
                                            class="dropdown-item"
                                            onclick="App.redirect('/admin/admins/{{$admin->id}}/edit')"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        @endif
                                        @if(auth('admin')->user()->canDelete('admin'))
                                        <a href="#"
                                            onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\AdminController@destroy', $admin->id)}}', null, App.redirect('/admin/admins?ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
