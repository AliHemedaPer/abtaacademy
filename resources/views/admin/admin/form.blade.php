<form method="POST" action="{{url('admin/admins'). (isset($admin) ? ('/' . $admin->id) : '')}}" class="form__init">
    @if(isset($admin))
        {{ method_field('PATCH') }}
    @endif
    @csrf
    <div class="row">
        <div class="col-md-12">
            @if(!isset($admin))
            <fieldset>
                <legend class="font-weight-semibold" style="font-size: 18px"><i class="icon-user mr-2"></i> @lang('admin.info')</legend>
                <div class="form-group">
                    <label class="d-block font-weight-semibold">@lang('admin.name'):</label>
                    <input type="text" class="form-control" name="name"
                        value="{{isset($admin) ? $admin->name: old('name') }}" placeholder="Jhone duo" autocomplete="off" required>
                </div>

                <div class="form-group">
                    <label class="d-block font-weight-semibold">@lang('admin.email'):</label>
                    <input type="text" class="form-control" name="email"
                        value="{{isset($admin) ? $admin->email: old('email') }}" placeholder="example@mail.com" autocomplete="off" required>
                </div>

                <div class="form-group">
                    <label class="d-block font-weight-semibold">@lang('admin.password'):</label>
                    <input type="text" class="form-control" name="password"
                        value="{{isset($admin) ? $admin->password : (isset($randomPassword) ? $randomPassword : old('password'))}}" placeholder="Strong password" required />
                </div>
            </fieldset>
            @endif

            <fieldset>
                <legend class="font-weight-semibold" style="font-size: 18px"><i class="icon-user-lock mr-2"></i> @lang('admin.permissions')

                    

                </legend>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group perm_categories">
                            <div class="custom-control  custom-checkbox">
                                <input 
                                    type="checkbox" class="custom-control-input" id="select_all" onClick="toggle(this)">
                                <label class="custom-control-label" for="select_all">@lang('admin.select_all')</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group perm_categories">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.categories')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['category']) && in_array('create', $admin['rols']['category']) ? 'checked' : ''}}
                                    name="rols[category][create]" type="checkbox" class="custom-control-input" id="perm_categories_create" >
                                <label class="custom-control-label" for="perm_categories_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['category']) && in_array('read', $admin['rols']['category']) ? 'checked' : ''}}
                                    name="rols[category][read]" type="checkbox" class="custom-control-input" id="perm_categories_read">
                                <label class="custom-control-label" for="perm_categories_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['category']) && in_array('update', $admin['rols']['category']) ? 'checked' : ''}}
                                    name="rols[category][update]" type="checkbox" class="custom-control-input" id="perm_categories_update">
                                <label class="custom-control-label" for="perm_categories_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['category']) && in_array('delete', $admin['rols']['category']) ? 'checked' : ''}}
                                    name="rols[category][delete]" type="checkbox" class="custom-control-input" id="perm_categories_delete">
                                <label class="custom-control-label" for="perm_categories_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.services')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['service']) && in_array('create', $admin['rols']['service']) ? 'checked' : ''}}
                                    name="rols[service][create]" type="checkbox" class="custom-control-input" id="perm_service_create" >
                                <label class="custom-control-label" for="perm_servicees_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['service']) && in_array('read', $admin['rols']['service']) ? 'checked' : ''}}
                                    name="rols[service][read]" type="checkbox" class="custom-control-input" id="perm_services_read">
                                <label class="custom-control-label" for="perm_services_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['service']) && in_array('update', $admin['rols']['service']) ? 'checked' : ''}}
                                    name="rols[service][update]" type="checkbox" class="custom-control-input" id="perm_services_update">
                                <label class="custom-control-label" for="perm_services_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['service']) && in_array('delete', $admin['rols']['service']) ? 'checked' : ''}}
                                    name="rols[service][delete]" type="checkbox" class="custom-control-input" id="perm_services_delete">
                                <label class="custom-control-label" for="perm_services_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.courses')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['course']) && in_array('create', $admin['rols']['course']) ? 'checked' : ''}}
                                    name="rols[course][create]" type="checkbox" class="custom-control-input" id="perm_course_create" >
                                <label class="custom-control-label" for="perm_coursees_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['course']) && in_array('read', $admin['rols']['course']) ? 'checked' : ''}}
                                    name="rols[course][read]" type="checkbox" class="custom-control-input" id="perm_courses_read">
                                <label class="custom-control-label" for="perm_courses_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['course']) && in_array('update', $admin['rols']['course']) ? 'checked' : ''}}
                                    name="rols[course][update]" type="checkbox" class="custom-control-input" id="perm_courses_update">
                                <label class="custom-control-label" for="perm_courses_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['course']) && in_array('delete', $admin['rols']['course']) ? 'checked' : ''}}
                                    name="rols[course][delete]" type="checkbox" class="custom-control-input" id="perm_courses_delete">
                                <label class="custom-control-label" for="perm_courses_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.pages')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['page']) && in_array('create', $admin['rols']['page']) ? 'checked' : ''}}
                                    name="rols[page][create]" type="checkbox" class="custom-control-input" id="perm_page_create" >
                                <label class="custom-control-label" for="perm_pagees_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['page']) && in_array('read', $admin['rols']['page']) ? 'checked' : ''}}
                                    name="rols[page][read]" type="checkbox" class="custom-control-input" id="perm_pages_read">
                                <label class="custom-control-label" for="perm_pages_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['page']) && in_array('update', $admin['rols']['page']) ? 'checked' : ''}}
                                    name="rols[page][update]" type="checkbox" class="custom-control-input" id="perm_pages_update">
                                <label class="custom-control-label" for="perm_pages_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['page']) && in_array('delete', $admin['rols']['page']) ? 'checked' : ''}}
                                    name="rols[page][delete]" type="checkbox" class="custom-control-input" id="perm_pages_delete">
                                <label class="custom-control-label" for="perm_pages_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.our_valuess')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['our_values']) && in_array('create', $admin['rols']['our_values']) ? 'checked' : ''}}
                                    name="rols[our_values][create]" type="checkbox" class="custom-control-input" id="perm_our_values_create" >
                                <label class="custom-control-label" for="perm_our_valueses_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['our_values']) && in_array('read', $admin['rols']['our_values']) ? 'checked' : ''}}
                                    name="rols[our_values][read]" type="checkbox" class="custom-control-input" id="perm_our_valuess_read">
                                <label class="custom-control-label" for="perm_our_valuess_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['our_values']) && in_array('update', $admin['rols']['our_values']) ? 'checked' : ''}}
                                    name="rols[our_values][update]" type="checkbox" class="custom-control-input" id="perm_our_valuess_update">
                                <label class="custom-control-label" for="perm_our_valuess_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['our_values']) && in_array('delete', $admin['rols']['our_values']) ? 'checked' : ''}}
                                    name="rols[our_values][delete]" type="checkbox" class="custom-control-input" id="perm_our_valuess_delete">
                                <label class="custom-control-label" for="perm_our_valuess_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.blogs')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['blog']) && in_array('create', $admin['rols']['blog']) ? 'checked' : ''}}
                                    name="rols[blog][create]" type="checkbox" class="custom-control-input" id="perm_blog_create" >
                                <label class="custom-control-label" for="perm_bloges_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['blog']) && in_array('read', $admin['rols']['blog']) ? 'checked' : ''}}
                                    name="rols[blog][read]" type="checkbox" class="custom-control-input" id="perm_blogs_read">
                                <label class="custom-control-label" for="perm_blogs_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['blog']) && in_array('update', $admin['rols']['blog']) ? 'checked' : ''}}
                                    name="rols[blog][update]" type="checkbox" class="custom-control-input" id="perm_blogs_update">
                                <label class="custom-control-label" for="perm_blogs_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['blog']) && in_array('delete', $admin['rols']['blog']) ? 'checked' : ''}}
                                    name="rols[blog][delete]" type="checkbox" class="custom-control-input" id="perm_blogs_delete">
                                <label class="custom-control-label" for="perm_blogs_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.banners')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['banner']) && in_array('create', $admin['rols']['banner']) ? 'checked' : ''}}
                                    name="rols[banner][create]" type="checkbox" class="custom-control-input" id="perm_banner_create" >
                                <label class="custom-control-label" for="perm_banneres_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['banner']) && in_array('read', $admin['rols']['banner']) ? 'checked' : ''}}
                                    name="rols[banner][read]" type="checkbox" class="custom-control-input" id="perm_banners_read">
                                <label class="custom-control-label" for="perm_banners_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['banner']) && in_array('update', $admin['rols']['banner']) ? 'checked' : ''}}
                                    name="rols[banner][update]" type="checkbox" class="custom-control-input" id="perm_banners_update">
                                <label class="custom-control-label" for="perm_banners_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['banner']) && in_array('delete', $admin['rols']['banner']) ? 'checked' : ''}}
                                    name="rols[banner][delete]" type="checkbox" class="custom-control-input" id="perm_banners_delete">
                                <label class="custom-control-label" for="perm_banners_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_medicines">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.counters')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['counter']) && in_array('create', $admin['rols']['counter']) ? 'checked' : ''}}
                                    name="rols[counter][create]" type="checkbox" class="custom-control-input" id="perm_counter_create" >
                                <label class="custom-control-label" for="perm_counteres_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['counter']) && in_array('read', $admin['rols']['counter']) ? 'checked' : ''}}
                                    name="rols[counter][read]" type="checkbox" class="custom-control-input" id="perm_counters_read">
                                <label class="custom-control-label" for="perm_counters_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['counter']) && in_array('update', $admin['rols']['counter']) ? 'checked' : ''}}
                                    name="rols[counter][update]" type="checkbox" class="custom-control-input" id="perm_counters_update">
                                <label class="custom-control-label" for="perm_counters_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['counter']) && in_array('delete', $admin['rols']['counter']) ? 'checked' : ''}}
                                    name="rols[counter][delete]" type="checkbox" class="custom-control-input" id="perm_counters_delete">
                                <label class="custom-control-label" for="perm_counters_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_admins">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.admins')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['admin']) && in_array('create', $admin['rols']['admin']) ? 'checked' : ''}}
                                    name="rols[admin][create]" type="checkbox" class="custom-control-input" id="perm_admins_create" >
                                <label class="custom-control-label" for="perm_admins_create">@lang('admin.add')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['admin']) && in_array('read', $admin['rols']['admin']) ? 'checked' : ''}}
                                    name="rols[admin][read]" type="checkbox" class="custom-control-input" id="perm_admins_read">
                                <label class="custom-control-label" for="perm_admins_read">@lang('admin.read')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['admin']) && in_array('update', $admin['rols']['admin']) ? 'checked' : ''}}
                                    name="rols[admin][update]" type="checkbox" class="custom-control-input" id="perm_admins_update">
                                <label class="custom-control-label" for="perm_admins_update">@lang('admin.edit')</label>
                            </div>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['admin']) && in_array('delete', $admin['rols']['admin']) ? 'checked' : ''}}
                                    name="rols[admin][delete]" type="checkbox" class="custom-control-input" id="perm_admins_delete">
                                <label class="custom-control-label" for="perm_admins_delete">@lang('admin.delete')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_settings">
                            <label class="d-block font-weight-semibold text-info">@lang('admin.general_stin')</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['settings']) && in_array('update', $admin['rols']['settings']) ? 'checked' : ''}}
                                    name="rols[settings][update]" type="checkbox" class="custom-control-input" id="perm_settings_update">
                                <label class="custom-control-label" for="perm_settings_update">@lang('admin.edit')</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group perm_clients">
                            <label class="d-block font-weight-semibold text-info">العملاء</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['clients']) && in_array('read', $admin['rols']['clients']) ? 'checked' : ''}}
                                    name="rols[clients][read]" type="checkbox" class="custom-control-input" id="perm_clients_Read">
                                <label class="custom-control-label" for="perm_clients_Read">@lang('admin.read')</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group perm_messages">
                            <label class="d-block font-weight-semibold text-info">الرسائل والطلبات</label>
                            <div class="custom-control  custom-checkbox">
                                <input {{isset($admin) && isset($admin['rols']['messages']) && in_array('read', $admin['rols']['messages']) ? 'checked' : ''}}
                                    name="rols[messages][read]" type="checkbox" class="custom-control-input" id="perm_messages_Read">
                                <label class="custom-control-label" for="perm_messages_Read">@lang('admin.read')</label>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </fieldset>
        </div>
    </div>

    <div class="d-flex justify-content-end align-items-center">
        
        <button type="button" onclick="App.redirect('/admin/admins');" class="btn btn-default mr-2">Cancel <i class="fa fa-undo ml-2"></i></button>
        
        <button type="submit" class="btn btn-primary">Save <i class="icon-floppy-disk"></i></button>
    </div>
</form>
