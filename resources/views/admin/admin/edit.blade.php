@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection
@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <!-- /theme JS files -->
    <script type="text/javascript">
         // Validation config
        var FormValidation = function() {
            var _componentValidation = function() {
                if (!$().validate) {
                    console.warn('Warning - validate.min.js is not loaded.');
                    return;
                }
                // Initialize
                var validator = $('.form__init').validate({
                    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                    errorClass: 'validation-invalid-label',
                    successClass: 'validation-valid-label',
                    validClass: 'validation-valid-label',
                    highlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    success: function(label) {
                        label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
                    },

                    // Different components require proper error label placement
                    errorPlacement: function(error, element) {
                        // Unstyled checkboxes, radios
                        if (element.parents().hasClass('form-check')) {
                            error.appendTo( element.parents('.form-check').parent() );
                        }

                        // Input with icons and Select2
                        else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                            error.appendTo( element.parent() );
                        }

                        // Input group, styled file input
                        else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                            error.appendTo( element.parent().parent() );
                        }

                        // Other elements
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        name: {
                            minlength: 6,
                        },
                        email: {
                            email: true,
                        },
                        password: {
                            minlength: 6,
                        }
                    },
                    messages: {
                    }
                });

                // Reset form
                $('#reset').on('click', function() {
                    validator.resetForm();
                });

                // Listen for click on toggle checkbox
                $('#select_all').on('click', function() {
                    if(this.checked) {
                        // Iterate each checkbox
                        $(':checkbox').each(function() {
                            this.checked = true;                        
                        });
                    } else {
                        $(':checkbox').each(function() {
                            this.checked = false;                       
                        });
                    }
                });
                
            };
            return {
                init: function() {
                    _componentValidation();
                }
            }
        }();
        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
        });
    </script>
@endpush

@section('header')
@endsection

@section('content')
    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.edit') @lang('admin.admin')</h5>
            <div class="header-elements">
            </div>
        </div>

        <div class="card-body">
            @include('admin.admin.form')
        </div>
    </div>
    <!-- /2 columns form -->
@endsection