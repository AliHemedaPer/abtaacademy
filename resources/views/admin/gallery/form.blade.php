@push('scripts')
     <!-- Theme JS files -->
     <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
     <script src="{{asset('backend/js/plugins/pickers/daterangepicker.js')}}"></script>

     <script src="{{asset('public/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>
 
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>   
    <!-- /theme JS files -->
     <script type="text/javascript">    
         // Validation config
        var FormValidation = function() {


            // Lightbox
            var _componentFancybox = function() {
                if (!$().fancybox) {
                    console.warn('Warning - fancybox.min.js is not loaded.');
                    return;
                }

                // Image lightbox
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            };


            var _componentFileUpload = function() {
                if (!$().fileinput) {
                    console.warn('Warning - fileinput.min.js is not loaded.');
                    return;
                }

                //
                // Define variables
                //

                // Modal template
                var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                    '  <div class="modal-content">\n' +
                    '    <div class="modal-header align-items-center">\n' +
                    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                    '    </div>\n' +
                    '    <div class="modal-body">\n' +
                    '      <div class="floating-buttons btn-group"></div>\n' +
                    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                    '    </div>\n' +
                    '  </div>\n' +
                    '</div>\n';

                // Buttons inside zoom modal
                var previewZoomButtonClasses = {
                    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                    fullscreen: 'btn btn-light btn-icon btn-sm',
                    borderless: 'btn btn-light btn-icon btn-sm',
                    close: 'btn btn-light btn-icon btn-sm'
                };

                // Icons inside zoom modal classes
                var previewZoomButtonIcons = {
                    prev: '<i class="icon-arrow-left32"></i>',
                    next: '<i class="icon-arrow-right32"></i>',
                    toggleheader: '<i class="icon-menu-open"></i>',
                    fullscreen: '<i class="icon-screen-full"></i>',
                    borderless: '<i class="icon-alignment-unalign"></i>',
                    close: '<i class="icon-cross2 font-size-base"></i>'
                };

                // File actions
                var fileActionSettings = {
                    zoomClass: '',
                    zoomIcon: '<i class="icon-zoomin3"></i>',
                    dragClass: 'p-2',
                    dragIcon: '<i class="icon-three-bars"></i>',
                    removeClass: '',
                    removeErrorClass: 'text-danger',
                    removeIcon: '<i class="icon-bin"></i>',
                    indicatorNew: '<i class="icon-file-plus text-success"></i>',
                    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                    indicatorError: '<i class="icon-cross2 text-danger"></i>',
                    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
                };

                $('.file-input').fileinput({
                    browseLabel: 'Browse',
                    browseIcon: '<i class="icon-file-plus mr-2"></i>',
                   // uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                   // removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate
                    },
                    initialCaption: "No file selected",
                    previewZoomButtonClasses: previewZoomButtonClasses,
                    previewZoomButtonIcons: previewZoomButtonIcons,
                    fileActionSettings: fileActionSettings
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }
                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };    

          


            return {
                init: function() {
                    _componentFileUpload();
                    _componentFancybox();
                    _componentUniform();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
           
        });

    function removeTr(id, typ){
        $('#'+typ+'_'+id).remove();
    }
    </script>
@endpush
    
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> 
            @if(isset($gallery))
                @lang('admin.edit') 
            @else
                @lang('admin.add_new') 
            @endif

            معرض الصور</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <button type="button" onclick="App.redirect('/admin/gallery_images');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>

                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            @if(isset($gallery))
            {{ method_field('PATCH') }}
            @endif
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <fieldset>

                        <div class="form-group col-md-12" >
                            <label>@lang('admin.title') (EN):</label>
                            <input type="text" class="form-control" name="title_en"
                                   value="{{isset($gallery) ? $gallery->title_en: old('title_en') }}" required>
                        </div>



                        <div class="form-group col-md-12">
                            <label>@lang('admin.image'):</label>
                            <input type="file" name="image" class="form-control-uniform-custom">
                        </div>
                        <div class="card-img-actions m-1 col-md-6">
                            @if(isset($gallery) && $gallery->image)
                            <img class="card-img img-fluid" src="{{ asset($gallery->image) }}" alt="">
                            @else
                            <img class="card-img img-fluid" src="{{ asset('images/no-photo.png') }}" alt="" style="max-width: 100px">
                            @endif
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>


                        <div class="form-group col-md-12" >
                            <label>@lang('admin.title') (AR):</label>
                            <input type="text" class="form-control" name="title_ar"
                                   value="{{isset($gallery) ? $gallery->title_ar: old('title_ar') }}" required>
                        </div>

                    </fieldset>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <div class="form-group">
                            <label>@lang('admin.content') (EN):</label>
                            <textarea rows="10" id="desc_en" class="form-control" name="desc_en" 
                                >{{isset($gallery) ? $gallery->desc_en : old('desc_en')}}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <label>@lang('admin.content') (AR):</label>
                            <textarea rows="10" id="desc_ar" class="form-control" name="desc_ar" 
                                >{{isset($gallery) ? $gallery->desc_ar : old('desc_ar')}}</textarea>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> الصور</h5>
        </div>

        <div class="card-body">
            <div class="col-lg-12">
                <input type="file" name="images[]" class="file-input" multiple="multiple" data-fouc>
            </div>
            <br/>
            <div class="row">
                @if(isset($gallery))
                    @foreach ($gallery->images as $p_image) 
                    <div id="image_{{ $loop->index }}" class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-img-actions m-1">
                                <img class="card-img img-fluid" src="{{ asset($p_image->image) }}" alt="">
                                <div class="card-img-actions-overlay card-img">
                                    <a href="{{ asset($p_image->image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                        <i class="icon-eye"></i>
                                    </a>

                                    <a href="javascript://" onclick="App.dialog({}, () => App.makeRequest('delete', '/admin/image/{{ $p_image->id }}', null, removeTr('{{ $loop->index }}', 'image') ));" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                        <i class="icon-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i></h5>
            <div class="header-elements">
                <div class="list-icons">
                    <button type="button" onclick="App.redirect('/admin/gallery_images');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>

                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>

@section('jquery')

@endsection

    

