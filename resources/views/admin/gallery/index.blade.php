@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$gallery->count()}}"></script>
    <!-- /theme JS files -->

        <script type="text/javascript">    
         // Validation config
        var FormValidation = function() {
            // Select2 select

            return {
                init: function() {
                    //_componentValidation();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
           
        });
    </script>
@endpush

@section('header')

@endsection

@section('content')

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> معرض الصور </h5>
            <div class="header-elements">
                <div class="list-icons">
                        
                        <a href="{{url('admin/gallery_images/create')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add') مجموعة</a>
                    
                </div>
            </div>
        </div>

        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>المجموعة</th>
                    <th class="text-center" style="width: 200px;"></th>                    
                </tr>
            </thead>
            <tbody>
                @foreach($gallery as $image)
                    <tr>
                        <td>
                            &larr; {{ $image->title_ar }}
                        </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="#"
                                            class="dropdown-item"
                                            onclick="App.redirect('/admin/gallery_images/{{$image->id}}/edit')"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        
                                        <a href="#"
                                            onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\GalleryController@destroy', $image->id)}}', null, App.redirect('/admin/gallery_images?ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
