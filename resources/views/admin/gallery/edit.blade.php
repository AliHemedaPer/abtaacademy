@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')

@endpush

@section('header')

@endsection

@section('content')
    <!-- 2 columns form -->
    <form method="POST" action="{{url('admin/gallery_images'). (isset($gallery) ? ('/' . $gallery->id) : '')}}" class="form__init" enctype="multipart/form-data">
        @include('admin.gallery.form')
    </form>
    <!-- /2 columns form -->
@endsection
