@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$counters->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('header')
    
@endsection

@section('content')
    
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.counters')</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(auth('admin')->user()->canCreate('counter'))
                        <a href="{{url('admin/counters/create')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add_new') @lang('admin.counter')</a>
                    @endif
                    
                </div>
            </div>
        </div>
        
        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.title')</th>
                    <th>@lang('admin.count')</th>
                    @if(auth('admin')->user()->canUpdate('counter') or auth('admin')->user()->canDelete('counter'))
                    <th class="text-center"></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($counters as $counter)
                    <tr>
                        <td>{{ $counter->title_en }} / {{ $counter->title_ar }}</td>
                        <td>{{ $counter->count }}</td>
                        @if(auth('admin')->user()->canUpdate('counter') or auth('admin')->user()->canDelete('counter'))
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(auth('admin')->user()->canUpdate('counter'))
                                        <a href="{{ url('/admin/counters/'.$counter->id.'/edit')}}"
                                            class="dropdown-item"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        @endif

                                        @if(auth('admin')->user()->canDelete('counter'))
                                        <a href="#"
                                            onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\CountersController@destroy', $counter->id)}}', null, App.redirect('/admin/counters?ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                       @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
