@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$clients->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('styles')
.uniform-choice {
    margin: auto !important;
}
@endsection

@section('content')
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> العملاء</h5>
            <div class="header-elements">
                <div class="list-icons">
                        <a href="{{url('admin/messages/mail')}}" class="btn btn-info btn-labeled btn-labeled-left btn-sm"><b><i class="icon-envelop3"></i></b>@lang('admin.send_all')</a>
                    
                    
                </div>
            </div>
        </div>

        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.name')</th>
                    <th>@lang('admin.email')</th>
                    <th>@lang('admin.phone')</th>
                    <th>نوع العميل</th>
                    <th class="text-center"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                    <tr id="row_{{ $client->id }}">
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->phone }}</td>
                        <td>{{ $client->client_type == 'company' ? 'شركات': 'أفراد' }}</td>
                        <td class="text-center">
                            @if(auth('admin')->user()->canRead('clients'))
                            <a href="{{ url('/admin/messages/mail?cid='.$client->id)}}"
                                            class="btn btn-info" title="إرسال رسالة"><i class="icon-envelop3"></i></a>
                            @endif
                        </td>
                        <!-- <td class="text-center">
                            <input class="status_radio form-check-input-styled" type="radio" name="client_{{ $client->id }}" data-id="{{ $client->id }}" value="0" {!! $client->status == 0 ? 'checked=""':'' !!}>
                        </td>
                        <td class="text-center">
                            <input class="status_radio form-check-input-styled" type="radio" name="client_{{ $client->id }}" data-id="{{ $client->id }}" value="-1" {!! $client->status == -1 ? 'checked=""':'' !!}>
                        </td> -->
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
     <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
    <!-- /page length options -->
@endsection

@section('jquery')
<script type="text/javascript">
    var _token = $('#_token').val();
    $(document).ready(function(){
        $('.status_radio').click(function(){
            let cl  = $(this).attr('data-id');
            let sts = $(this).val();
            $.ajax({
                url:'{{ URL::to('admin/client')}}/'+cl+'/'+sts,
                type:'POST',
                data:'_token='+_token,
                success: function(alerts){
                    //alert(alerts);
                }
            });
        });
    });
</script>
@endsection