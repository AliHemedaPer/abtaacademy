@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$trainers->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('header')
    
@endsection

@section('content')
    
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.trainers')</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if(auth('admin')->user()->canCreate('trainer'))
                        <a href="{{url('admin/trainers/create')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-plus3"></i></b> @lang('admin.add_new') @lang('admin.trainer')</a>
                    @endif
                    
                </div>
            </div>
        </div>
        
        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.name')</th>
                    <th>@lang('admin.position')</th>
                    <th>@lang('admin.image')</th>
                    @if(auth('admin')->user()->canUpdate('trainer') or auth('admin')->user()->canDelete('trainer'))
                    <th class="text-center"></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($trainers as $trainer)
                    <tr>
                        <td>{{ $trainer->name_ar }}</td>
                        <td>{{ $trainer->title_ar }}</td>
                        <td><img src="{{ url($trainer->image) }}" width="65" /></td>
                        @if(auth('admin')->user()->canUpdate('trainer') or auth('admin')->user()->canDelete('trainer'))
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(auth('admin')->user()->canUpdate('trainer'))
                                        <a href="{{ url('/admin/trainers/'.$trainer->id.'/edit')}}"
                                            class="dropdown-item"><i class="fa fa-edit"></i> @lang('admin.edit')</a>
                                        @endif

                                        @if(auth('admin')->user()->canDelete('trainer'))
                                        <a href="#"
                                            onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\TrainerController@destroy', $trainer->id)}}', null, App.redirect('/admin/trainers?ref={{ time() }}')));" class="dropdown-item"><i class="fa fa-trash"></i> @lang('admin.delete')</a>
                                        @endif
                                       
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
@endsection
