 @extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin - 
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>  
    <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
    <!-- /theme JS files -->
    <script type="text/javascript">
        var Lightbox = function() {

            // Summernote
            var _componentSummernote = function() {
                if (!$().summernote) {
                    console.warn('Warning - summernote.min.js is not loaded.');
                    return;
                }

                // Basic examples
                // ------------------------------

                // Default initialization
                $('.summernote').summernote({
                    height: 200
                });

            };

             var _componentSelect2 = function() {
                    if (!$().select2) {
                        console.warn('Warning - select2.min.js is not loaded.');
                        return;
                    }
                var $selectCategory = $('.form-control-select2-category').select2({
                        
                    });

                $selectCategory.on('change', function() {
                    $(this).trigger('blur');
                });

                $(".form-control-select2-category").data('select2').trigger('select', {
                    data: {"id": '{{ isset($trainer) ? $trainer->category : ''}}' }
                });
            };

            var _componentFancybox = function() {
                if (!$().fancybox) {
                    console.warn('Warning - fancybox.min.js is not loaded.');
                    return;
                }
                // Image lightbox
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            };
            return {
                init: function() {
                    _componentSummernote();
                    _componentSelect2();
                    _componentFancybox();
                }
            }
        }();

         // Validation config
        var FormComponents = function() {
            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }

                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };
            
            // Daterange picker
            var _componentDaterange = function() {
                if (!$().daterangepicker) {
                    console.warn('Warning - daterangepicker.js is not loaded.');
                    return;
                }

                // Single picker
                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    minDate: '{{ date('Y/m/d') }}',
                    locale: {
                        // formatSubmit: 'Y-M-D',
                        format: 'Y/M/D',
                    }
                });
            };

            var _componentValidation = function() {
                if (!$().validate) {
                    console.warn('Warning - validate.min.js is not loaded.');
                    return;
                }
                // Initialize
                var validator = $('.form__init').validate({
                    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                    errorClass: 'validation-invalid-label',
                    successClass: 'validation-valid-label',
                    validClass: 'validation-valid-label',
                    highlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    success: function(label) {
                        label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
                    },

                    // Different components require proper error label placement
                    errorPlacement: function(error, element) {
                        // Unstyled checkboxes, radios
                        if (element.parents().hasClass('form-check')) {
                            error.appendTo( element.parents('.form-check').parent() );
                        }

                        // Input with icons and Select2
                        else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                            error.appendTo( element.parent() );
                        }

                        // Input group, styled file input
                        else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                            error.appendTo( element.parent().parent() );
                        }

                        // Other elements
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        quantity: {
                            number: true,
                            min: 1,
                        },
                        selling_price: {
                            number: true,
                        }
                    },
                    messages: {
                        quantity: {
                            min: 'Please select at least {0} quantity'
                        }
                    }
                });
            };
            return {
                init: function() {
                    _componentUniform();
                    _componentDaterange();
                    //_componentValidation();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormComponents.init();
            Lightbox.init();
        });

    function removeTr(id, typ){
        $('#'+typ+'_'+id).remove();
    }
    </script>
@endpush

@section('header')

@endsection

@section('content')
<form method="POST" action="{{url('admin/trainers'). (isset($trainer) ? ('/' . $trainer->id) : '')}}" class="form__init" enctype="multipart/form-data">
    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ isset($trainer) ? __('admin.edit') : __('admin.add') }} {{ __('admin.trainer') }}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <button type="button" onclick="App.redirect('/admin/trainers');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body">
                @if(isset($trainer))
                    {{ method_field('PATCH') }}
                @endif
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.category'):</label>
                                <select data-placeholder="Select Category" name="category" id="category" class="form-control form-control-select2-category select-search" data-fouc>
                                    <option></option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ (isset($post) && $post->category == $category->id) ?'selected=""':'' }}>
                                            @include('admin.categories.parents', $category) 
                                        </option>
                                    @endforeach
                                </select>
                            </div>               
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.name')  (EN):</label>
                                <input type="text" class="form-control" name="name_en"
                                    value="{{isset($trainer) ? $trainer->name_en: old('name_en') }}" required>
                            </div>              
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.name') (AR):</label>
                                <input type="text" class="form-control" name="name_ar"
                                    value="{{isset($trainer) ? $trainer->name_ar: old('name_ar') }}" required>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.position')  (EN):</label>
                                <input type="text" class="form-control" name="title_en"
                                    value="{{isset($trainer) ? $trainer->title_en: old('title_en') }}" required>
                            </div>              
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.position') (AR):</label>
                                <input type="text" class="form-control" name="title_ar"
                                    value="{{isset($trainer) ? $trainer->title_ar: old('title_ar') }}" required>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.short_desc') (EN):</label>
                                <input type="text" class="form-control" name="short_desc_en"
                                    value="{{isset($trainer) ? $trainer->short_desc_en: old('short_desc_en') }}">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            
                            <div class="form-group">
                                <label>@lang('admin.short_desc') (AR):</label>
                                <input type="text" class="form-control" name="short_desc_ar"
                                    value="{{isset($trainer) ? $trainer->short_desc_ar: old('short_desc_ar') }}">
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.image'):</label>
                                <input type="file" name="image" class="form-control-uniform-custom">
                            </div>
                            
                            <div class="card-img-actions m-1">
                                @if(isset($trainer) && $trainer->image)
                                <img class="card-img img-fluid" src="{{ asset($trainer->image) }}" alt="">
                                <div class="card-img-actions-overlay card-img">
                                    <a href="{{ asset($trainer->image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                        <i class="icon-plus3"></i>
                                    </a>
                                </div>
                                @else
                                <img class="card-img img-fluid" src="{{ asset('public/images/no-photo.png') }}" alt="" style="max-width: 100px">
                                @endif
                            </div>
                        </fieldset>
                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.content') (EN):</label>
                                <textarea rows="4" id="content_en" class="summernote" name="content_en" 
                                    >{{isset($trainer) ? $trainer->content_en : old('content_en')}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>@lang('admin.content') (AR):</label>
                                <textarea rows="4" id="content_ar" class="summernote" name="content_ar" 
                                    >{{isset($trainer) ? $trainer->content_ar : old('content_ar')}}</textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <br/>
                <div class="d-flex justify-content-end align-items-center">
                    
                        <button type="button" onclick="App.redirect('/admin/trainers');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
        </div>
    </div>
    <!-- /2 columns form -->
</form>
@endsection

@section('jquery')
<script type="text/javascript">

</script>
@endsection


