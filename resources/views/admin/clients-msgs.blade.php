@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'ABTA ACADEMY') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$messages->count()}}&v=1"></script>
    <!-- /theme JS files -->
@endpush

@section('styles')
.uniform-choice {
    margin: auto !important;
}
@endsection

@section('content')
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i>طلبات العملاء</h5>
            <div class="header-elements">
                <div class="list-icons">
                        

                        <a href="{{url('admin/messages')}}" class="btn btn-primary btn-labeled btn-labeled-left btn-sm"><b><i class="icon-envelop3"></i></b>كل الطلبات</a>

                        <a href="{{url('admin/messages?vd=y')}}" class="btn btn-success btn-labeled btn-labeled-left btn-sm"><b><i class="icon-envelop3"></i></b>تمت المشاهدة</a>

                        <a href="{{url('admin/messages?vd=n')}}" class="btn btn-warning btn-labeled btn-labeled-left btn-sm"><b><i class="icon-envelop3"></i></b>لم تتم المشاهدة</a>

                        <a href="{{url('admin/messages/mail')}}" class="btn btn-info btn-labeled btn-labeled-left btn-sm"><b><i class="icon-envelop3"></i></b>@lang('admin.send_all')</a>
                    
                    
                </div>
            </div>
        </div>

        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>#</th>
                    <th>الطلب</th>
                    <th>@lang('admin.name')</th>
                    <th>@lang('admin.email')</th>
                    <th>@lang('admin.phone')</th>
                    <th class="text-center"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($messages as $message)
                    <tr id="row_{{ $message->id }}" style="{{ $message->viewed ? '':'    background: #bbbbbb;' }}">
                        <td>{{ $message->id }}</td>
                        <td>{{ $message->title }}</td>
                        <td>{{ $message->name }}</td>
                        <td>{{ $message->email }}</td>
                        <td>{{ $message->phone }}</td>
                        <td class="text-center">
                            @if(auth('admin')->user()->canRead('messages'))
                            <a href="{{ url('/admin/messages/mail?id='.$message->id)}}"
                                            class="btn btn-info" title="عرض الرسالة"><i class="fa fa-eye"></i></a>
                            <a href="#"
                                onclick="App.dialog({}, () => App.makeRequest('delete', '{{action('V1\\Admin\\AdminController@msgdestroy', $message->id)}}', null, App.redirect('/admin/messages?ref={{ time() }}')));" class="btn btn-danger"><i class="fa fa-trash"></i> </a>
                            @endif
                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
     <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
    <!-- /page length options -->
@endsection

@section('jquery')
<script type="text/javascript">
    var _token = $('#_token').val();
    $(document).ready(function(){
        $('.status_radio').click(function(){
            let cl  = $(this).attr('data-id');
            let sts = $(this).val();
            $.ajax({
                url:'{{ URL::to('admin/client')}}/'+cl+'/'+sts,
                type:'POST',
                data:'_token='+_token,
                success: function(alerts){
                    //alert(alerts);
                }
            });
        });
    });
</script>
@endsection