<div><strong>Request for:</strong> {{ $data['title'] }}</div>
<br/>
<strong>User Data:</strong>
<br/>
<div><strong>Name:</strong> {{ $data['quote_name_en'] }}</div>
<div><strong>Phone:</strong> {{ $data['quote_phone'] }}</div>
<div><strong>Email:</strong> {{ $data['quote_email'] }}</div> 
<div><strong>Country:</strong> {{ $data['quote_country'] }}</div> 
<div><strong>Job Title:</strong> {{ $data['quote_job'] }}</div> 
<div><strong>Education:</strong> {{ $data['quote_education'] }}</div> 
<div><strong>Payment:</strong> {{ $data['payment'] }}</div>