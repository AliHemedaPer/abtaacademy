<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'follow' => 'Follow US',
    'home' => 'Home',
    'about' => 'About Us',
    'services' => 'Services',
    'products' => 'Products',
    'links' => 'Links',
    'contact' => 'Contact Us',
    'language' => 'عربي',
    'details' => 'Details',
    'more' => 'Read More',
    'services' => 'Our services',
    'services_hint' => 'Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. <br/>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'courses' => 'Cources',
    'latest_courses' => 'Our Latest Courses',
    
    'our_values' => 'Our Values',

    'vesion' => 'Vesion',
    'mission' => 'Mission',
    'goal' => 'Goal',
    'value' => 'Value',

    'vesion_txt' => 'Far far away, behind the word mountains, far from the countries Vokalia.',
    'mission_txt' => 'Far far away, behind the word mountains, far from the countries Vokalia.',
    'goal_txt' => 'Far far away, behind the word mountains, far from the countries Vokalia.',
    'value_txt' => 'Far far away, behind the word mountains, far from the countries Vokalia.',
    
    'blog' => 'Events & Articles',
    'blog_hint' => 'Blogs & News',
    'address' => 'Address',
    'call' => 'Call Us',
    'emailus' => 'Email Us',
    'join' => 'Join us',
    'name' => 'Name',
    'email' => 'Email',
    'subject' => 'Subject',
    'message' => 'Message',
    'submit' => 'Submit',
    'rights' => '2019 © American Academy All Rights Reserved',
    'aboutus' => 'About Us',
    'aboutus_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. ',
    'buy' => 'Buy Now',
    'phone' => 'Phone',
    'sending' => 'Sending',
    'close' => 'Close',
    'send' => 'Send',
    'view' => 'View',
    'joinus' => 'Join us',
    'joinus_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'job' => 'Job Title',
    'attach' => 'Attach',
    'send' => 'Send',
    'category' => 'Category',
    'subcategory' => 'Sub Category',
    'filter' => 'Filter',
    'all' => 'All',
    'who_we_are' => 'Who We Are?',
    'why_us' => 'Why American Academy?',
    'service_agreement' => 'Service Agreement',
    'privacy_policy' => 'Privacy Policy',
    'payments' => 'Payments',
    'message_sent' => 'Message Sent Successfully!',
    'message_not_sent' => 'Message Failed, Please Try Later!',
    'select_service' => 'Select Service',
    'other_service' => 'Other Service',
    'request' => 'Request',
    'request_txt' => 'Request or Have a Question?',
    'request_quote' => 'Request A Quote',
    'menu' => 'Menu',
    'guidance' => 'You Always Get the Best Guidance',

    'have_aquestion' => 'Do you have a Question? Let us help you?',
    'haveques' => 'Have any questions?',
    'question' => 'Your Question',

    'login' => 'Login',
    'register' => 'Register',
    'pleasewait' => 'Please Wait...',
    'welcome' => 'Welcome',
    'logout' => 'Logout',
    'pass' => 'Password',
    'confpass' => 'Confirm Password',
    'plslog' => 'Please Login to Request!',
    'success' => 'Success!',
    'failed' => 'Failed, Please try later!',

    'forget' => 'Forget Password?',
    'resetpassword' => 'Reset Password',
    'newpass' => 'New Password',

    'date' => 'Date',
    'duration' => 'Duration',
    'location' => 'Location',
    'level' => 'Level',
    'specialization' => 'Specialization',
    'fees' => 'Fees',
    'joincourse' => 'Join This Course',
    'download' => 'Download Brochure',

    'required' => 'Required!',
    'passmatch' => ', Not Match!',

    'search' => 'Search',
    'searchfor' => 'Search For:',

    'pages' => 'Pages',

    'related_courses' => 'Related Courses',
    'latest_blog' => 'Latest Posts',
    'viewmore' => 'Details',

    'trainer' => 'Course Trainer',
    'trainers' => 'Trainers',
    'alltrainers' => 'All Trainers',
    'trainer_page' => 'Trainer page',
    'cources_in' => 'Courses in category: ',
    'whyjoin' => 'Why joining ',
    'cv' => 'Curriculum Vitae',
    'nocourses' => 'No Cources !',
    'certificates' => 'Certificates and credits',
    'addtofav' => 'Add to favorite',
    'addtolib' => 'Add to library',

    'watsApp' => 'WatsApp Number',
    'education' => 'Education',

    'offers' => 'Recent Offers',
    'popular' => 'Popular Cources',

    'downloads' => 'Downloads',
    'nodownloads' => 'No Downloads!',

    'videos' => 'Video Gallery',
    'novideos' => 'No Videos!',

    'clients' => 'Our Clients',
    'partners' => 'Our Partners',

    'gallery' => 'Images Gallery',
    'noimages' => 'No Images!',

    'categories' => 'Categories',
    'nomatch' => 'No Matches!',

    'certificate' => 'Certificate',

    'payment' => 'Payment',

    'bank' => 'Bank Transfer.',
    'post' => 'Post Mail.',
    'cash' => 'Cash.',
    'country' => 'Country',
    'req' => 'Required!',

    'client_type' => 'Register type',
    'client' => 'Individuals',
    'company' => 'Comapnies',
    'cp_name' => 'Company name',
    'totle' => 'Total',

    'fawaterak' => 'Fawaterak.',

    'profile' => 'My Profile',
    'mylib' => 'My Library',
    'myfav' => 'My Favorites'
];
