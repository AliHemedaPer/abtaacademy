import axios from 'axios';

export const getBaseURL = function() {
    let baseURL = window.BASE_URL || (window.BASE_URL = window.document.head.querySelector('meta[name="base-path"]'));
    return baseURL.content || false;
}
export const caxios = (function(sys) {
    sys = sys || {};
    let conf = {};
    let instance = null;
    let token = sys.token || (sys.token = window.document.head.querySelector('meta[name="csrf-token"]'));
    let baseURL = getBaseURL();
    let headers = {
        'common': {
            'X-Requested-With': 'XMLHttpRequest'
        }
    };
    if (token) headers.common['X-CSRF-TOKEN'] = token.content;
    if (baseURL) conf.baseURL = baseURL;
    conf.headers = headers;
    return function() {
        if (!instance) instance = axios.create({...conf
        });
        return instance;
    }
}(window));

export const dbug = function() {
    console.log.apply(this, arguments);
}

export const selectionSearch = function(array, comparator) {
    return array.filter(comparator)
}

export const attachEvent = function(eventName, status, elem, fnc) {
    if (status) {
        elem.addEventListener(eventName, fnc, false);
    } else {
        elem.removeEventListener(eventName, fnc, false);
    }
}
